﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class BubbleGameManagerStatic
{
    public static BubbleGameManager bubbleGameManager;
}

public class BubbleGameManager : MonoBehaviour
{
    public GameObject ResultsCanvas;
    public GameObject Timer;
    public GameObject MultiplierPanel;
    public Text MultiplierText;
    public Button ResultsContinueButton;
    public Button ResultsBackButton;


    private double roundScore;

    void Awake()
    {
        BubbleGameManagerStatic.bubbleGameManager = this;
    }

    public void DoNextRound() {
        ResultsCanvas.GetComponent<BubbleResults>().GoNextRound();
    }

    public void ShowResults(bool roundPassed) {
        if (!roundPassed) {
            ResultsCanvas.GetComponentInChildren<Animator>().enabled = false;
        } else {
            ResultsCanvas.GetComponentInChildren<Animator>().enabled = true;
        }

        if (roundPassed) {
            roundScore = Math.Round(Timer.GetComponent<Timer>().RoundTimeLeft() / GameControl.control.GetRoundTime() * 100);
            SetResultsButtons(false);
        } else {
            SetExpMultiplier(false);
            SetResultsButtons(true);
            roundScore = 0;
        }
        
        
        ResultsCanvas.GetComponent<BubbleResults>().SetRoundScore(roundScore.ToString());
        ResultsCanvas.SetActive(true);
    }

    public double GetRoundScore() {
        return roundScore;
    }

    public void SetExpMultiplier(bool turnOn) {
        if (turnOn) {
            MultiplierPanel.SetActive(true);
            MultiplierText.text = "x" + GameControl.control.expMult.ToString();
        } else {
            MultiplierPanel.SetActive(false);
        }
    }

    public void SetResultsButtons(bool turnOn) {
        if (turnOn) {
            ResultsContinueButton.interactable = true;
            ResultsBackButton.interactable = true;
        } else {
            ResultsContinueButton.interactable = false;
            ResultsBackButton.interactable = false;
        }
    }
}
