﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectiveText : MonoBehaviour
{
    public Timer timer;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //timerState = timer.GetComponent<Timer>().timerState;
        animator.SetInteger("State", timer.GetComponent<Timer>().timerState);
    }

    // Update is called once per frame
    void Update()
    {
        //timerState = timer.GetComponent<Timer>().timerState;
        animator.SetInteger("State", timer.GetComponent<Timer>().timerState);
    }
}
