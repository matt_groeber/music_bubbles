﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour
{

    public void LoadByIndex(int sceneIndex)
    {
        GameControl.control.Save();     // save settings whenever changing scenes
        SceneManager.LoadScene(sceneIndex);
    }
    
    public void ExitGame()
    {
        GameControl.control.Save();

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
