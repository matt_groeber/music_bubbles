﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleResults : MonoBehaviour
{
    public GameObject ResultsCanvas;
    public GameObject[] stars;
    public MainCamera theMainCamera;
    public GameObject RoundScore;

    public Color starColor;
    public Color starBorderColor;
    public Color starShadowColor;


    public void GoNextRound() {
        
        // return stars to default (gray = off) color and turn off particle effects
        foreach(GameObject star in stars) {
            star.GetComponent<Image>().color = starColor;
            star.GetComponent<Outline>().effectColor = starBorderColor;
            star.GetComponent<Shadow>().effectColor = starShadowColor;
            star.transform.GetChild(0).gameObject.SetActive(false);
        }
        
        ResultsCanvas.SetActive(false);

        theMainCamera.StartRound();
    }

    public void SetRoundScore(string value) {
        RoundScore.GetComponent<Text>().text = value;
    }
    
}
