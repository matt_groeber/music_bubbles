﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveGUI : MonoBehaviour
{

    public Text PlayerLevelText;
    public Image experienceBar;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerLevelText.text = GameControl.control.level.ToString();
        experienceBar.fillAmount = GameControl.control.GetLevelProgression();
    }
}
