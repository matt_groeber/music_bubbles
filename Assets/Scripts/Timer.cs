﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    Image timerBar;
    float timeLeft;
    float introTime;
    float roundTime;
    public GameObject StartGameLeadText;
    public GameObject StartGameObjectiveText;
    public GameObject EndTimeText;
    public Animator objectiveAnimator;
    public static bool runTimer;

    public static bool doingIntro;
    public MainCamera mainCamera;
    public AudioSource audioSource;

    public int timerState;

    // Start is called before the first frame update
    void Start()
    {
        timerState = 0;
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void StartTimer() {
        introTime = 2f;
        roundTime = GameControl.control.GetRoundTime();
        StartGameLeadText.SetActive(true);
        StartGameObjectiveText.SetActive(true);
        
        StartGameObjectiveText.GetComponent<Text>().text = MainCamera.theAnswer.ToUpper();
        EndTimeText.SetActive(false);
        this.GetComponent<Image>().enabled = false;
        timerBar = GetComponent<Image>();
        timeLeft = introTime;
        doingIntro = true;
        runTimer = true;
        MainCamera.gameOn = true;

        timerState = 1;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (runTimer) {
            if (timeLeft > 0) {
                timeLeft -= Time.deltaTime;

                if (!doingIntro) {
                    timerState = 2;
                    this.GetComponent<Image>().enabled = true;
                    timerBar.fillAmount = timeLeft / roundTime;
                }
            } else {
                if (doingIntro) {
                    StartGameLeadText.SetActive(false);
                    objectiveAnimator.enabled = true;

                    mainCamera.ShowBalls();
                    doingIntro = false;
                    timeLeft = roundTime;
                } else {
                    timerState = 3;
                    GameControl.control.IncorrectAnswer();
                    GameControl.control.Save();
                    EndTimeText.SetActive(true);
                    audioSource.PlayOneShot(Resources.Load<AudioClip>("SoundEffects/studio audience awwww sound FX"));
                    StartCoroutine(LoadLevelAfterDelay(3f));
                    //SceneManager.LoadScene("MainMenu");
                    //Time.timeScale = 0;
                    runTimer = false;
                }
            }
        } else {
            timerState = 3;
        }
        
    }
    
    public float RoundTimeLeft() {
        return timeLeft;
    }
    
    IEnumerator LoadLevelAfterDelay(float delay) {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("MainMenu");
        timerState = 4;
    }
    
}
