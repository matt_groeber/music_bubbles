﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleBlue : MonoBehaviour
{
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void StartOfBubbleAnimation()
    {
        animator.speed = Random.Range(1, 3);
    }

    public void StopAnimating()
    {
        animator.speed = 0;
    }
    
}
