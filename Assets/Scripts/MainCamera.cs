﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public float colDepth = 40f;
    public float zPosition = 0f;
    public static string theAnswer;
    public static Vector2 screenSize;
    private Transform topCollider;
    private Transform bottomCollider;
    private Transform leftCollider;
    private Transform rightCollider;
    private Vector3 cameraPos;

    public Rigidbody2D ball;
    public Timer timer;
    public static List<Ball> ballList = new List<Ball>();
    public static bool gameOn = false;
    public static int numPossibleAnswers;
    public static int numCorrectAnswers;
    public static System.Random rand = new System.Random();
    
    public List<Note> answerList = new List<Note>();
    public List<Note> answerListCopy = new List<Note>();

    private float ballDiameter;
    private float fbpToNegX;
    private float fbpToPosX;
    private float fbpToNegY;
    private float fbpToPosY;


    // Start is called before the first frame update
    void Start() {
        GameControl.control.Load();
        //Generate our empty objects
        topCollider = new GameObject().transform;
        bottomCollider = new GameObject().transform;
        rightCollider = new GameObject().transform;
        leftCollider = new GameObject().transform;

        //Name our objects 
        topCollider.name = "TopCollider";
        bottomCollider.name = "BottomCollider";
        rightCollider.name = "RightCollider";
        leftCollider.name = "LeftCollider";

        //Add the colliders
        topCollider.gameObject.AddComponent<BoxCollider2D>();
        bottomCollider.gameObject.AddComponent<BoxCollider2D>();
        rightCollider.gameObject.AddComponent<BoxCollider2D>();
        leftCollider.gameObject.AddComponent<BoxCollider2D>();

        //Make them the child of whatever object this script is on, preferably on the Camera so the objects move with the camera without extra scripting
        topCollider.parent = transform;
        bottomCollider.parent = transform;
        rightCollider.parent = transform;
        leftCollider.parent = transform;

        //Generate world space point information for position and scale calculations
        cameraPos = Camera.main.transform.position;
        screenSize.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
        screenSize.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;

        //Change our scale and positions to match the edges of the screen...   
        rightCollider.localScale = new Vector3(colDepth, screenSize.y * 2, colDepth);
        rightCollider.position = new Vector3(cameraPos.x + screenSize.x + (rightCollider.localScale.x * 0.5f), cameraPos.y, zPosition);
        leftCollider.localScale = new Vector3(colDepth, screenSize.y * 2, colDepth);
        leftCollider.position = new Vector3(cameraPos.x - screenSize.x - (leftCollider.localScale.x * 0.5f), cameraPos.y, zPosition);
        topCollider.localScale = new Vector3(screenSize.x * 2, colDepth, colDepth);
        topCollider.position = new Vector3(cameraPos.x, cameraPos.y + screenSize.y + (topCollider.localScale.y * 0.5f), zPosition);
        bottomCollider.localScale = new Vector3(screenSize.x * 2, colDepth, colDepth);
        bottomCollider.position = new Vector3(cameraPos.x, cameraPos.y - screenSize.y - (bottomCollider.localScale.y * 0.5f), zPosition);

        ballDiameter = ball.GetComponent<CircleCollider2D>().radius * 2;
        fbpToNegX = -screenSize.x + ballDiameter;
        fbpToPosX = screenSize.x - ballDiameter;
        fbpToNegY = -screenSize.y + ballDiameter;
        fbpToPosY = screenSize.y - ballDiameter;

        //StartRound();
        BubbleGameManagerStatic.bubbleGameManager.DoNextRound();
    }

    private void GenBall(float fbpToNegX, float fbpToPosX, float fbpToNegY, float fbpToPosY, Note note)
    {
        Rigidbody2D newBall = Instantiate(ball, new Vector3(UnityEngine.Random.Range(fbpToNegX, fbpToPosX), UnityEngine.Random.Range(fbpToNegY, fbpToPosY), 0), Quaternion.identity);
        newBall.GetComponent<Ball>().note = note;

        ballList.Add(newBall.GetComponent<Ball>());
    }

    public void StartRound() {

        answerList = GameControl.control.GetNoteList();
        answerListCopy = new List<Note>();

        foreach (Ball ball in ballList) {
            if (!ball.Equals(null)) {
                Destroy(ball.gameObject);
            }
        }
        ballList.Clear();

        int answerIndex = rand.Next(answerList.Count);
        theAnswer = answerList[answerIndex].theNote;

        for(int ii = 0; ii < answerList.Count; ii++) {
            if(ii != answerIndex) {
                answerListCopy.Add(answerList[ii]);
            }
        }

        //answerListCopy.Remove(answerList[answerIndex]);

        int totNumBalls = GameControl.control.GetNumBubbles();
        numPossibleAnswers = rand.Next(2, totNumBalls - 1);
        GameControl.control.SetNumAnswers(numPossibleAnswers);
        numCorrectAnswers = 0;
        //Note newNote = new Note();
        Note newNote = gameObject.AddComponent<Note>();

        //Generate Balls
        for (var ii = 1; ii <= totNumBalls; ii++) {

            if (ii <= numPossibleAnswers) {
                newNote = answerList[answerIndex];
            } else {

                // this ensures two balls will be the same wrong answer if more than 3 balls
                if(ii != numPossibleAnswers + 2) {
                    newNote = answerListCopy[rand.Next(answerListCopy.Count)];
                }
                    
            }


            GenBall(fbpToNegX, fbpToPosX, fbpToNegY, fbpToPosY, newNote);

        }
        HideBalls();
        timer.StartTimer();
    }

    public void HideBalls() {
        foreach(Ball ball in ballList) {
            ball.gameObject.SetActive(false);
        }
    }

    public void ShowBalls() {
        foreach (Ball ball in ballList) {
            ball.gameObject.SetActive(true);
        }
    }

    public void ClearRound() {
        ballList.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
