﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    private readonly String[] baseNotes = { "C", "D", "E", "F", "G", "A", "B" };
    private readonly String[] notes = { "C", "C#/D♭", "D", "D#/E♭", "E", "F", "F#/G♭", "G", "G#/A♭", "A", "A#/B♭", "B" };

    private int _octave;
    private string _note;
    private bool hasAccidentals;

    public Note() {

    }

    public void SetNote(int noteIndex) {
        // a noteIndex of 0 = C0
        _note = notes[noteIndex % notes.Length];
        _octave = (int)Math.Floor((float)noteIndex / notes.Length);

        if(_note.Length > 1) {
            hasAccidentals = true;
        } else {
            hasAccidentals = false;
        }
    }

    public string theNote {
        get { return _note; }
    }

    public int Octave {
        get { return _octave; }
    }
    
}
