﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Ball : MonoBehaviour
{
    public Rigidbody2D ball;
    public Vector2 velocity;
    public AudioClip MusicClip;
    public GameObject collideEffectObj;
    public GameObject popEffectObj;
    public MainCamera theMC;
    public BubbleBlue bubbleBlue;

    public AudioSource collisionAudioSource;
    public AudioSource noteAudioSource;
    public AudioClip[] cseArr;

    public Note note;

    // Start is called before the first frame update
    void Start()
    {
        if(note.theNote == "")
        {
            Destroy(gameObject);
        }

        float smallestCamDimension = MainCamera.screenSize.x;
        float sizeDivider;
        float screenArea = MainCamera.screenSize.x * MainCamera.screenSize.y;
        float areaPerBall = screenArea / (float)(MainCamera.ballList.Count * 1.3);
        float SQRTapb = (float)Math.Sqrt(areaPerBall);

        if (MainCamera.screenSize.y < MainCamera.screenSize.x)
        {
            smallestCamDimension = MainCamera.screenSize.y;
            sizeDivider = UnityEngine.Random.Range(1.7f, 2.2f);

        } else {
            
            sizeDivider = UnityEngine.Random.Range(1.7f, 2.2f);
        }
        
        ////////this.transform.localScale = new Vector2(smallestCamDimension / sizeDivider, smallestCamDimension / sizeDivider);

        float ballSize = UnityEngine.Random.Range(SQRTapb - 0.2f, SQRTapb + 0.2f);
        this.transform.localScale = new Vector2(ballSize, ballSize);

        Vector3 impulse = new Vector3((UnityEngine.Random.value - 0.5f) * 3, (UnityEngine.Random.value - 0.5f) * 3, 0.0f);
        ball.gameObject.GetComponent<MeshRenderer>().enabled = true;
        ball.AddForce(impulse, ForceMode2D.Impulse);
        GetComponent<AudioSource>().playOnAwake = false;

        AudioSource[] audioSources = gameObject.GetComponents<AudioSource>();
        collisionAudioSource = audioSources[0];
        noteAudioSource = audioSources[1];
        //GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("GuitarSounds/" + note + "4");
        ball.GetComponentsInChildren<SpriteRenderer>()[1].sprite = Resources.Load<Sprite>("Images/gclef_" + note.theNote.ToLower() + note.Octave);
    }


    private void OnMouseDown()
    {
        if (MainCamera.gameOn) {
            Animator animator = this.GetComponent<Animator>();
            animator.enabled = true;
        }
    }

    private void popBubble()
    {
        this.gameObject.GetComponent<Animator>().speed = 0;
        DoParticles(popEffectObj, new Vector3(this.transform.position.x, this.transform.position.y, 0f));

        if (MainCamera.gameOn)
        {
            if (MainCamera.theAnswer == this.note.theNote)
            {
                GameControl.control.CorrectAnswer();
                noteAudioSource.volume = GameControl.control.sfxVolume;
                noteAudioSource.PlayOneShot(Resources.Load<AudioClip>("GuitarSounds/" + note.theNote.ToLower() + note.Octave));
                Destroy(this.gameObject, 3f);
                MainCamera.numCorrectAnswers += 1;

                if(MainCamera.numCorrectAnswers == MainCamera.numPossibleAnswers) {
                    
                    GameControl.control.Save();
                    //noteAudioSource.PlayOneShot(Resources.Load<AudioClip>("SoundEffects/1_person_cheering-Jett_Rifkin-1851518140"));
                    //noteAudioSource.PlayOneShot(Resources.Load<AudioClip>("SoundEffects/smb_coin"));
                    //noteAudioSource.PlayOneShot(Resources.Load<AudioClip>("SoundEffects/JackOhYeahYeah"));
                    MainCamera.gameOn = false;
                    Timer.runTimer = false;
                    EndRound(true);
                    //StartCoroutine(StartNextRoundAfterDelay(2.5f));
                }
            }
            else
            {
                GameControl.control.IncorrectAnswer();
                GameControl.control.Save();
                foreach (Ball ball in MainCamera.ballList)
                {
                    if (!ball.Equals(null))
                    {
                        Animator animator = ball.GetComponent<Animator>();
                        animator.enabled = true;
                        DoParticles(popEffectObj, new Vector3(ball.transform.position.x, ball.transform.position.y, 0f));
                        Destroy(ball.gameObject, 4f);
                    }

                }
                MainCamera.gameOn = false;
                Timer.runTimer = false;
                noteAudioSource.volume = GameControl.control.sfxVolume;
                noteAudioSource.PlayOneShot(Resources.Load<AudioClip>("SoundEffects/AwwMan"));
                //StartCoroutine(LoadLevelAfterDelay(2f));
                EndRound(false);
            }
        }
    }

    private void DoParticles(GameObject gameObj, Vector3 location)
    {
        if (MainCamera.gameOn)
        {
            GameObject effect = Instantiate(gameObj, new Vector3(location.x, location.y, 0f), Quaternion.identity);
        }
    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (MainCamera.gameOn && collisionAudioSource != null)
        {
            collisionAudioSource.volume = GameControl.control.sfxVolume;
            collisionAudioSource.PlayOneShot(cseArr[UnityEngine.Random.Range(0, cseArr.Length)]);

            ContactPoint2D contactPt = collision.GetContact(0);
            DoParticles(collideEffectObj, new Vector3(contactPt.point.x, contactPt.point.y, 0f));
        }
    }

    public Rect BoundsToScreenRect(Bounds bounds) {
        // Get mesh origin and farthest extent (this works best with simple convex meshes)
        Vector3 origin = Camera.main.WorldToScreenPoint(new Vector3(bounds.min.x, bounds.max.y, 0f));
        Vector3 extent = Camera.main.WorldToScreenPoint(new Vector3(bounds.max.x, bounds.min.y, 0f));

        // Create rect in screen space and return - does not account for camera perspective
        return new Rect(origin.x, Screen.height - origin.y, extent.x - origin.x, origin.y - extent.y);
    }

    public void EndRound(bool roundPassed) {
        BubbleGameManagerStatic.bubbleGameManager.ShowResults(roundPassed);
    }

    IEnumerator StartNextRoundAfterDelay(float delay) {
        yield return new WaitForSeconds(delay);
        GameObject mcObj = GameObject.Find("Main Camera");
        MainCamera obj2 = mcObj.GetComponent<MainCamera>();
        obj2.StartRound();
    }

    IEnumerator LoadLevelAfterDelay(float delay) {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("MainMenu");
    }
}
