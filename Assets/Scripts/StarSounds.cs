﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSounds : MonoBehaviour
{

    public AudioClip[] clips;
    public AudioSource audioSource;
    public Animator ResultsPanelAnimator;
    

    // Start is called before the first frame update
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayOneStar() {
        audioSource.volume = GameControl.control.sfxVolume;
        audioSource.PlayOneShot(clips[0]);

        bool twoStarScore = (BubbleGameManagerStatic.bubbleGameManager.GetRoundScore() >= 25) ? true : false;

        // turn off multiplier gui iff only 1 star
        if (!twoStarScore) {
            GameControl.control.expMult = 1;
            BubbleGameManagerStatic.bubbleGameManager.SetExpMultiplier(false);
            BubbleGameManagerStatic.bubbleGameManager.SetResultsButtons(true);
        }
        ResultsPanelAnimator.SetBool("starOneFinished", twoStarScore);
    }

    public void PlayTwoStar() {
        audioSource.volume = GameControl.control.sfxVolume;
        audioSource.PlayOneShot(clips[1]);
        bool threeStarScore = (BubbleGameManagerStatic.bubbleGameManager.GetRoundScore() >= 70) ? true : false;

        // turn off multiplier gui iff 2 star
        if (!threeStarScore) {
            GameControl.control.expMult = 1;
            BubbleGameManagerStatic.bubbleGameManager.SetExpMultiplier(false);
            BubbleGameManagerStatic.bubbleGameManager.SetResultsButtons(true);
        }
        ResultsPanelAnimator.SetBool("starTwoFinished", threeStarScore);
    }

    public void PlayThreeStar() {
        GameControl.control.expMult += (GameControl.control.expMult >= 5) ? 0 : 1;
        BubbleGameManagerStatic.bubbleGameManager.SetExpMultiplier(true);
        BubbleGameManagerStatic.bubbleGameManager.SetResultsButtons(true);
        audioSource.volume = GameControl.control.sfxVolume;
        audioSource.PlayOneShot(clips[2]);
        audioSource.PlayOneShot(Resources.Load<AudioClip>("SoundEffects/JackOhYeahYeah"));
    }
}
