﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameControl : MonoBehaviour
{
    public static GameControl control;

    public int level;
    public float experience;
    public int expMult;
    public float musicVolume;
    public float sfxVolume;

    private GUIStyle guiStyle = new GUIStyle();
    public Font guiFont;

    private int numPossibleAnswers = 2;
    private readonly int[] expToLvl_BL = { 80, 120, 160, 200, 240, 280, 320, 360, 400, 440, 480, 520, 560, 600, 640, 680, 720, 760, 800, 840, 880, 920, 960, 1000, 1040, 1080, 1120 };
    private readonly int[] numBubbles_BL = { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12 };
    private readonly int[] numPossNotes_BL = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 };
    private readonly float[] roundTimePerAns_BL = { 4f, 3.5f, 3f, 2.75f, 2.75f, 2.75f, 2.5f, 2.5f, 2.5f, 2.25f, 2.25f, 2.25f, 2f, 2f, 2f, 1.75f, 1.75f, 1.75f, 1.5f, 1.5f, 1.5f, 1.25f, 1.25f, 1.25f, 1f, 1f, 1f };

    private readonly int[] notesToInclude = { 64, 65, 67, 59, 60, 62, 55, 57, 50, 52, 53, 45, 47, 48, 40, 41, 43, 66, 68, 61, 63, 56, 58, 51, 54, 46, 49, 42, 44 };
    

    private Color guiColor = new Color(0.3520313f, 0.3584906f, 0f);

    void Awake () {
        if(control == null) {
            DontDestroyOnLoad(gameObject);
            control = this;
        } else if (control != this) {
            Destroy(gameObject);
        }
    }

    private void OnGUI() {
        /*GUIStyle playerGUIStyle = new GUIStyle();
        playerGUIStyle.fontSize = FontSizeScaled();
        playerGUIStyle.font = guiFont;
        playerGUIStyle.normal.textColor = guiColor;


        GUI.Label(new Rect(555, 13, 100, 30), "" + level, playerGUIStyle);
        GUI.Label(new Rect(510, 80, 150, 30), "E: " + experience, playerGUIStyle);*/

    }

    private int FontSizeScaled() {
        Vector2 nativeSize = new Vector2(1920, 1080);
        int fontSize;

        if(Screen.width > Screen.height) {
            fontSize = (int)(57f * (float)Screen.width / (float)nativeSize.x);
        } else {
            fontSize = (int)(42f * (float)Screen.height / (float)nativeSize.y);
        }
        
        return fontSize;
    }


    public int GetNumBubbles() {
        return numBubbles_BL[GetLevelBase()];
    }

    public float GetRoundTime() {
        return roundTimePerAns_BL[GetLevelBase()] * numPossibleAnswers;
    }

    public int GetLevelBase() {
        return (int)Math.Floor((float)level / 10);
    }

    public float GetLevelProgression() {
        return experience / expToLvl_BL[GetLevelBase()];
    }

    public void SetNumAnswers(int numAnswers) {
        numPossibleAnswers = numAnswers;
    }

    public List<Note> GetNoteList() {
        List<Note> retList = new List<Note>();

        for (int ii = 0; ii < numPossNotes_BL[GetLevelBase()]; ii++) {
            Note newNote = gameObject.AddComponent<Note>();
            newNote.SetNote(notesToInclude[ii]);
            retList.Add(newNote);
        }

        return retList;
    }


    public void CorrectAnswer() {
        experience += 10 * expMult;
        
        if (experience >= expToLvl_BL[GetLevelBase()]) {
            LevelUp();
            experience = 0;
        }
    }

    public void IncorrectAnswer() {
        if(experience > 20) {
            experience -= 40;
        } else {
            experience = 0;
        }
    }

    public void LevelUp() {
        level += 1;
    }

    public void Save() {
        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData data = new PlayerData {
            level = level,
            experience = experience,
            expMult = expMult,
            musicVolume = musicVolume,
            sfxVolume = sfxVolume
        };

        //data.level = 1;

        bf.Serialize(file, data);
        file.Close();
    }

    public void Load() {
        if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData) bf.Deserialize(file);
            file.Close();

            level = data.level;
            experience = data.experience;
            expMult = data.expMult;
            musicVolume = data.musicVolume;
            sfxVolume = data.sfxVolume;
        } else {
            // defaults are set in game object
            //musicVolume = 0.3f;
            //sfxVolume = 0.7f;
            expMult = 1;

        }
    }
}

[Serializable]
class PlayerData
{
    public int level;
    public float experience;
    public int expMult;
    public float musicVolume;
    public float sfxVolume;
}

