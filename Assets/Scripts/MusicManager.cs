﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour
{

    public Slider musicSlider;
    public Slider sfxSlider;
    public AudioSource myMusic;

    public void Start() {
        GameControl.control.Load();
        musicSlider.value = GameControl.control.musicVolume;
        sfxSlider.value = GameControl.control.sfxVolume;
    }

    private void Update() {
        myMusic.volume = musicSlider.value;
        GameControl.control.musicVolume = musicSlider.value;
        GameControl.control.sfxVolume = sfxSlider.value;
    }
}
