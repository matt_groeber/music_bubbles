﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MainCamera
struct MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573;
// System.Collections.Generic.List`1<Ball>
struct List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6;
// System.Collections.Generic.List`1<Note>
struct List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Timer
struct Timer_tD11671555E440B2E0490D401C230CA3B19EDD798;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CCHECKFORCHANGEU3ED__2_T71710E97EFE51ED653E9F3FE4835F607C8324A01_H
#define U3CCHECKFORCHANGEU3ED__2_T71710E97EFE51ED653E9F3FE4835F607C8324A01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuLayout/<CheckForChange>d__2
struct  U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01  : public RuntimeObject
{
public:
	// System.Int32 MenuLayout/<CheckForChange>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MenuLayout/<CheckForChange>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKFORCHANGEU3ED__2_T71710E97EFE51ED653E9F3FE4835F607C8324A01_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CLOADLEVELAFTERDELAYU3ED__14_T8770C7FEF537DAA3A3A54000B456E5940BD58B5B_H
#define U3CLOADLEVELAFTERDELAYU3ED__14_T8770C7FEF537DAA3A3A54000B456E5940BD58B5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer/<LoadLevelAfterDelay>d__14
struct  U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B  : public RuntimeObject
{
public:
	// System.Int32 Timer/<LoadLevelAfterDelay>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Timer/<LoadLevelAfterDelay>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Timer/<LoadLevelAfterDelay>d__14::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLEVELAFTERDELAYU3ED__14_T8770C7FEF537DAA3A3A54000B456E5940BD58B5B_H
#ifndef __STATICARRAYINITTYPESIZEU3D108_T1D3815BD9654C521776B19932355EA4A81BD71FF_H
#define __STATICARRAYINITTYPESIZEU3D108_T1D3815BD9654C521776B19932355EA4A81BD71FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108
struct  __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF__padding[108];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D108_T1D3815BD9654C521776B19932355EA4A81BD71FF_H
#ifndef __STATICARRAYINITTYPESIZEU3D116_TA65347B6FD25750C4C660008486DF03F17E1C1B5_H
#define __STATICARRAYINITTYPESIZEU3D116_TA65347B6FD25750C4C660008486DF03F17E1C1B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=116
struct  __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5__padding[116];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D116_TA65347B6FD25750C4C660008486DF03F17E1C1B5_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::6FC197002B3B804BF6C339C44A94A60A8622BC93
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___6FC197002B3B804BF6C339C44A94A60A8622BC93_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::A265A6CED05B2457B5DB2B8AE4B7775CF3F75004
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::CAD08EB04AFAE771A9BE8CDA687EC435D84090A9
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::CF829E312D8BFDA60F0842C6A044BE77B6C5883D
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=116 <PrivateImplementationDetails>::EFE5D31872C3AD74482056589725763E97F501B9
	__StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5  ___EFE5D31872C3AD74482056589725763E97F501B9_4;

public:
	inline static int32_t get_offset_of_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6FC197002B3B804BF6C339C44A94A60A8622BC93_0)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0() const { return ___6FC197002B3B804BF6C339C44A94A60A8622BC93_0; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0() { return &___6FC197002B3B804BF6C339C44A94A60A8622BC93_0; }
	inline void set_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___6FC197002B3B804BF6C339C44A94A60A8622BC93_0 = value;
	}

	inline static int32_t get_offset_of_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1() const { return ___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1() { return &___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1; }
	inline void set_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1 = value;
	}

	inline static int32_t get_offset_of_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2() const { return ___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2() { return &___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2; }
	inline void set_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2 = value;
	}

	inline static int32_t get_offset_of_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3() const { return ___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3() { return &___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3; }
	inline void set_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3 = value;
	}

	inline static int32_t get_offset_of_EFE5D31872C3AD74482056589725763E97F501B9_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___EFE5D31872C3AD74482056589725763E97F501B9_4)); }
	inline __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5  get_EFE5D31872C3AD74482056589725763E97F501B9_4() const { return ___EFE5D31872C3AD74482056589725763E97F501B9_4; }
	inline __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5 * get_address_of_EFE5D31872C3AD74482056589725763E97F501B9_4() { return &___EFE5D31872C3AD74482056589725763E97F501B9_4; }
	inline void set_EFE5D31872C3AD74482056589725763E97F501B9_4(__StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5  value)
	{
		___EFE5D31872C3AD74482056589725763E97F501B9_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef MAINCAMERA_TFD85DD5C86E2E33A0D38306F77612F04983DC573_H
#define MAINCAMERA_TFD85DD5C86E2E33A0D38306F77612F04983DC573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainCamera
struct  MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MainCamera::colDepth
	float ___colDepth_4;
	// System.Single MainCamera::zPosition
	float ___zPosition_5;
	// UnityEngine.Transform MainCamera::topCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___topCollider_8;
	// UnityEngine.Transform MainCamera::bottomCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bottomCollider_9;
	// UnityEngine.Transform MainCamera::leftCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___leftCollider_10;
	// UnityEngine.Transform MainCamera::rightCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___rightCollider_11;
	// UnityEngine.Vector3 MainCamera::cameraPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cameraPos_12;
	// UnityEngine.Rigidbody2D MainCamera::ball
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___ball_13;
	// Timer MainCamera::timer
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * ___timer_14;
	// System.Collections.Generic.List`1<Note> MainCamera::answerList
	List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * ___answerList_20;
	// System.Collections.Generic.List`1<Note> MainCamera::answerListCopy
	List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * ___answerListCopy_21;
	// System.Single MainCamera::ballDiameter
	float ___ballDiameter_22;
	// System.Single MainCamera::fbpToNegX
	float ___fbpToNegX_23;
	// System.Single MainCamera::fbpToPosX
	float ___fbpToPosX_24;
	// System.Single MainCamera::fbpToNegY
	float ___fbpToNegY_25;
	// System.Single MainCamera::fbpToPosY
	float ___fbpToPosY_26;

public:
	inline static int32_t get_offset_of_colDepth_4() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___colDepth_4)); }
	inline float get_colDepth_4() const { return ___colDepth_4; }
	inline float* get_address_of_colDepth_4() { return &___colDepth_4; }
	inline void set_colDepth_4(float value)
	{
		___colDepth_4 = value;
	}

	inline static int32_t get_offset_of_zPosition_5() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___zPosition_5)); }
	inline float get_zPosition_5() const { return ___zPosition_5; }
	inline float* get_address_of_zPosition_5() { return &___zPosition_5; }
	inline void set_zPosition_5(float value)
	{
		___zPosition_5 = value;
	}

	inline static int32_t get_offset_of_topCollider_8() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___topCollider_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_topCollider_8() const { return ___topCollider_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_topCollider_8() { return &___topCollider_8; }
	inline void set_topCollider_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___topCollider_8 = value;
		Il2CppCodeGenWriteBarrier((&___topCollider_8), value);
	}

	inline static int32_t get_offset_of_bottomCollider_9() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___bottomCollider_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bottomCollider_9() const { return ___bottomCollider_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bottomCollider_9() { return &___bottomCollider_9; }
	inline void set_bottomCollider_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bottomCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&___bottomCollider_9), value);
	}

	inline static int32_t get_offset_of_leftCollider_10() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___leftCollider_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_leftCollider_10() const { return ___leftCollider_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_leftCollider_10() { return &___leftCollider_10; }
	inline void set_leftCollider_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___leftCollider_10 = value;
		Il2CppCodeGenWriteBarrier((&___leftCollider_10), value);
	}

	inline static int32_t get_offset_of_rightCollider_11() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___rightCollider_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_rightCollider_11() const { return ___rightCollider_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_rightCollider_11() { return &___rightCollider_11; }
	inline void set_rightCollider_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___rightCollider_11 = value;
		Il2CppCodeGenWriteBarrier((&___rightCollider_11), value);
	}

	inline static int32_t get_offset_of_cameraPos_12() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___cameraPos_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cameraPos_12() const { return ___cameraPos_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cameraPos_12() { return &___cameraPos_12; }
	inline void set_cameraPos_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cameraPos_12 = value;
	}

	inline static int32_t get_offset_of_ball_13() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___ball_13)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_ball_13() const { return ___ball_13; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_ball_13() { return &___ball_13; }
	inline void set_ball_13(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___ball_13 = value;
		Il2CppCodeGenWriteBarrier((&___ball_13), value);
	}

	inline static int32_t get_offset_of_timer_14() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___timer_14)); }
	inline Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * get_timer_14() const { return ___timer_14; }
	inline Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 ** get_address_of_timer_14() { return &___timer_14; }
	inline void set_timer_14(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * value)
	{
		___timer_14 = value;
		Il2CppCodeGenWriteBarrier((&___timer_14), value);
	}

	inline static int32_t get_offset_of_answerList_20() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___answerList_20)); }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * get_answerList_20() const { return ___answerList_20; }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 ** get_address_of_answerList_20() { return &___answerList_20; }
	inline void set_answerList_20(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * value)
	{
		___answerList_20 = value;
		Il2CppCodeGenWriteBarrier((&___answerList_20), value);
	}

	inline static int32_t get_offset_of_answerListCopy_21() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___answerListCopy_21)); }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * get_answerListCopy_21() const { return ___answerListCopy_21; }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 ** get_address_of_answerListCopy_21() { return &___answerListCopy_21; }
	inline void set_answerListCopy_21(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * value)
	{
		___answerListCopy_21 = value;
		Il2CppCodeGenWriteBarrier((&___answerListCopy_21), value);
	}

	inline static int32_t get_offset_of_ballDiameter_22() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___ballDiameter_22)); }
	inline float get_ballDiameter_22() const { return ___ballDiameter_22; }
	inline float* get_address_of_ballDiameter_22() { return &___ballDiameter_22; }
	inline void set_ballDiameter_22(float value)
	{
		___ballDiameter_22 = value;
	}

	inline static int32_t get_offset_of_fbpToNegX_23() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToNegX_23)); }
	inline float get_fbpToNegX_23() const { return ___fbpToNegX_23; }
	inline float* get_address_of_fbpToNegX_23() { return &___fbpToNegX_23; }
	inline void set_fbpToNegX_23(float value)
	{
		___fbpToNegX_23 = value;
	}

	inline static int32_t get_offset_of_fbpToPosX_24() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToPosX_24)); }
	inline float get_fbpToPosX_24() const { return ___fbpToPosX_24; }
	inline float* get_address_of_fbpToPosX_24() { return &___fbpToPosX_24; }
	inline void set_fbpToPosX_24(float value)
	{
		___fbpToPosX_24 = value;
	}

	inline static int32_t get_offset_of_fbpToNegY_25() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToNegY_25)); }
	inline float get_fbpToNegY_25() const { return ___fbpToNegY_25; }
	inline float* get_address_of_fbpToNegY_25() { return &___fbpToNegY_25; }
	inline void set_fbpToNegY_25(float value)
	{
		___fbpToNegY_25 = value;
	}

	inline static int32_t get_offset_of_fbpToPosY_26() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToPosY_26)); }
	inline float get_fbpToPosY_26() const { return ___fbpToPosY_26; }
	inline float* get_address_of_fbpToPosY_26() { return &___fbpToPosY_26; }
	inline void set_fbpToPosY_26(float value)
	{
		___fbpToPosY_26 = value;
	}
};

struct MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields
{
public:
	// System.String MainCamera::theAnswer
	String_t* ___theAnswer_6;
	// UnityEngine.Vector2 MainCamera::screenSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenSize_7;
	// System.Collections.Generic.List`1<Ball> MainCamera::ballList
	List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * ___ballList_15;
	// System.Boolean MainCamera::gameOn
	bool ___gameOn_16;
	// System.Int32 MainCamera::numPossibleAnswers
	int32_t ___numPossibleAnswers_17;
	// System.Int32 MainCamera::numCorrectAnswers
	int32_t ___numCorrectAnswers_18;
	// System.Random MainCamera::rand
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___rand_19;

public:
	inline static int32_t get_offset_of_theAnswer_6() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___theAnswer_6)); }
	inline String_t* get_theAnswer_6() const { return ___theAnswer_6; }
	inline String_t** get_address_of_theAnswer_6() { return &___theAnswer_6; }
	inline void set_theAnswer_6(String_t* value)
	{
		___theAnswer_6 = value;
		Il2CppCodeGenWriteBarrier((&___theAnswer_6), value);
	}

	inline static int32_t get_offset_of_screenSize_7() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___screenSize_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenSize_7() const { return ___screenSize_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenSize_7() { return &___screenSize_7; }
	inline void set_screenSize_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenSize_7 = value;
	}

	inline static int32_t get_offset_of_ballList_15() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___ballList_15)); }
	inline List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * get_ballList_15() const { return ___ballList_15; }
	inline List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 ** get_address_of_ballList_15() { return &___ballList_15; }
	inline void set_ballList_15(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * value)
	{
		___ballList_15 = value;
		Il2CppCodeGenWriteBarrier((&___ballList_15), value);
	}

	inline static int32_t get_offset_of_gameOn_16() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___gameOn_16)); }
	inline bool get_gameOn_16() const { return ___gameOn_16; }
	inline bool* get_address_of_gameOn_16() { return &___gameOn_16; }
	inline void set_gameOn_16(bool value)
	{
		___gameOn_16 = value;
	}

	inline static int32_t get_offset_of_numPossibleAnswers_17() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___numPossibleAnswers_17)); }
	inline int32_t get_numPossibleAnswers_17() const { return ___numPossibleAnswers_17; }
	inline int32_t* get_address_of_numPossibleAnswers_17() { return &___numPossibleAnswers_17; }
	inline void set_numPossibleAnswers_17(int32_t value)
	{
		___numPossibleAnswers_17 = value;
	}

	inline static int32_t get_offset_of_numCorrectAnswers_18() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___numCorrectAnswers_18)); }
	inline int32_t get_numCorrectAnswers_18() const { return ___numCorrectAnswers_18; }
	inline int32_t* get_address_of_numCorrectAnswers_18() { return &___numCorrectAnswers_18; }
	inline void set_numCorrectAnswers_18(int32_t value)
	{
		___numCorrectAnswers_18 = value;
	}

	inline static int32_t get_offset_of_rand_19() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___rand_19)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_rand_19() const { return ___rand_19; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_rand_19() { return &___rand_19; }
	inline void set_rand_19(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___rand_19 = value;
		Il2CppCodeGenWriteBarrier((&___rand_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINCAMERA_TFD85DD5C86E2E33A0D38306F77612F04983DC573_H
#ifndef MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#define MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifndef MENULAYOUT_TF5F4902CBDEF9388989C7F621F64B424BCA978B7_H
#define MENULAYOUT_TF5F4902CBDEF9388989C7F621F64B424BCA978B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuLayout
struct  MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENULAYOUT_TF5F4902CBDEF9388989C7F621F64B424BCA978B7_H
#ifndef MUSICMANAGER_TA48147377617955FE0763C9A03D97A97DAA15879_H
#define MUSICMANAGER_TA48147377617955FE0763C9A03D97A97DAA15879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicManager
struct  MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider MusicManager::musicSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___musicSlider_4;
	// UnityEngine.UI.Slider MusicManager::sfxSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___sfxSlider_5;
	// UnityEngine.AudioSource MusicManager::myMusic
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___myMusic_6;

public:
	inline static int32_t get_offset_of_musicSlider_4() { return static_cast<int32_t>(offsetof(MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879, ___musicSlider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_musicSlider_4() const { return ___musicSlider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_musicSlider_4() { return &___musicSlider_4; }
	inline void set_musicSlider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___musicSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___musicSlider_4), value);
	}

	inline static int32_t get_offset_of_sfxSlider_5() { return static_cast<int32_t>(offsetof(MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879, ___sfxSlider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_sfxSlider_5() const { return ___sfxSlider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_sfxSlider_5() { return &___sfxSlider_5; }
	inline void set_sfxSlider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___sfxSlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___sfxSlider_5), value);
	}

	inline static int32_t get_offset_of_myMusic_6() { return static_cast<int32_t>(offsetof(MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879, ___myMusic_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_myMusic_6() const { return ___myMusic_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_myMusic_6() { return &___myMusic_6; }
	inline void set_myMusic_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___myMusic_6 = value;
		Il2CppCodeGenWriteBarrier((&___myMusic_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUSICMANAGER_TA48147377617955FE0763C9A03D97A97DAA15879_H
#ifndef NOTE_TB8381BB2C942DB3B8911836FC539545E83D4D43B_H
#define NOTE_TB8381BB2C942DB3B8911836FC539545E83D4D43B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Note
struct  Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] Note::baseNotes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___baseNotes_4;
	// System.String[] Note::notes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___notes_5;
	// System.Int32 Note::_octave
	int32_t ____octave_6;
	// System.String Note::_note
	String_t* ____note_7;
	// System.Boolean Note::hasAccidentals
	bool ___hasAccidentals_8;

public:
	inline static int32_t get_offset_of_baseNotes_4() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ___baseNotes_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_baseNotes_4() const { return ___baseNotes_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_baseNotes_4() { return &___baseNotes_4; }
	inline void set_baseNotes_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___baseNotes_4 = value;
		Il2CppCodeGenWriteBarrier((&___baseNotes_4), value);
	}

	inline static int32_t get_offset_of_notes_5() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ___notes_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_notes_5() const { return ___notes_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_notes_5() { return &___notes_5; }
	inline void set_notes_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___notes_5 = value;
		Il2CppCodeGenWriteBarrier((&___notes_5), value);
	}

	inline static int32_t get_offset_of__octave_6() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ____octave_6)); }
	inline int32_t get__octave_6() const { return ____octave_6; }
	inline int32_t* get_address_of__octave_6() { return &____octave_6; }
	inline void set__octave_6(int32_t value)
	{
		____octave_6 = value;
	}

	inline static int32_t get_offset_of__note_7() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ____note_7)); }
	inline String_t* get__note_7() const { return ____note_7; }
	inline String_t** get_address_of__note_7() { return &____note_7; }
	inline void set__note_7(String_t* value)
	{
		____note_7 = value;
		Il2CppCodeGenWriteBarrier((&____note_7), value);
	}

	inline static int32_t get_offset_of_hasAccidentals_8() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ___hasAccidentals_8)); }
	inline bool get_hasAccidentals_8() const { return ___hasAccidentals_8; }
	inline bool* get_address_of_hasAccidentals_8() { return &___hasAccidentals_8; }
	inline void set_hasAccidentals_8(bool value)
	{
		___hasAccidentals_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTE_TB8381BB2C942DB3B8911836FC539545E83D4D43B_H
#ifndef PSBUBBLECOLLISION_T0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA_H
#define PSBUBBLECOLLISION_T0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PSbubbleCollision
struct  PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem PSbubbleCollision::ps
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___ps_4;

public:
	inline static int32_t get_offset_of_ps_4() { return static_cast<int32_t>(offsetof(PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA, ___ps_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_ps_4() const { return ___ps_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_ps_4() { return &___ps_4; }
	inline void set_ps_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___ps_4 = value;
		Il2CppCodeGenWriteBarrier((&___ps_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PSBUBBLECOLLISION_T0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA_H
#ifndef TIMER_TD11671555E440B2E0490D401C230CA3B19EDD798_H
#define TIMER_TD11671555E440B2E0490D401C230CA3B19EDD798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer
struct  Timer_tD11671555E440B2E0490D401C230CA3B19EDD798  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image Timer::timerBar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___timerBar_4;
	// System.Single Timer::timeLeft
	float ___timeLeft_5;
	// System.Single Timer::introTime
	float ___introTime_6;
	// System.Single Timer::roundTime
	float ___roundTime_7;
	// UnityEngine.GameObject Timer::StartGameLeadText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___StartGameLeadText_8;
	// UnityEngine.GameObject Timer::StartGameObjectiveText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___StartGameObjectiveText_9;
	// UnityEngine.GameObject Timer::EndTimeText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EndTimeText_10;
	// System.Boolean Timer::doingIntro
	bool ___doingIntro_12;
	// MainCamera Timer::mainCamera
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * ___mainCamera_13;
	// UnityEngine.AudioSource Timer::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_14;

public:
	inline static int32_t get_offset_of_timerBar_4() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___timerBar_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_timerBar_4() const { return ___timerBar_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_timerBar_4() { return &___timerBar_4; }
	inline void set_timerBar_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___timerBar_4 = value;
		Il2CppCodeGenWriteBarrier((&___timerBar_4), value);
	}

	inline static int32_t get_offset_of_timeLeft_5() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___timeLeft_5)); }
	inline float get_timeLeft_5() const { return ___timeLeft_5; }
	inline float* get_address_of_timeLeft_5() { return &___timeLeft_5; }
	inline void set_timeLeft_5(float value)
	{
		___timeLeft_5 = value;
	}

	inline static int32_t get_offset_of_introTime_6() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___introTime_6)); }
	inline float get_introTime_6() const { return ___introTime_6; }
	inline float* get_address_of_introTime_6() { return &___introTime_6; }
	inline void set_introTime_6(float value)
	{
		___introTime_6 = value;
	}

	inline static int32_t get_offset_of_roundTime_7() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___roundTime_7)); }
	inline float get_roundTime_7() const { return ___roundTime_7; }
	inline float* get_address_of_roundTime_7() { return &___roundTime_7; }
	inline void set_roundTime_7(float value)
	{
		___roundTime_7 = value;
	}

	inline static int32_t get_offset_of_StartGameLeadText_8() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___StartGameLeadText_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_StartGameLeadText_8() const { return ___StartGameLeadText_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_StartGameLeadText_8() { return &___StartGameLeadText_8; }
	inline void set_StartGameLeadText_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___StartGameLeadText_8 = value;
		Il2CppCodeGenWriteBarrier((&___StartGameLeadText_8), value);
	}

	inline static int32_t get_offset_of_StartGameObjectiveText_9() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___StartGameObjectiveText_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_StartGameObjectiveText_9() const { return ___StartGameObjectiveText_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_StartGameObjectiveText_9() { return &___StartGameObjectiveText_9; }
	inline void set_StartGameObjectiveText_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___StartGameObjectiveText_9 = value;
		Il2CppCodeGenWriteBarrier((&___StartGameObjectiveText_9), value);
	}

	inline static int32_t get_offset_of_EndTimeText_10() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___EndTimeText_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EndTimeText_10() const { return ___EndTimeText_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EndTimeText_10() { return &___EndTimeText_10; }
	inline void set_EndTimeText_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EndTimeText_10 = value;
		Il2CppCodeGenWriteBarrier((&___EndTimeText_10), value);
	}

	inline static int32_t get_offset_of_doingIntro_12() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___doingIntro_12)); }
	inline bool get_doingIntro_12() const { return ___doingIntro_12; }
	inline bool* get_address_of_doingIntro_12() { return &___doingIntro_12; }
	inline void set_doingIntro_12(bool value)
	{
		___doingIntro_12 = value;
	}

	inline static int32_t get_offset_of_mainCamera_13() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___mainCamera_13)); }
	inline MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * get_mainCamera_13() const { return ___mainCamera_13; }
	inline MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 ** get_address_of_mainCamera_13() { return &___mainCamera_13; }
	inline void set_mainCamera_13(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * value)
	{
		___mainCamera_13 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_13), value);
	}

	inline static int32_t get_offset_of_audioSource_14() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___audioSource_14)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_14() const { return ___audioSource_14; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_14() { return &___audioSource_14; }
	inline void set_audioSource_14(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_14 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_14), value);
	}
};

struct Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields
{
public:
	// System.Boolean Timer::runTimer
	bool ___runTimer_11;

public:
	inline static int32_t get_offset_of_runTimer_11() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields, ___runTimer_11)); }
	inline bool get_runTimer_11() const { return ___runTimer_11; }
	inline bool* get_address_of_runTimer_11() { return &___runTimer_11; }
	inline void set_runTimer_11(bool value)
	{
		___runTimer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_TD11671555E440B2E0490D401C230CA3B19EDD798_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573), -1, sizeof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2500[23] = 
{
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_colDepth_4(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_zPosition_5(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields::get_offset_of_theAnswer_6(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields::get_offset_of_screenSize_7(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_topCollider_8(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_bottomCollider_9(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_leftCollider_10(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_rightCollider_11(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_cameraPos_12(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_ball_13(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_timer_14(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields::get_offset_of_ballList_15(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields::get_offset_of_gameOn_16(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields::get_offset_of_numPossibleAnswers_17(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields::get_offset_of_numCorrectAnswers_18(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields::get_offset_of_rand_19(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_answerList_20(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_answerListCopy_21(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_ballDiameter_22(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_fbpToNegX_23(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_fbpToPosX_24(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_fbpToNegY_25(),
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573::get_offset_of_fbpToPosY_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01::get_offset_of_U3CU3E1__state_0(),
	U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[3] = 
{
	MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879::get_offset_of_musicSlider_4(),
	MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879::get_offset_of_sfxSlider_5(),
	MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879::get_offset_of_myMusic_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[5] = 
{
	Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B::get_offset_of_baseNotes_4(),
	Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B::get_offset_of_notes_5(),
	Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B::get_offset_of__octave_6(),
	Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B::get_offset_of__note_7(),
	Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B::get_offset_of_hasAccidentals_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA::get_offset_of_ps_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798), -1, sizeof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2507[11] = 
{
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_timerBar_4(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_timeLeft_5(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_introTime_6(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_roundTime_7(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_StartGameLeadText_8(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_StartGameObjectiveText_9(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_EndTimeText_10(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields::get_offset_of_runTimer_11(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_doingIntro_12(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_mainCamera_13(),
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798::get_offset_of_audioSource_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[3] = 
{
	U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B::get_offset_of_U3CU3E1__state_0(),
	U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B::get_offset_of_U3CU3E2__current_1(),
	U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2509[5] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_EFE5D31872C3AD74482056589725763E97F501B9_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (__StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
