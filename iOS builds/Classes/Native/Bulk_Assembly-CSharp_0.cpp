﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// AudioScript
struct AudioScript_t5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB;
// Ball
struct Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3;
// Ball/<LoadLevelAfterDelay>d__18
struct U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086;
// Ball/<StartNextRoundAfterDelay>d__17
struct U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362;
// Ball[]
struct BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064;
// BubbleBlue
struct BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF;
// ButtonFunctions
struct ButtonFunctions_tBA2ACAB62354CE0CB45267950432DF1D6A8E542A;
// CollisionPS
struct CollisionPS_t63D9F9E34A1D39F94602B7F3DA4D196029A20801;
// CollisionScript
struct CollisionScript_tAAD9616D66499CF3EE2069913094BC14012990C4;
// GameControl
struct GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248;
// GuitarSounds
struct GuitarSounds_t69398102599F6F9CE64D066A848098D68AECE165;
// MainCamera
struct MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573;
// MainMenu
struct MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105;
// MenuLayout
struct MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7;
// MenuLayout/<CheckForChange>d__2
struct U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01;
// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB;
// MusicManager
struct MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879;
// Note
struct Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B;
// Note[]
struct NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C;
// PSbubbleCollision
struct PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA;
// PlayerData
struct PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,System.Runtime.Serialization.Formatters.Binary.TypeInformation>
struct Dictionary_2_tDF0B764EEAE1242A344103EC40130E5D891C6934;
// System.Collections.Generic.List`1<Ball>
struct List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6;
// System.Collections.Generic.List`1<Note>
struct List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t64BA96BFC713F221050385E91C868CE455C245D6;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IO.FileStream
struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t4C99617DAC31689CEC0EDB09B067A65E80E1C3EA;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Timer
struct Timer_tD11671555E440B2E0490D401C230CA3B19EDD798;
// Timer/<LoadLevelAfterDelay>d__14
struct U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F;
// UnityEngine.Collision2D
struct Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GUIStyleState
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8;
// runningAnimator
struct runningAnimator_t3E9FF414BEDA1271746FF9D344FE5D64A059DA16;

extern RuntimeClass* BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIStyle_t671F175A201A19166385EE3392292A5F50070572_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t3E5CBC6B113E392EBBE1453DEF2B7CD020F345AA_il2cpp_TypeInfo_var;
extern RuntimeClass* GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784_il2cpp_TypeInfo_var;
extern RuntimeClass* MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var;
extern RuntimeClass* Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
extern RuntimeClass* Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern RuntimeClass* Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F_il2cpp_TypeInfo_var;
extern RuntimeClass* SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
extern RuntimeClass* Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____6FC197002B3B804BF6C339C44A94A60A8622BC93_0_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____EFE5D31872C3AD74482056589725763E97F501B9_4_FieldInfo_var;
extern String_t* _stringLiteral1994557C9850CA12CEDF370B8F7E1A2E35748A9C;
extern String_t* _stringLiteral20889B47F440A2CF9FB9939B043699BEF0A898A9;
extern String_t* _stringLiteral2193DBF0F5379E90F733E226DB48796946392057;
extern String_t* _stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D;
extern String_t* _stringLiteral37797DDCDD6B610A6C3DF82593C89B2BC0D01901;
extern String_t* _stringLiteral3C38EBEDF83A90567CB4B0BB88497E43FA652A67;
extern String_t* _stringLiteral44D95C3B6D58CE780E8322AC7DD9F7883B0E0BE1;
extern String_t* _stringLiteral49BEA2E16586CCC7DFEF61BBCE35FDFB4C1763EF;
extern String_t* _stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F;
extern String_t* _stringLiteral5356314F1E58DF795010B28A52D552EBDE2168F4;
extern String_t* _stringLiteral57B3C08D169273340910055DDEF7535B8DA26BD2;
extern String_t* _stringLiteral6105C32FEC73E438F25D3CC1CD9533F5A37249B7;
extern String_t* _stringLiteral6B9ACFFF7C03940622A9EE21C81E8EED7407A09F;
extern String_t* _stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B;
extern String_t* _stringLiteral6EAAAD406CBC78CB91855411019F284B7225DE23;
extern String_t* _stringLiteral6FBA1A95114471EB786272EF22C5244A40D9AD40;
extern String_t* _stringLiteral7FE80403354BDA7FB596B0F9608845E4DA306DDC;
extern String_t* _stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B;
extern String_t* _stringLiteralAAB03F7C7C5F55684470A259A8D0708E0C2A30F6;
extern String_t* _stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC;
extern String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
extern String_t* _stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D;
extern String_t* _stringLiteralE202493A5370BDEF61FBD54C32E91081454D6606;
extern String_t* _stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372;
extern String_t* _stringLiteralEABCFFD8D5120B660823E2C294A8DC252DA5EA29;
extern String_t* _stringLiteralF93EE3D08E02119EFE5A9C363603C595399BCBB2;
extern String_t* _stringLiteralFE2E4EE3AD4A13C6904C5AB69929F7AD1CE1203F;
extern const RuntimeMethod* Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisBall_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3_m1EFA6F667F1EE1492D5E1FFCE95E405249525BD0_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCircleCollider2D_t862AED067B612149EF365A560B26406F6088641F_m3DAB966F6D30A22BFF7873DAA1254C2FFD25226A_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mA1B86DC3DA1A3CE3E451FA4F1952456C0AE938F3_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_mCDC3359066029BF682DB5CC73FECB9C648B1BE8E_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_m2B6BDE27F8E0A59E4BD0D9F9F65427812542DE92_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponents_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m0527EC164E9C63C03797F7F71607A88771DDE1B4_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m6F48A1BBD6E77DDC2CF47802AD9D4E24DF825270_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mE0D78113FD836E165C8F1B82481737DC8C2E51A3_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m7C39C9921E33484DF592F287D97CCBDCD64E14C0_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m586CA3DF487C8F421B52B94EBB8AFB966C4E3D22_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_m7A27F732EFDF61FC8EB6C64ECDBCB4E5029B798A_RuntimeMethod_var;
extern const RuntimeMethod* Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F_RuntimeMethod_var;
extern const RuntimeMethod* Resources_Load_TisSprite_tCA09498D612D08DE668653AF1E9C12BF53434198_m098412B7E141B9C12C6A755BF488DB1902FF9A97_RuntimeMethod_var;
extern const RuntimeMethod* U3CCheckForChangeU3Ed__2_System_Collections_IEnumerator_Reset_m3A2B6944DF0E6BC0C61F8AA1DE6E9B9DED15D8E6_RuntimeMethod_var;
extern const RuntimeMethod* U3CLoadLevelAfterDelayU3Ed__14_System_Collections_IEnumerator_Reset_m46CD2F1CB19E31D90E3E090D3FE356A887381F59_RuntimeMethod_var;
extern const RuntimeMethod* U3CLoadLevelAfterDelayU3Ed__18_System_Collections_IEnumerator_Reset_mFCB6C49D1A3FA13D71AAA216E40504EB977D08CE_RuntimeMethod_var;
extern const RuntimeMethod* U3CStartNextRoundAfterDelayU3Ed__17_System_Collections_IEnumerator_Reset_m2AB6F1864DDED9EC481CE5B0AE98BC1F1FE1507D_RuntimeMethod_var;
extern const uint32_t Ball_DoParticles_mDB8D1063295BDF10CC104A1095D8F4D00944E3DC_MetadataUsageId;
extern const uint32_t Ball_LoadLevelAfterDelay_mE7A8F2D13D12E276E9FFD81C447E87809A8F1A5A_MetadataUsageId;
extern const uint32_t Ball_OnCollisionEnter2D_m2235B169D9A924C84E5C68015086634B2B826B95_MetadataUsageId;
extern const uint32_t Ball_OnMouseDown_mBDE8F785E5F59C095C108A67E0409A4241520D46_MetadataUsageId;
extern const uint32_t Ball_StartNextRoundAfterDelay_m5C458064B8D41DC06C0659E8DAA76152F57CB864_MetadataUsageId;
extern const uint32_t Ball_Start_mDDC34813CC3D81185647763525B29D09F3DB8C73_MetadataUsageId;
extern const uint32_t Ball_popBubble_m8EF60E2CAC7A2822CFA3FBDECCF0FBF847471973_MetadataUsageId;
extern const uint32_t ButtonFunctions_ExitGame_m82B14835541FC4CB35A892B8909BDDFDC26445DB_MetadataUsageId;
extern const uint32_t ButtonFunctions_LoadByIndex_mE690CB2009749257C8E90C0F7A065C66A8AE1850_MetadataUsageId;
extern const uint32_t GameControl_Awake_m7DCD4282485D4BFA123E5E7DE9933F241E7C44DD_MetadataUsageId;
extern const uint32_t GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA_MetadataUsageId;
extern const uint32_t GameControl_GetNoteList_m08557D7260276BDFD31767A38D7BE9CBC627F9F3_MetadataUsageId;
extern const uint32_t GameControl_Load_mBEDB97AE94477B6D97BAD1FC7A8820AB0B5D4968_MetadataUsageId;
extern const uint32_t GameControl_OnGUI_m9420AEB8AF63DBB3AD8F729B7D1D9EB3EB7C57BA_MetadataUsageId;
extern const uint32_t GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD_MetadataUsageId;
extern const uint32_t GameControl__ctor_m6DCEE981B44B5B587E589B227A098AE27ED86C78_MetadataUsageId;
extern const uint32_t MainCamera_ClearRound_m6BC054933A01450947243932EFEF8270081E826F_MetadataUsageId;
extern const uint32_t MainCamera_GenBall_m55EF5932A3ED533C76096EDB8A14162AB5B1155E_MetadataUsageId;
extern const uint32_t MainCamera_HideBalls_mC0B833AE68EF16E75E7F7D4572C1D075C9AA5B62_MetadataUsageId;
extern const uint32_t MainCamera_ShowBalls_mF2D16795F6AA46BD96C109D468AD2C3C4063875D_MetadataUsageId;
extern const uint32_t MainCamera_StartRound_m78EB3716D63672EE7D7BB8F9A0C20AEFA6689A1D_MetadataUsageId;
extern const uint32_t MainCamera_Start_mBE9D257EA9AE8C3D6EAA5AECF1AFB5AABC62B73F_MetadataUsageId;
extern const uint32_t MainCamera__cctor_m79E60A08E55B86886C621B56E1DBC0DFEF90A292_MetadataUsageId;
extern const uint32_t MainCamera__ctor_m0DBB1A4B33E2FA2CAE1BEAB5E7F1AA96BE67BF17_MetadataUsageId;
extern const uint32_t MenuLayout_CheckForChange_mFE349E388AC54C070288FC2F2A0A0AC2A48FD57B_MetadataUsageId;
extern const uint32_t MusicManager_Start_mF77913A780CD343FC5CC6FE204CF7AFCD8043272_MetadataUsageId;
extern const uint32_t MusicManager_Update_m59A3AA607F68AD3DD974C49E955F3520914A1442_MetadataUsageId;
extern const uint32_t Note__ctor_m13B09FD5AD10AE98E732FC3B88D900C819D100C6_MetadataUsageId;
extern const uint32_t Note__ctor_mC1987B976BE8297565673F2B85BC34F3CB6F2D65_MetadataUsageId;
extern const uint32_t PSbubbleCollision_OnParticleSystemStopped_mD70A211C5AD327481604FFAAA6ED99A5A34FC5DB_MetadataUsageId;
extern const uint32_t Timer_FixedUpdate_mFFCBEB534DA7514BB9F15499D09EA7DFEE134E6D_MetadataUsageId;
extern const uint32_t Timer_LoadLevelAfterDelay_mA28E0E56333FA52D98A2FBA9D42AC3240354E26E_MetadataUsageId;
extern const uint32_t Timer_StartTimer_m297CF68F48B8693DAF2424C0FC96F40B7BB9C5B5_MetadataUsageId;
extern const uint32_t Timer_Start_mEFDCF17412717C132D11A41F14C8F9E96A4B78E4_MetadataUsageId;
extern const uint32_t U3CCheckForChangeU3Ed__2_System_Collections_IEnumerator_Reset_m3A2B6944DF0E6BC0C61F8AA1DE6E9B9DED15D8E6_MetadataUsageId;
extern const uint32_t U3CLoadLevelAfterDelayU3Ed__14_MoveNext_m2169FB23AF6A69A4D091FD7687A75EAD456DAF3E_MetadataUsageId;
extern const uint32_t U3CLoadLevelAfterDelayU3Ed__14_System_Collections_IEnumerator_Reset_m46CD2F1CB19E31D90E3E090D3FE356A887381F59_MetadataUsageId;
extern const uint32_t U3CLoadLevelAfterDelayU3Ed__18_MoveNext_m9415CFC69489F6D0E8ED1321D03DA436B76376E4_MetadataUsageId;
extern const uint32_t U3CLoadLevelAfterDelayU3Ed__18_System_Collections_IEnumerator_Reset_mFCB6C49D1A3FA13D71AAA216E40504EB977D08CE_MetadataUsageId;
extern const uint32_t U3CStartNextRoundAfterDelayU3Ed__17_MoveNext_mF4E1A991B96F77DD5B12A46607092CD47F3283B5_MetadataUsageId;
extern const uint32_t U3CStartNextRoundAfterDelayU3Ed__17_System_Collections_IEnumerator_Reset_m2AB6F1864DDED9EC481CE5B0AE98BC1F1FE1507D_MetadataUsageId;
struct ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 ;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com;
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke;
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com;

struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2;
struct AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF;
struct SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32;


#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADLEVELAFTERDELAYU3ED__18_T913ACD66B31877FB20D9DF596D8D69C10FB74086_H
#define U3CLOADLEVELAFTERDELAYU3ED__18_T913ACD66B31877FB20D9DF596D8D69C10FB74086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball/<LoadLevelAfterDelay>d__18
struct  U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086  : public RuntimeObject
{
public:
	// System.Int32 Ball/<LoadLevelAfterDelay>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Ball/<LoadLevelAfterDelay>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Ball/<LoadLevelAfterDelay>d__18::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLEVELAFTERDELAYU3ED__18_T913ACD66B31877FB20D9DF596D8D69C10FB74086_H
#ifndef U3CSTARTNEXTROUNDAFTERDELAYU3ED__17_T2336B7CD61FC8A8B347C6A29DE116B49D07A9362_H
#define U3CSTARTNEXTROUNDAFTERDELAYU3ED__17_T2336B7CD61FC8A8B347C6A29DE116B49D07A9362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball/<StartNextRoundAfterDelay>d__17
struct  U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362  : public RuntimeObject
{
public:
	// System.Int32 Ball/<StartNextRoundAfterDelay>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Ball/<StartNextRoundAfterDelay>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Ball/<StartNextRoundAfterDelay>d__17::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTNEXTROUNDAFTERDELAYU3ED__17_T2336B7CD61FC8A8B347C6A29DE116B49D07A9362_H
#ifndef U3CCHECKFORCHANGEU3ED__2_T71710E97EFE51ED653E9F3FE4835F607C8324A01_H
#define U3CCHECKFORCHANGEU3ED__2_T71710E97EFE51ED653E9F3FE4835F607C8324A01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuLayout/<CheckForChange>d__2
struct  U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01  : public RuntimeObject
{
public:
	// System.Int32 MenuLayout/<CheckForChange>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MenuLayout/<CheckForChange>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKFORCHANGEU3ED__2_T71710E97EFE51ED653E9F3FE4835F607C8324A01_H
#ifndef PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#define PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043  : public RuntimeObject
{
public:
	// System.Int32 PlayerData::level
	int32_t ___level_0;
	// System.Single PlayerData::experience
	float ___experience_1;
	// System.Single PlayerData::musicVolume
	float ___musicVolume_2;
	// System.Single PlayerData::sfxVolume
	float ___sfxVolume_3;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___level_0)); }
	inline int32_t get_level_0() const { return ___level_0; }
	inline int32_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(int32_t value)
	{
		___level_0 = value;
	}

	inline static int32_t get_offset_of_experience_1() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___experience_1)); }
	inline float get_experience_1() const { return ___experience_1; }
	inline float* get_address_of_experience_1() { return &___experience_1; }
	inline void set_experience_1(float value)
	{
		___experience_1 = value;
	}

	inline static int32_t get_offset_of_musicVolume_2() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___musicVolume_2)); }
	inline float get_musicVolume_2() const { return ___musicVolume_2; }
	inline float* get_address_of_musicVolume_2() { return &___musicVolume_2; }
	inline void set_musicVolume_2(float value)
	{
		___musicVolume_2 = value;
	}

	inline static int32_t get_offset_of_sfxVolume_3() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___sfxVolume_3)); }
	inline float get_sfxVolume_3() const { return ___sfxVolume_3; }
	inline float* get_address_of_sfxVolume_3() { return &___sfxVolume_3; }
	inline void set_sfxVolume_3(float value)
	{
		___sfxVolume_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T2D41893E28DE8FBC9208386ACDC461ED7C3E63A6_H
#define LIST_1_T2D41893E28DE8FBC9208386ACDC461ED7C3E63A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Ball>
struct  List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6, ____items_1)); }
	inline BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064* get__items_1() const { return ____items_1; }
	inline BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6_StaticFields, ____emptyArray_5)); }
	inline BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064* get__emptyArray_5() const { return ____emptyArray_5; }
	inline BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(BallU5BU5D_t0C7709BC76D5D7E34ED1A756FC10731DC2E44064* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2D41893E28DE8FBC9208386ACDC461ED7C3E63A6_H
#ifndef LIST_1_TCABEADDE7156DE34035F33555BC0B75E12FB3784_H
#define LIST_1_TCABEADDE7156DE34035F33555BC0B75E12FB3784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Note>
struct  List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784, ____items_1)); }
	inline NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C* get__items_1() const { return ____items_1; }
	inline NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784_StaticFields, ____emptyArray_5)); }
	inline NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NoteU5BU5D_t74246AEDC38034B82FC591FC62C81F6AF82FC48C* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TCABEADDE7156DE34035F33555BC0B75E12FB3784_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#define RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Random
struct  Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_3;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_4;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SeedArray_5;

public:
	inline static int32_t get_offset_of_inext_3() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inext_3)); }
	inline int32_t get_inext_3() const { return ___inext_3; }
	inline int32_t* get_address_of_inext_3() { return &___inext_3; }
	inline void set_inext_3(int32_t value)
	{
		___inext_3 = value;
	}

	inline static int32_t get_offset_of_inextp_4() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inextp_4)); }
	inline int32_t get_inextp_4() const { return ___inextp_4; }
	inline int32_t* get_address_of_inextp_4() { return &___inextp_4; }
	inline void set_inextp_4(int32_t value)
	{
		___inextp_4 = value;
	}

	inline static int32_t get_offset_of_SeedArray_5() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___SeedArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SeedArray_5() const { return ___SeedArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SeedArray_5() { return &___SeedArray_5; }
	inline void set_SeedArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SeedArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___SeedArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CLOADLEVELAFTERDELAYU3ED__14_T8770C7FEF537DAA3A3A54000B456E5940BD58B5B_H
#define U3CLOADLEVELAFTERDELAYU3ED__14_T8770C7FEF537DAA3A3A54000B456E5940BD58B5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer/<LoadLevelAfterDelay>d__14
struct  U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B  : public RuntimeObject
{
public:
	// System.Int32 Timer/<LoadLevelAfterDelay>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Timer/<LoadLevelAfterDelay>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Timer/<LoadLevelAfterDelay>d__14::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLEVELAFTERDELAYU3ED__14_T8770C7FEF537DAA3A3A54000B456E5940BD58B5B_H
#ifndef YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#define YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#ifndef __STATICARRAYINITTYPESIZEU3D108_T1D3815BD9654C521776B19932355EA4A81BD71FF_H
#define __STATICARRAYINITTYPESIZEU3D108_T1D3815BD9654C521776B19932355EA4A81BD71FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108
struct  __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF__padding[108];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D108_T1D3815BD9654C521776B19932355EA4A81BD71FF_H
#ifndef __STATICARRAYINITTYPESIZEU3D116_TA65347B6FD25750C4C660008486DF03F17E1C1B5_H
#define __STATICARRAYINITTYPESIZEU3D116_TA65347B6FD25750C4C660008486DF03F17E1C1B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=116
struct  __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5__padding[116];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D116_TA65347B6FD25750C4C660008486DF03F17E1C1B5_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUMERATOR_TEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF_H
#define ENUMERATOR_TEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Ball>
struct  Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF, ___list_0)); }
	inline List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * get_list_0() const { return ___list_0; }
	inline List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF, ___current_3)); }
	inline Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * get_current_3() const { return ___current_3; }
	inline Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF_H
#ifndef ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#define ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#ifndef DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#define DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#define SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef WAITFORSECONDS_T3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_H
#define WAITFORSECONDS_T3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::6FC197002B3B804BF6C339C44A94A60A8622BC93
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___6FC197002B3B804BF6C339C44A94A60A8622BC93_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::A265A6CED05B2457B5DB2B8AE4B7775CF3F75004
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::CAD08EB04AFAE771A9BE8CDA687EC435D84090A9
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::CF829E312D8BFDA60F0842C6A044BE77B6C5883D
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=116 <PrivateImplementationDetails>::EFE5D31872C3AD74482056589725763E97F501B9
	__StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5  ___EFE5D31872C3AD74482056589725763E97F501B9_4;

public:
	inline static int32_t get_offset_of_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6FC197002B3B804BF6C339C44A94A60A8622BC93_0)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0() const { return ___6FC197002B3B804BF6C339C44A94A60A8622BC93_0; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0() { return &___6FC197002B3B804BF6C339C44A94A60A8622BC93_0; }
	inline void set_U36FC197002B3B804BF6C339C44A94A60A8622BC93_0(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___6FC197002B3B804BF6C339C44A94A60A8622BC93_0 = value;
	}

	inline static int32_t get_offset_of_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1() const { return ___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1() { return &___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1; }
	inline void set_A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1 = value;
	}

	inline static int32_t get_offset_of_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2() const { return ___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2() { return &___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2; }
	inline void set_CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2 = value;
	}

	inline static int32_t get_offset_of_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3() const { return ___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3() { return &___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3; }
	inline void set_CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3 = value;
	}

	inline static int32_t get_offset_of_EFE5D31872C3AD74482056589725763E97F501B9_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___EFE5D31872C3AD74482056589725763E97F501B9_4)); }
	inline __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5  get_EFE5D31872C3AD74482056589725763E97F501B9_4() const { return ___EFE5D31872C3AD74482056589725763E97F501B9_4; }
	inline __StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5 * get_address_of_EFE5D31872C3AD74482056589725763E97F501B9_4() { return &___EFE5D31872C3AD74482056589725763E97F501B9_4; }
	inline void set_EFE5D31872C3AD74482056589725763E97F501B9_4(__StaticArrayInitTypeSizeU3D116_tA65347B6FD25750C4C660008486DF03F17E1C1B5  value)
	{
		___EFE5D31872C3AD74482056589725763E97F501B9_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#define FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#ifndef FILEMODE_TD19D05B1E6CAF201F88401B04FDB25227664C419_H
#define FILEMODE_TD19D05B1E6CAF201F88401B04FDB25227664C419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileMode
struct  FileMode_tD19D05B1E6CAF201F88401B04FDB25227664C419 
{
public:
	// System.Int32 System.IO.FileMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileMode_tD19D05B1E6CAF201F88401B04FDB25227664C419, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEMODE_TD19D05B1E6CAF201F88401B04FDB25227664C419_H
#ifndef NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#define NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#ifndef FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#define FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifndef FORMATTERTYPESTYLE_TFEF4ABC0D7DE012B1C0976F196E45600568D67AF_H
#define FORMATTERTYPESTYLE_TFEF4ABC0D7DE012B1C0976F196E45600568D67AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterTypeStyle
struct  FormatterTypeStyle_tFEF4ABC0D7DE012B1C0976F196E45600568D67AF 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterTypeStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterTypeStyle_tFEF4ABC0D7DE012B1C0976F196E45600568D67AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERTYPESTYLE_TFEF4ABC0D7DE012B1C0976F196E45600568D67AF_H
#ifndef TYPEFILTERLEVEL_T8FC0F5849147B01F3EB6E3B876E06B3022E0C59A_H
#define TYPEFILTERLEVEL_T8FC0F5849147B01F3EB6E3B876E06B3022E0C59A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.TypeFilterLevel
struct  TypeFilterLevel_t8FC0F5849147B01F3EB6E3B876E06B3022E0C59A 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.TypeFilterLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeFilterLevel_t8FC0F5849147B01F3EB6E3B876E06B3022E0C59A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERLEVEL_T8FC0F5849147B01F3EB6E3B876E06B3022E0C59A_H
#ifndef STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#define STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifndef RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#define RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifndef COLLISION2D_T45DC963DE1229CFFC7D0B666800F0AE93688764D_H
#define COLLISION2D_T45DC963DE1229CFFC7D0B666800F0AE93688764D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision2D
struct  Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RelativeVelocity_4;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_5;
	// System.Int32 UnityEngine.Collision2D::m_ContactCount
	int32_t ___m_ContactCount_6;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_RecycledContacts
	ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32* ___m_RecycledContacts_7;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_LegacyContacts
	ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32* ___m_LegacyContacts_8;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_Collider_0)); }
	inline int32_t get_m_Collider_0() const { return ___m_Collider_0; }
	inline int32_t* get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(int32_t value)
	{
		___m_Collider_0 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_1() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_OtherCollider_1)); }
	inline int32_t get_m_OtherCollider_1() const { return ___m_OtherCollider_1; }
	inline int32_t* get_address_of_m_OtherCollider_1() { return &___m_OtherCollider_1; }
	inline void set_m_OtherCollider_1(int32_t value)
	{
		___m_OtherCollider_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_Rigidbody_2)); }
	inline int32_t get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline int32_t* get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(int32_t value)
	{
		___m_Rigidbody_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_3() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_OtherRigidbody_3)); }
	inline int32_t get_m_OtherRigidbody_3() const { return ___m_OtherRigidbody_3; }
	inline int32_t* get_address_of_m_OtherRigidbody_3() { return &___m_OtherRigidbody_3; }
	inline void set_m_OtherRigidbody_3(int32_t value)
	{
		___m_OtherRigidbody_3 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_4() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_RelativeVelocity_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RelativeVelocity_4() const { return ___m_RelativeVelocity_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RelativeVelocity_4() { return &___m_RelativeVelocity_4; }
	inline void set_m_RelativeVelocity_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RelativeVelocity_4 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_5() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_Enabled_5)); }
	inline int32_t get_m_Enabled_5() const { return ___m_Enabled_5; }
	inline int32_t* get_address_of_m_Enabled_5() { return &___m_Enabled_5; }
	inline void set_m_Enabled_5(int32_t value)
	{
		___m_Enabled_5 = value;
	}

	inline static int32_t get_offset_of_m_ContactCount_6() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_ContactCount_6)); }
	inline int32_t get_m_ContactCount_6() const { return ___m_ContactCount_6; }
	inline int32_t* get_address_of_m_ContactCount_6() { return &___m_ContactCount_6; }
	inline void set_m_ContactCount_6(int32_t value)
	{
		___m_ContactCount_6 = value;
	}

	inline static int32_t get_offset_of_m_RecycledContacts_7() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_RecycledContacts_7)); }
	inline ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32* get_m_RecycledContacts_7() const { return ___m_RecycledContacts_7; }
	inline ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32** get_address_of_m_RecycledContacts_7() { return &___m_RecycledContacts_7; }
	inline void set_m_RecycledContacts_7(ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32* value)
	{
		___m_RecycledContacts_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RecycledContacts_7), value);
	}

	inline static int32_t get_offset_of_m_LegacyContacts_8() { return static_cast<int32_t>(offsetof(Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D, ___m_LegacyContacts_8)); }
	inline ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32* get_m_LegacyContacts_8() const { return ___m_LegacyContacts_8; }
	inline ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32** get_address_of_m_LegacyContacts_8() { return &___m_LegacyContacts_8; }
	inline void set_m_LegacyContacts_8(ContactPoint2DU5BU5D_t390B6CBF0673E9C408A97BC093462A33516F2C32* value)
	{
		___m_LegacyContacts_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyContacts_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 * ___m_RecycledContacts_7;
	ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 * ___m_LegacyContacts_8;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 * ___m_RecycledContacts_7;
	ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 * ___m_LegacyContacts_8;
};
#endif // COLLISION2D_T45DC963DE1229CFFC7D0B666800F0AE93688764D_H
#ifndef CONTACTPOINT2D_T7DE4097DD62E4240F4629EBB41F4BF089141E2C0_H
#define CONTACTPOINT2D_T7DE4097DD62E4240F4629EBB41F4BF089141E2C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContactPoint2D
struct  ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 
{
public:
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_0;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_1;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_RelativeVelocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RelativeVelocity_2;
	// System.Single UnityEngine.ContactPoint2D::m_Separation
	float ___m_Separation_3;
	// System.Single UnityEngine.ContactPoint2D::m_NormalImpulse
	float ___m_NormalImpulse_4;
	// System.Single UnityEngine.ContactPoint2D::m_TangentImpulse
	float ___m_TangentImpulse_5;
	// System.Int32 UnityEngine.ContactPoint2D::m_Collider
	int32_t ___m_Collider_6;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherCollider
	int32_t ___m_OtherCollider_7;
	// System.Int32 UnityEngine.ContactPoint2D::m_Rigidbody
	int32_t ___m_Rigidbody_8;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_9;
	// System.Int32 UnityEngine.ContactPoint2D::m_Enabled
	int32_t ___m_Enabled_10;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_Point_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_Normal_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_2() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_RelativeVelocity_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RelativeVelocity_2() const { return ___m_RelativeVelocity_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RelativeVelocity_2() { return &___m_RelativeVelocity_2; }
	inline void set_m_RelativeVelocity_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RelativeVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_Separation_3() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_Separation_3)); }
	inline float get_m_Separation_3() const { return ___m_Separation_3; }
	inline float* get_address_of_m_Separation_3() { return &___m_Separation_3; }
	inline void set_m_Separation_3(float value)
	{
		___m_Separation_3 = value;
	}

	inline static int32_t get_offset_of_m_NormalImpulse_4() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_NormalImpulse_4)); }
	inline float get_m_NormalImpulse_4() const { return ___m_NormalImpulse_4; }
	inline float* get_address_of_m_NormalImpulse_4() { return &___m_NormalImpulse_4; }
	inline void set_m_NormalImpulse_4(float value)
	{
		___m_NormalImpulse_4 = value;
	}

	inline static int32_t get_offset_of_m_TangentImpulse_5() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_TangentImpulse_5)); }
	inline float get_m_TangentImpulse_5() const { return ___m_TangentImpulse_5; }
	inline float* get_address_of_m_TangentImpulse_5() { return &___m_TangentImpulse_5; }
	inline void set_m_TangentImpulse_5(float value)
	{
		___m_TangentImpulse_5 = value;
	}

	inline static int32_t get_offset_of_m_Collider_6() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_Collider_6)); }
	inline int32_t get_m_Collider_6() const { return ___m_Collider_6; }
	inline int32_t* get_address_of_m_Collider_6() { return &___m_Collider_6; }
	inline void set_m_Collider_6(int32_t value)
	{
		___m_Collider_6 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_7() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_OtherCollider_7)); }
	inline int32_t get_m_OtherCollider_7() const { return ___m_OtherCollider_7; }
	inline int32_t* get_address_of_m_OtherCollider_7() { return &___m_OtherCollider_7; }
	inline void set_m_OtherCollider_7(int32_t value)
	{
		___m_OtherCollider_7 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_8() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_Rigidbody_8)); }
	inline int32_t get_m_Rigidbody_8() const { return ___m_Rigidbody_8; }
	inline int32_t* get_address_of_m_Rigidbody_8() { return &___m_Rigidbody_8; }
	inline void set_m_Rigidbody_8(int32_t value)
	{
		___m_Rigidbody_8 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_9() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_OtherRigidbody_9)); }
	inline int32_t get_m_OtherRigidbody_9() const { return ___m_OtherRigidbody_9; }
	inline int32_t* get_address_of_m_OtherRigidbody_9() { return &___m_OtherRigidbody_9; }
	inline void set_m_OtherRigidbody_9(int32_t value)
	{
		___m_OtherRigidbody_9 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_10() { return static_cast<int32_t>(offsetof(ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0, ___m_Enabled_10)); }
	inline int32_t get_m_Enabled_10() const { return ___m_Enabled_10; }
	inline int32_t* get_address_of_m_Enabled_10() { return &___m_Enabled_10; }
	inline void set_m_Enabled_10(int32_t value)
	{
		___m_Enabled_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACTPOINT2D_T7DE4097DD62E4240F4629EBB41F4BF089141E2C0_H
#ifndef COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#define COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#ifndef DEVICEORIENTATION_T810CFE5E6AF3B033010EC62D7AA2BEFD97A06158_H
#define DEVICEORIENTATION_T810CFE5E6AF3B033010EC62D7AA2BEFD97A06158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DeviceOrientation
struct  DeviceOrientation_t810CFE5E6AF3B033010EC62D7AA2BEFD97A06158 
{
public:
	// System.Int32 UnityEngine.DeviceOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceOrientation_t810CFE5E6AF3B033010EC62D7AA2BEFD97A06158, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEORIENTATION_T810CFE5E6AF3B033010EC62D7AA2BEFD97A06158_H
#ifndef FORCEMODE2D_T80F82E2BBC1733F0B88593968BA9CE9BC96A7E36_H
#define FORCEMODE2D_T80F82E2BBC1733F0B88593968BA9CE9BC96A7E36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode2D
struct  ForceMode2D_t80F82E2BBC1733F0B88593968BA9CE9BC96A7E36 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode2D_t80F82E2BBC1733F0B88593968BA9CE9BC96A7E36, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE2D_T80F82E2BBC1733F0B88593968BA9CE9BC96A7E36_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RECTOFFSET_TED44B1176E93501050480416699D1F11BAE8C87A_H
#define RECTOFFSET_TED44B1176E93501050480416699D1F11BAE8C87A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_TED44B1176E93501050480416699D1F11BAE8C87A_H
#ifndef COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#define COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifndef FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#define FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#ifndef TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#define TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#ifndef MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#define MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifndef SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#define SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_tF089B96B46A592693753CBF23C52A3887632D210 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tF089B96B46A592693753CBF23C52A3887632D210, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifndef TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#define TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifndef DIRECTION_TAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E_H
#define DIRECTION_TAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_TAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E_H
#ifndef FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#define FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_6;
	// System.String System.IO.FileStream::name
	String_t* ___name_7;
	// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.FileStream::safeHandle
	SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * ___safeHandle_8;
	// System.Boolean System.IO.FileStream::isExposed
	bool ___isExposed_9;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_10;
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_11;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_12;
	// System.Boolean System.IO.FileStream::async
	bool ___async_13;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_14;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_15;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_16;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_17;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_18;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_19;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_20;

public:
	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((&___buf_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_safeHandle_8() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___safeHandle_8)); }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * get_safeHandle_8() const { return ___safeHandle_8; }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB ** get_address_of_safeHandle_8() { return &___safeHandle_8; }
	inline void set_safeHandle_8(SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * value)
	{
		___safeHandle_8 = value;
		Il2CppCodeGenWriteBarrier((&___safeHandle_8), value);
	}

	inline static int32_t get_offset_of_isExposed_9() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___isExposed_9)); }
	inline bool get_isExposed_9() const { return ___isExposed_9; }
	inline bool* get_address_of_isExposed_9() { return &___isExposed_9; }
	inline void set_isExposed_9(bool value)
	{
		___isExposed_9 = value;
	}

	inline static int32_t get_offset_of_append_startpos_10() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___append_startpos_10)); }
	inline int64_t get_append_startpos_10() const { return ___append_startpos_10; }
	inline int64_t* get_address_of_append_startpos_10() { return &___append_startpos_10; }
	inline void set_append_startpos_10(int64_t value)
	{
		___append_startpos_10 = value;
	}

	inline static int32_t get_offset_of_access_11() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___access_11)); }
	inline int32_t get_access_11() const { return ___access_11; }
	inline int32_t* get_address_of_access_11() { return &___access_11; }
	inline void set_access_11(int32_t value)
	{
		___access_11 = value;
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___owner_12)); }
	inline bool get_owner_12() const { return ___owner_12; }
	inline bool* get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(bool value)
	{
		___owner_12 = value;
	}

	inline static int32_t get_offset_of_async_13() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___async_13)); }
	inline bool get_async_13() const { return ___async_13; }
	inline bool* get_address_of_async_13() { return &___async_13; }
	inline void set_async_13(bool value)
	{
		___async_13 = value;
	}

	inline static int32_t get_offset_of_canseek_14() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___canseek_14)); }
	inline bool get_canseek_14() const { return ___canseek_14; }
	inline bool* get_address_of_canseek_14() { return &___canseek_14; }
	inline void set_canseek_14(bool value)
	{
		___canseek_14 = value;
	}

	inline static int32_t get_offset_of_anonymous_15() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___anonymous_15)); }
	inline bool get_anonymous_15() const { return ___anonymous_15; }
	inline bool* get_address_of_anonymous_15() { return &___anonymous_15; }
	inline void set_anonymous_15(bool value)
	{
		___anonymous_15 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_16() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_dirty_16)); }
	inline bool get_buf_dirty_16() const { return ___buf_dirty_16; }
	inline bool* get_address_of_buf_dirty_16() { return &___buf_dirty_16; }
	inline void set_buf_dirty_16(bool value)
	{
		___buf_dirty_16 = value;
	}

	inline static int32_t get_offset_of_buf_size_17() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_size_17)); }
	inline int32_t get_buf_size_17() const { return ___buf_size_17; }
	inline int32_t* get_address_of_buf_size_17() { return &___buf_size_17; }
	inline void set_buf_size_17(int32_t value)
	{
		___buf_size_17 = value;
	}

	inline static int32_t get_offset_of_buf_length_18() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_length_18)); }
	inline int32_t get_buf_length_18() const { return ___buf_length_18; }
	inline int32_t* get_address_of_buf_length_18() { return &___buf_length_18; }
	inline void set_buf_length_18(int32_t value)
	{
		___buf_length_18 = value;
	}

	inline static int32_t get_offset_of_buf_offset_19() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_offset_19)); }
	inline int32_t get_buf_offset_19() const { return ___buf_offset_19; }
	inline int32_t* get_address_of_buf_offset_19() { return &___buf_offset_19; }
	inline void set_buf_offset_19(int32_t value)
	{
		___buf_offset_19 = value;
	}

	inline static int32_t get_offset_of_buf_start_20() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_start_20)); }
	inline int64_t get_buf_start_20() const { return ___buf_start_20; }
	inline int64_t* get_address_of_buf_start_20() { return &___buf_start_20; }
	inline void set_buf_start_20(int64_t value)
	{
		___buf_start_20 = value;
	}
};

struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields
{
public:
	// System.Byte[] System.IO.FileStream::buf_recycle
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_recycle_4;
	// System.Object System.IO.FileStream::buf_recycle_lock
	RuntimeObject * ___buf_recycle_lock_5;

public:
	inline static int32_t get_offset_of_buf_recycle_4() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_recycle_4() const { return ___buf_recycle_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_recycle_4() { return &___buf_recycle_4; }
	inline void set_buf_recycle_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_recycle_4 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_4), value);
	}

	inline static int32_t get_offset_of_buf_recycle_lock_5() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_lock_5)); }
	inline RuntimeObject * get_buf_recycle_lock_5() const { return ___buf_recycle_lock_5; }
	inline RuntimeObject ** get_address_of_buf_recycle_lock_5() { return &___buf_recycle_lock_5; }
	inline void set_buf_recycle_lock_5(RuntimeObject * value)
	{
		___buf_recycle_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_lock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#ifndef STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#define STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifndef AUDIOCLIP_TCC3C35F579203CE2601243585AB3D6953C3BA051_H
#define AUDIOCLIP_TCC3C35F579203CE2601243585AB3D6953C3BA051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_4), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_TCC3C35F579203CE2601243585AB3D6953C3BA051_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GUISTYLE_T671F175A201A19166385EE3392292A5F50070572_H
#define GUISTYLE_T671F175A201A19166385EE3392292A5F50070572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t671F175A201A19166385EE3392292A5F50070572  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Overflow_12;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Normal_1)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Hover_2)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Active_3)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Focused_4)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnNormal_5)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnHover_6)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnActive_7)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnFocused_8)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Border_9)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Padding_10)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Margin_11)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Overflow_12)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}
};

struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_13;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___s_None_14;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_13() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields, ___showKeyboardFocus_13)); }
	inline bool get_showKeyboardFocus_13() const { return ___showKeyboardFocus_13; }
	inline bool* get_address_of_showKeyboardFocus_13() { return &___showKeyboardFocus_13; }
	inline void set_showKeyboardFocus_13(bool value)
	{
		___showKeyboardFocus_13 = value;
	}

	inline static int32_t get_offset_of_s_None_14() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields, ___s_None_14)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_s_None_14() const { return ___s_None_14; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_s_None_14() { return &___s_None_14; }
	inline void set_s_None_14(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___s_None_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Border_9;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Padding_10;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Margin_11;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Overflow_12;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Normal_1;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Hover_2;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Active_3;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Focused_4;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnFocused_8;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Border_9;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Padding_10;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Margin_11;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Overflow_12;
};
#endif // GUISTYLE_T671F175A201A19166385EE3392292A5F50070572_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#define SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#ifndef NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#define NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifndef BINARYFORMATTER_T116398AB9D7E425E4CFF83C37824A46443A2E6D0_H
#define BINARYFORMATTER_T116398AB9D7E425E4CFF83C37824A46443A2E6D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct  BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::m_surrogates
	RuntimeObject* ___m_surrogates_0;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::m_context
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___m_context_1;
	// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::m_binder
	SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * ___m_binder_2;
	// System.Runtime.Serialization.Formatters.FormatterTypeStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::m_typeFormat
	int32_t ___m_typeFormat_3;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::m_assemblyFormat
	int32_t ___m_assemblyFormat_4;
	// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::m_securityLevel
	int32_t ___m_securityLevel_5;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::m_crossAppDomainArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_crossAppDomainArray_6;

public:
	inline static int32_t get_offset_of_m_surrogates_0() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0, ___m_surrogates_0)); }
	inline RuntimeObject* get_m_surrogates_0() const { return ___m_surrogates_0; }
	inline RuntimeObject** get_address_of_m_surrogates_0() { return &___m_surrogates_0; }
	inline void set_m_surrogates_0(RuntimeObject* value)
	{
		___m_surrogates_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_surrogates_0), value);
	}

	inline static int32_t get_offset_of_m_context_1() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0, ___m_context_1)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get_m_context_1() const { return ___m_context_1; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of_m_context_1() { return &___m_context_1; }
	inline void set_m_context_1(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		___m_context_1 = value;
	}

	inline static int32_t get_offset_of_m_binder_2() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0, ___m_binder_2)); }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * get_m_binder_2() const { return ___m_binder_2; }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 ** get_address_of_m_binder_2() { return &___m_binder_2; }
	inline void set_m_binder_2(SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * value)
	{
		___m_binder_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_binder_2), value);
	}

	inline static int32_t get_offset_of_m_typeFormat_3() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0, ___m_typeFormat_3)); }
	inline int32_t get_m_typeFormat_3() const { return ___m_typeFormat_3; }
	inline int32_t* get_address_of_m_typeFormat_3() { return &___m_typeFormat_3; }
	inline void set_m_typeFormat_3(int32_t value)
	{
		___m_typeFormat_3 = value;
	}

	inline static int32_t get_offset_of_m_assemblyFormat_4() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0, ___m_assemblyFormat_4)); }
	inline int32_t get_m_assemblyFormat_4() const { return ___m_assemblyFormat_4; }
	inline int32_t* get_address_of_m_assemblyFormat_4() { return &___m_assemblyFormat_4; }
	inline void set_m_assemblyFormat_4(int32_t value)
	{
		___m_assemblyFormat_4 = value;
	}

	inline static int32_t get_offset_of_m_securityLevel_5() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0, ___m_securityLevel_5)); }
	inline int32_t get_m_securityLevel_5() const { return ___m_securityLevel_5; }
	inline int32_t* get_address_of_m_securityLevel_5() { return &___m_securityLevel_5; }
	inline void set_m_securityLevel_5(int32_t value)
	{
		___m_securityLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_crossAppDomainArray_6() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0, ___m_crossAppDomainArray_6)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_crossAppDomainArray_6() const { return ___m_crossAppDomainArray_6; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_crossAppDomainArray_6() { return &___m_crossAppDomainArray_6; }
	inline void set_m_crossAppDomainArray_6(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_crossAppDomainArray_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_crossAppDomainArray_6), value);
	}
};

struct BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Runtime.Serialization.Formatters.Binary.TypeInformation> System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::typeNameCache
	Dictionary_2_tDF0B764EEAE1242A344103EC40130E5D891C6934 * ___typeNameCache_7;

public:
	inline static int32_t get_offset_of_typeNameCache_7() { return static_cast<int32_t>(offsetof(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0_StaticFields, ___typeNameCache_7)); }
	inline Dictionary_2_tDF0B764EEAE1242A344103EC40130E5D891C6934 * get_typeNameCache_7() const { return ___typeNameCache_7; }
	inline Dictionary_2_tDF0B764EEAE1242A344103EC40130E5D891C6934 ** get_address_of_typeNameCache_7() { return &___typeNameCache_7; }
	inline void set_typeNameCache_7(Dictionary_2_tDF0B764EEAE1242A344103EC40130E5D891C6934 * value)
	{
		___typeNameCache_7 = value;
		Il2CppCodeGenWriteBarrier((&___typeNameCache_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYFORMATTER_T116398AB9D7E425E4CFF83C37824A46443A2E6D0_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef PARTICLESYSTEM_T45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_H
#define PARTICLESYSTEM_T45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_H
#ifndef RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#define RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifndef RIGIDBODY2D_TBDC6900A76D3C47E291446FF008D02B817C81CDE_H
#define RIGIDBODY2D_TBDC6900A76D3C47E291446FF008D02B817C81CDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_TBDC6900A76D3C47E291446FF008D02B817C81CDE_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#define ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifndef AUDIOBEHAVIOUR_TC612EC4E17A648A5C568621F3FBF1DBD773C71C7_H
#define AUDIOBEHAVIOUR_TC612EC4E17A648A5C568621F3FBF1DBD773C71C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioBehaviour
struct  AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBEHAVIOUR_TC612EC4E17A648A5C568621F3FBF1DBD773C71C7_H
#ifndef CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#define CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifndef COLLIDER2D_TD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_H
#define COLLIDER2D_TD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_TD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_H
#ifndef MESHRENDERER_T9D67CA54E83315F743623BDE8EADCD5074659EED_H
#define MESHRENDERER_T9D67CA54E83315F743623BDE8EADCD5074659EED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED  : public Renderer_t0556D67DD582620D1F495627EDE30D03284151F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T9D67CA54E83315F743623BDE8EADCD5074659EED_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SPRITERENDERER_TCD51E875611195DBB91123B68434881D3441BC6F_H
#define SPRITERENDERER_TCD51E875611195DBB91123B68434881D3441BC6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F  : public Renderer_t0556D67DD582620D1F495627EDE30D03284151F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_TCD51E875611195DBB91123B68434881D3441BC6F_H
#ifndef AUDIOSCRIPT_T5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB_H
#define AUDIOSCRIPT_T5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioScript
struct  AudioScript_t5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip AudioScript::MusicClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___MusicClip_4;
	// UnityEngine.AudioSource AudioScript::MusicSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___MusicSource_5;

public:
	inline static int32_t get_offset_of_MusicClip_4() { return static_cast<int32_t>(offsetof(AudioScript_t5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB, ___MusicClip_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_MusicClip_4() const { return ___MusicClip_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_MusicClip_4() { return &___MusicClip_4; }
	inline void set_MusicClip_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___MusicClip_4 = value;
		Il2CppCodeGenWriteBarrier((&___MusicClip_4), value);
	}

	inline static int32_t get_offset_of_MusicSource_5() { return static_cast<int32_t>(offsetof(AudioScript_t5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB, ___MusicSource_5)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_MusicSource_5() const { return ___MusicSource_5; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_MusicSource_5() { return &___MusicSource_5; }
	inline void set_MusicSource_5(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___MusicSource_5 = value;
		Il2CppCodeGenWriteBarrier((&___MusicSource_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSCRIPT_T5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB_H
#ifndef BALL_T2A505E3143FBAC85D561E48B8E9B9462719F3EB3_H
#define BALL_T2A505E3143FBAC85D561E48B8E9B9462719F3EB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball
struct  Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody2D Ball::ball
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___ball_4;
	// UnityEngine.Vector2 Ball::velocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___velocity_5;
	// UnityEngine.AudioClip Ball::MusicClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___MusicClip_6;
	// UnityEngine.GameObject Ball::collideEffectObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___collideEffectObj_7;
	// UnityEngine.GameObject Ball::popEffectObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___popEffectObj_8;
	// MainCamera Ball::theMC
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * ___theMC_9;
	// BubbleBlue Ball::bubbleBlue
	BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * ___bubbleBlue_10;
	// UnityEngine.AudioSource Ball::collisionAudioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___collisionAudioSource_11;
	// UnityEngine.AudioSource Ball::noteAudioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___noteAudioSource_12;
	// UnityEngine.AudioClip[] Ball::cseArr
	AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* ___cseArr_13;
	// Note Ball::note
	Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * ___note_14;

public:
	inline static int32_t get_offset_of_ball_4() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___ball_4)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_ball_4() const { return ___ball_4; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_ball_4() { return &___ball_4; }
	inline void set_ball_4(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___ball_4 = value;
		Il2CppCodeGenWriteBarrier((&___ball_4), value);
	}

	inline static int32_t get_offset_of_velocity_5() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___velocity_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_velocity_5() const { return ___velocity_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_velocity_5() { return &___velocity_5; }
	inline void set_velocity_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___velocity_5 = value;
	}

	inline static int32_t get_offset_of_MusicClip_6() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___MusicClip_6)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_MusicClip_6() const { return ___MusicClip_6; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_MusicClip_6() { return &___MusicClip_6; }
	inline void set_MusicClip_6(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___MusicClip_6 = value;
		Il2CppCodeGenWriteBarrier((&___MusicClip_6), value);
	}

	inline static int32_t get_offset_of_collideEffectObj_7() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___collideEffectObj_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_collideEffectObj_7() const { return ___collideEffectObj_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_collideEffectObj_7() { return &___collideEffectObj_7; }
	inline void set_collideEffectObj_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___collideEffectObj_7 = value;
		Il2CppCodeGenWriteBarrier((&___collideEffectObj_7), value);
	}

	inline static int32_t get_offset_of_popEffectObj_8() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___popEffectObj_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_popEffectObj_8() const { return ___popEffectObj_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_popEffectObj_8() { return &___popEffectObj_8; }
	inline void set_popEffectObj_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___popEffectObj_8 = value;
		Il2CppCodeGenWriteBarrier((&___popEffectObj_8), value);
	}

	inline static int32_t get_offset_of_theMC_9() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___theMC_9)); }
	inline MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * get_theMC_9() const { return ___theMC_9; }
	inline MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 ** get_address_of_theMC_9() { return &___theMC_9; }
	inline void set_theMC_9(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * value)
	{
		___theMC_9 = value;
		Il2CppCodeGenWriteBarrier((&___theMC_9), value);
	}

	inline static int32_t get_offset_of_bubbleBlue_10() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___bubbleBlue_10)); }
	inline BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * get_bubbleBlue_10() const { return ___bubbleBlue_10; }
	inline BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF ** get_address_of_bubbleBlue_10() { return &___bubbleBlue_10; }
	inline void set_bubbleBlue_10(BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * value)
	{
		___bubbleBlue_10 = value;
		Il2CppCodeGenWriteBarrier((&___bubbleBlue_10), value);
	}

	inline static int32_t get_offset_of_collisionAudioSource_11() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___collisionAudioSource_11)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_collisionAudioSource_11() const { return ___collisionAudioSource_11; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_collisionAudioSource_11() { return &___collisionAudioSource_11; }
	inline void set_collisionAudioSource_11(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___collisionAudioSource_11 = value;
		Il2CppCodeGenWriteBarrier((&___collisionAudioSource_11), value);
	}

	inline static int32_t get_offset_of_noteAudioSource_12() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___noteAudioSource_12)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_noteAudioSource_12() const { return ___noteAudioSource_12; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_noteAudioSource_12() { return &___noteAudioSource_12; }
	inline void set_noteAudioSource_12(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___noteAudioSource_12 = value;
		Il2CppCodeGenWriteBarrier((&___noteAudioSource_12), value);
	}

	inline static int32_t get_offset_of_cseArr_13() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___cseArr_13)); }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* get_cseArr_13() const { return ___cseArr_13; }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2** get_address_of_cseArr_13() { return &___cseArr_13; }
	inline void set_cseArr_13(AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* value)
	{
		___cseArr_13 = value;
		Il2CppCodeGenWriteBarrier((&___cseArr_13), value);
	}

	inline static int32_t get_offset_of_note_14() { return static_cast<int32_t>(offsetof(Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3, ___note_14)); }
	inline Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * get_note_14() const { return ___note_14; }
	inline Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B ** get_address_of_note_14() { return &___note_14; }
	inline void set_note_14(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * value)
	{
		___note_14 = value;
		Il2CppCodeGenWriteBarrier((&___note_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T2A505E3143FBAC85D561E48B8E9B9462719F3EB3_H
#ifndef BUBBLEBLUE_T45A8F3C166C89E6648F24B58288914702EE867EF_H
#define BUBBLEBLUE_T45A8F3C166C89E6648F24B58288914702EE867EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BubbleBlue
struct  BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator BubbleBlue::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_4;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF, ___animator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_4() const { return ___animator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUBBLEBLUE_T45A8F3C166C89E6648F24B58288914702EE867EF_H
#ifndef BUTTONFUNCTIONS_TBA2ACAB62354CE0CB45267950432DF1D6A8E542A_H
#define BUTTONFUNCTIONS_TBA2ACAB62354CE0CB45267950432DF1D6A8E542A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonFunctions
struct  ButtonFunctions_tBA2ACAB62354CE0CB45267950432DF1D6A8E542A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONFUNCTIONS_TBA2ACAB62354CE0CB45267950432DF1D6A8E542A_H
#ifndef COLLISIONPS_T63D9F9E34A1D39F94602B7F3DA4D196029A20801_H
#define COLLISIONPS_T63D9F9E34A1D39F94602B7F3DA4D196029A20801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollisionPS
struct  CollisionPS_t63D9F9E34A1D39F94602B7F3DA4D196029A20801  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONPS_T63D9F9E34A1D39F94602B7F3DA4D196029A20801_H
#ifndef COLLISIONSCRIPT_TAAD9616D66499CF3EE2069913094BC14012990C4_H
#define COLLISIONSCRIPT_TAAD9616D66499CF3EE2069913094BC14012990C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollisionScript
struct  CollisionScript_tAAD9616D66499CF3EE2069913094BC14012990C4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CollisionScript::effectObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___effectObj_4;

public:
	inline static int32_t get_offset_of_effectObj_4() { return static_cast<int32_t>(offsetof(CollisionScript_tAAD9616D66499CF3EE2069913094BC14012990C4, ___effectObj_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_effectObj_4() const { return ___effectObj_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_effectObj_4() { return &___effectObj_4; }
	inline void set_effectObj_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___effectObj_4 = value;
		Il2CppCodeGenWriteBarrier((&___effectObj_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONSCRIPT_TAAD9616D66499CF3EE2069913094BC14012990C4_H
#ifndef GAMECONTROL_T60391CE7B0B029798FBC6F92207CDB38C6B1F248_H
#define GAMECONTROL_T60391CE7B0B029798FBC6F92207CDB38C6B1F248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameControl
struct  GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 GameControl::level
	int32_t ___level_5;
	// System.Single GameControl::experience
	float ___experience_6;
	// System.Single GameControl::musicVolume
	float ___musicVolume_7;
	// System.Single GameControl::sfxVolume
	float ___sfxVolume_8;
	// UnityEngine.GUIStyle GameControl::guiStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___guiStyle_9;
	// System.Int32[] GameControl::expToLvl_BL
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___expToLvl_BL_10;
	// System.Int32[] GameControl::numBubbles_BL
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___numBubbles_BL_11;
	// System.Int32[] GameControl::numPossNotes_BL
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___numPossNotes_BL_12;
	// System.Single[] GameControl::roundTime_BL
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___roundTime_BL_13;
	// System.Int32[] GameControl::notesToInclude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___notesToInclude_14;

public:
	inline static int32_t get_offset_of_level_5() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___level_5)); }
	inline int32_t get_level_5() const { return ___level_5; }
	inline int32_t* get_address_of_level_5() { return &___level_5; }
	inline void set_level_5(int32_t value)
	{
		___level_5 = value;
	}

	inline static int32_t get_offset_of_experience_6() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___experience_6)); }
	inline float get_experience_6() const { return ___experience_6; }
	inline float* get_address_of_experience_6() { return &___experience_6; }
	inline void set_experience_6(float value)
	{
		___experience_6 = value;
	}

	inline static int32_t get_offset_of_musicVolume_7() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___musicVolume_7)); }
	inline float get_musicVolume_7() const { return ___musicVolume_7; }
	inline float* get_address_of_musicVolume_7() { return &___musicVolume_7; }
	inline void set_musicVolume_7(float value)
	{
		___musicVolume_7 = value;
	}

	inline static int32_t get_offset_of_sfxVolume_8() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___sfxVolume_8)); }
	inline float get_sfxVolume_8() const { return ___sfxVolume_8; }
	inline float* get_address_of_sfxVolume_8() { return &___sfxVolume_8; }
	inline void set_sfxVolume_8(float value)
	{
		___sfxVolume_8 = value;
	}

	inline static int32_t get_offset_of_guiStyle_9() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___guiStyle_9)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_guiStyle_9() const { return ___guiStyle_9; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_guiStyle_9() { return &___guiStyle_9; }
	inline void set_guiStyle_9(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___guiStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___guiStyle_9), value);
	}

	inline static int32_t get_offset_of_expToLvl_BL_10() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___expToLvl_BL_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_expToLvl_BL_10() const { return ___expToLvl_BL_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_expToLvl_BL_10() { return &___expToLvl_BL_10; }
	inline void set_expToLvl_BL_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___expToLvl_BL_10 = value;
		Il2CppCodeGenWriteBarrier((&___expToLvl_BL_10), value);
	}

	inline static int32_t get_offset_of_numBubbles_BL_11() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___numBubbles_BL_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_numBubbles_BL_11() const { return ___numBubbles_BL_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_numBubbles_BL_11() { return &___numBubbles_BL_11; }
	inline void set_numBubbles_BL_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___numBubbles_BL_11 = value;
		Il2CppCodeGenWriteBarrier((&___numBubbles_BL_11), value);
	}

	inline static int32_t get_offset_of_numPossNotes_BL_12() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___numPossNotes_BL_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_numPossNotes_BL_12() const { return ___numPossNotes_BL_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_numPossNotes_BL_12() { return &___numPossNotes_BL_12; }
	inline void set_numPossNotes_BL_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___numPossNotes_BL_12 = value;
		Il2CppCodeGenWriteBarrier((&___numPossNotes_BL_12), value);
	}

	inline static int32_t get_offset_of_roundTime_BL_13() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___roundTime_BL_13)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_roundTime_BL_13() const { return ___roundTime_BL_13; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_roundTime_BL_13() { return &___roundTime_BL_13; }
	inline void set_roundTime_BL_13(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___roundTime_BL_13 = value;
		Il2CppCodeGenWriteBarrier((&___roundTime_BL_13), value);
	}

	inline static int32_t get_offset_of_notesToInclude_14() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248, ___notesToInclude_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_notesToInclude_14() const { return ___notesToInclude_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_notesToInclude_14() { return &___notesToInclude_14; }
	inline void set_notesToInclude_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___notesToInclude_14 = value;
		Il2CppCodeGenWriteBarrier((&___notesToInclude_14), value);
	}
};

struct GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields
{
public:
	// GameControl GameControl::control
	GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * ___control_4;

public:
	inline static int32_t get_offset_of_control_4() { return static_cast<int32_t>(offsetof(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields, ___control_4)); }
	inline GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * get_control_4() const { return ___control_4; }
	inline GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 ** get_address_of_control_4() { return &___control_4; }
	inline void set_control_4(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * value)
	{
		___control_4 = value;
		Il2CppCodeGenWriteBarrier((&___control_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTROL_T60391CE7B0B029798FBC6F92207CDB38C6B1F248_H
#ifndef GUITARSOUNDS_T69398102599F6F9CE64D066A848098D68AECE165_H
#define GUITARSOUNDS_T69398102599F6F9CE64D066A848098D68AECE165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuitarSounds
struct  GuitarSounds_t69398102599F6F9CE64D066A848098D68AECE165  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource GuitarSounds::myAudio
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___myAudio_4;

public:
	inline static int32_t get_offset_of_myAudio_4() { return static_cast<int32_t>(offsetof(GuitarSounds_t69398102599F6F9CE64D066A848098D68AECE165, ___myAudio_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_myAudio_4() const { return ___myAudio_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_myAudio_4() { return &___myAudio_4; }
	inline void set_myAudio_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___myAudio_4 = value;
		Il2CppCodeGenWriteBarrier((&___myAudio_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITARSOUNDS_T69398102599F6F9CE64D066A848098D68AECE165_H
#ifndef MAINCAMERA_TFD85DD5C86E2E33A0D38306F77612F04983DC573_H
#define MAINCAMERA_TFD85DD5C86E2E33A0D38306F77612F04983DC573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainCamera
struct  MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MainCamera::colDepth
	float ___colDepth_4;
	// System.Single MainCamera::zPosition
	float ___zPosition_5;
	// UnityEngine.Transform MainCamera::topCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___topCollider_8;
	// UnityEngine.Transform MainCamera::bottomCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bottomCollider_9;
	// UnityEngine.Transform MainCamera::leftCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___leftCollider_10;
	// UnityEngine.Transform MainCamera::rightCollider
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___rightCollider_11;
	// UnityEngine.Vector3 MainCamera::cameraPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cameraPos_12;
	// UnityEngine.Rigidbody2D MainCamera::ball
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___ball_13;
	// Timer MainCamera::timer
	Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * ___timer_14;
	// System.Collections.Generic.List`1<Note> MainCamera::answerList
	List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * ___answerList_20;
	// System.Collections.Generic.List`1<Note> MainCamera::answerListCopy
	List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * ___answerListCopy_21;
	// System.Single MainCamera::ballDiameter
	float ___ballDiameter_22;
	// System.Single MainCamera::fbpToNegX
	float ___fbpToNegX_23;
	// System.Single MainCamera::fbpToPosX
	float ___fbpToPosX_24;
	// System.Single MainCamera::fbpToNegY
	float ___fbpToNegY_25;
	// System.Single MainCamera::fbpToPosY
	float ___fbpToPosY_26;

public:
	inline static int32_t get_offset_of_colDepth_4() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___colDepth_4)); }
	inline float get_colDepth_4() const { return ___colDepth_4; }
	inline float* get_address_of_colDepth_4() { return &___colDepth_4; }
	inline void set_colDepth_4(float value)
	{
		___colDepth_4 = value;
	}

	inline static int32_t get_offset_of_zPosition_5() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___zPosition_5)); }
	inline float get_zPosition_5() const { return ___zPosition_5; }
	inline float* get_address_of_zPosition_5() { return &___zPosition_5; }
	inline void set_zPosition_5(float value)
	{
		___zPosition_5 = value;
	}

	inline static int32_t get_offset_of_topCollider_8() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___topCollider_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_topCollider_8() const { return ___topCollider_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_topCollider_8() { return &___topCollider_8; }
	inline void set_topCollider_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___topCollider_8 = value;
		Il2CppCodeGenWriteBarrier((&___topCollider_8), value);
	}

	inline static int32_t get_offset_of_bottomCollider_9() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___bottomCollider_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bottomCollider_9() const { return ___bottomCollider_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bottomCollider_9() { return &___bottomCollider_9; }
	inline void set_bottomCollider_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bottomCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&___bottomCollider_9), value);
	}

	inline static int32_t get_offset_of_leftCollider_10() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___leftCollider_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_leftCollider_10() const { return ___leftCollider_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_leftCollider_10() { return &___leftCollider_10; }
	inline void set_leftCollider_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___leftCollider_10 = value;
		Il2CppCodeGenWriteBarrier((&___leftCollider_10), value);
	}

	inline static int32_t get_offset_of_rightCollider_11() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___rightCollider_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_rightCollider_11() const { return ___rightCollider_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_rightCollider_11() { return &___rightCollider_11; }
	inline void set_rightCollider_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___rightCollider_11 = value;
		Il2CppCodeGenWriteBarrier((&___rightCollider_11), value);
	}

	inline static int32_t get_offset_of_cameraPos_12() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___cameraPos_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cameraPos_12() const { return ___cameraPos_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cameraPos_12() { return &___cameraPos_12; }
	inline void set_cameraPos_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cameraPos_12 = value;
	}

	inline static int32_t get_offset_of_ball_13() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___ball_13)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_ball_13() const { return ___ball_13; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_ball_13() { return &___ball_13; }
	inline void set_ball_13(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___ball_13 = value;
		Il2CppCodeGenWriteBarrier((&___ball_13), value);
	}

	inline static int32_t get_offset_of_timer_14() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___timer_14)); }
	inline Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * get_timer_14() const { return ___timer_14; }
	inline Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 ** get_address_of_timer_14() { return &___timer_14; }
	inline void set_timer_14(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * value)
	{
		___timer_14 = value;
		Il2CppCodeGenWriteBarrier((&___timer_14), value);
	}

	inline static int32_t get_offset_of_answerList_20() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___answerList_20)); }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * get_answerList_20() const { return ___answerList_20; }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 ** get_address_of_answerList_20() { return &___answerList_20; }
	inline void set_answerList_20(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * value)
	{
		___answerList_20 = value;
		Il2CppCodeGenWriteBarrier((&___answerList_20), value);
	}

	inline static int32_t get_offset_of_answerListCopy_21() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___answerListCopy_21)); }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * get_answerListCopy_21() const { return ___answerListCopy_21; }
	inline List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 ** get_address_of_answerListCopy_21() { return &___answerListCopy_21; }
	inline void set_answerListCopy_21(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * value)
	{
		___answerListCopy_21 = value;
		Il2CppCodeGenWriteBarrier((&___answerListCopy_21), value);
	}

	inline static int32_t get_offset_of_ballDiameter_22() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___ballDiameter_22)); }
	inline float get_ballDiameter_22() const { return ___ballDiameter_22; }
	inline float* get_address_of_ballDiameter_22() { return &___ballDiameter_22; }
	inline void set_ballDiameter_22(float value)
	{
		___ballDiameter_22 = value;
	}

	inline static int32_t get_offset_of_fbpToNegX_23() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToNegX_23)); }
	inline float get_fbpToNegX_23() const { return ___fbpToNegX_23; }
	inline float* get_address_of_fbpToNegX_23() { return &___fbpToNegX_23; }
	inline void set_fbpToNegX_23(float value)
	{
		___fbpToNegX_23 = value;
	}

	inline static int32_t get_offset_of_fbpToPosX_24() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToPosX_24)); }
	inline float get_fbpToPosX_24() const { return ___fbpToPosX_24; }
	inline float* get_address_of_fbpToPosX_24() { return &___fbpToPosX_24; }
	inline void set_fbpToPosX_24(float value)
	{
		___fbpToPosX_24 = value;
	}

	inline static int32_t get_offset_of_fbpToNegY_25() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToNegY_25)); }
	inline float get_fbpToNegY_25() const { return ___fbpToNegY_25; }
	inline float* get_address_of_fbpToNegY_25() { return &___fbpToNegY_25; }
	inline void set_fbpToNegY_25(float value)
	{
		___fbpToNegY_25 = value;
	}

	inline static int32_t get_offset_of_fbpToPosY_26() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573, ___fbpToPosY_26)); }
	inline float get_fbpToPosY_26() const { return ___fbpToPosY_26; }
	inline float* get_address_of_fbpToPosY_26() { return &___fbpToPosY_26; }
	inline void set_fbpToPosY_26(float value)
	{
		___fbpToPosY_26 = value;
	}
};

struct MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields
{
public:
	// System.String MainCamera::theAnswer
	String_t* ___theAnswer_6;
	// UnityEngine.Vector2 MainCamera::screenSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenSize_7;
	// System.Collections.Generic.List`1<Ball> MainCamera::ballList
	List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * ___ballList_15;
	// System.Boolean MainCamera::gameOn
	bool ___gameOn_16;
	// System.Int32 MainCamera::numPossibleAnswers
	int32_t ___numPossibleAnswers_17;
	// System.Int32 MainCamera::numCorrectAnswers
	int32_t ___numCorrectAnswers_18;
	// System.Random MainCamera::rand
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___rand_19;

public:
	inline static int32_t get_offset_of_theAnswer_6() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___theAnswer_6)); }
	inline String_t* get_theAnswer_6() const { return ___theAnswer_6; }
	inline String_t** get_address_of_theAnswer_6() { return &___theAnswer_6; }
	inline void set_theAnswer_6(String_t* value)
	{
		___theAnswer_6 = value;
		Il2CppCodeGenWriteBarrier((&___theAnswer_6), value);
	}

	inline static int32_t get_offset_of_screenSize_7() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___screenSize_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenSize_7() const { return ___screenSize_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenSize_7() { return &___screenSize_7; }
	inline void set_screenSize_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenSize_7 = value;
	}

	inline static int32_t get_offset_of_ballList_15() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___ballList_15)); }
	inline List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * get_ballList_15() const { return ___ballList_15; }
	inline List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 ** get_address_of_ballList_15() { return &___ballList_15; }
	inline void set_ballList_15(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * value)
	{
		___ballList_15 = value;
		Il2CppCodeGenWriteBarrier((&___ballList_15), value);
	}

	inline static int32_t get_offset_of_gameOn_16() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___gameOn_16)); }
	inline bool get_gameOn_16() const { return ___gameOn_16; }
	inline bool* get_address_of_gameOn_16() { return &___gameOn_16; }
	inline void set_gameOn_16(bool value)
	{
		___gameOn_16 = value;
	}

	inline static int32_t get_offset_of_numPossibleAnswers_17() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___numPossibleAnswers_17)); }
	inline int32_t get_numPossibleAnswers_17() const { return ___numPossibleAnswers_17; }
	inline int32_t* get_address_of_numPossibleAnswers_17() { return &___numPossibleAnswers_17; }
	inline void set_numPossibleAnswers_17(int32_t value)
	{
		___numPossibleAnswers_17 = value;
	}

	inline static int32_t get_offset_of_numCorrectAnswers_18() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___numCorrectAnswers_18)); }
	inline int32_t get_numCorrectAnswers_18() const { return ___numCorrectAnswers_18; }
	inline int32_t* get_address_of_numCorrectAnswers_18() { return &___numCorrectAnswers_18; }
	inline void set_numCorrectAnswers_18(int32_t value)
	{
		___numCorrectAnswers_18 = value;
	}

	inline static int32_t get_offset_of_rand_19() { return static_cast<int32_t>(offsetof(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields, ___rand_19)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_rand_19() const { return ___rand_19; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_rand_19() { return &___rand_19; }
	inline void set_rand_19(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___rand_19 = value;
		Il2CppCodeGenWriteBarrier((&___rand_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINCAMERA_TFD85DD5C86E2E33A0D38306F77612F04983DC573_H
#ifndef MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#define MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifndef MENULAYOUT_TF5F4902CBDEF9388989C7F621F64B424BCA978B7_H
#define MENULAYOUT_TF5F4902CBDEF9388989C7F621F64B424BCA978B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuLayout
struct  MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENULAYOUT_TF5F4902CBDEF9388989C7F621F64B424BCA978B7_H
#ifndef MUSICMANAGER_TA48147377617955FE0763C9A03D97A97DAA15879_H
#define MUSICMANAGER_TA48147377617955FE0763C9A03D97A97DAA15879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicManager
struct  MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider MusicManager::musicSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___musicSlider_4;
	// UnityEngine.UI.Slider MusicManager::sfxSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___sfxSlider_5;
	// UnityEngine.AudioSource MusicManager::myMusic
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___myMusic_6;

public:
	inline static int32_t get_offset_of_musicSlider_4() { return static_cast<int32_t>(offsetof(MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879, ___musicSlider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_musicSlider_4() const { return ___musicSlider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_musicSlider_4() { return &___musicSlider_4; }
	inline void set_musicSlider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___musicSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___musicSlider_4), value);
	}

	inline static int32_t get_offset_of_sfxSlider_5() { return static_cast<int32_t>(offsetof(MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879, ___sfxSlider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_sfxSlider_5() const { return ___sfxSlider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_sfxSlider_5() { return &___sfxSlider_5; }
	inline void set_sfxSlider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___sfxSlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___sfxSlider_5), value);
	}

	inline static int32_t get_offset_of_myMusic_6() { return static_cast<int32_t>(offsetof(MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879, ___myMusic_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_myMusic_6() const { return ___myMusic_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_myMusic_6() { return &___myMusic_6; }
	inline void set_myMusic_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___myMusic_6 = value;
		Il2CppCodeGenWriteBarrier((&___myMusic_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUSICMANAGER_TA48147377617955FE0763C9A03D97A97DAA15879_H
#ifndef NOTE_TB8381BB2C942DB3B8911836FC539545E83D4D43B_H
#define NOTE_TB8381BB2C942DB3B8911836FC539545E83D4D43B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Note
struct  Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] Note::baseNotes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___baseNotes_4;
	// System.String[] Note::notes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___notes_5;
	// System.Int32 Note::_octave
	int32_t ____octave_6;
	// System.String Note::_note
	String_t* ____note_7;
	// System.Boolean Note::hasAccidentals
	bool ___hasAccidentals_8;

public:
	inline static int32_t get_offset_of_baseNotes_4() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ___baseNotes_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_baseNotes_4() const { return ___baseNotes_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_baseNotes_4() { return &___baseNotes_4; }
	inline void set_baseNotes_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___baseNotes_4 = value;
		Il2CppCodeGenWriteBarrier((&___baseNotes_4), value);
	}

	inline static int32_t get_offset_of_notes_5() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ___notes_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_notes_5() const { return ___notes_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_notes_5() { return &___notes_5; }
	inline void set_notes_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___notes_5 = value;
		Il2CppCodeGenWriteBarrier((&___notes_5), value);
	}

	inline static int32_t get_offset_of__octave_6() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ____octave_6)); }
	inline int32_t get__octave_6() const { return ____octave_6; }
	inline int32_t* get_address_of__octave_6() { return &____octave_6; }
	inline void set__octave_6(int32_t value)
	{
		____octave_6 = value;
	}

	inline static int32_t get_offset_of__note_7() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ____note_7)); }
	inline String_t* get__note_7() const { return ____note_7; }
	inline String_t** get_address_of__note_7() { return &____note_7; }
	inline void set__note_7(String_t* value)
	{
		____note_7 = value;
		Il2CppCodeGenWriteBarrier((&____note_7), value);
	}

	inline static int32_t get_offset_of_hasAccidentals_8() { return static_cast<int32_t>(offsetof(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B, ___hasAccidentals_8)); }
	inline bool get_hasAccidentals_8() const { return ___hasAccidentals_8; }
	inline bool* get_address_of_hasAccidentals_8() { return &___hasAccidentals_8; }
	inline void set_hasAccidentals_8(bool value)
	{
		___hasAccidentals_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTE_TB8381BB2C942DB3B8911836FC539545E83D4D43B_H
#ifndef PSBUBBLECOLLISION_T0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA_H
#define PSBUBBLECOLLISION_T0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PSbubbleCollision
struct  PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem PSbubbleCollision::ps
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___ps_4;

public:
	inline static int32_t get_offset_of_ps_4() { return static_cast<int32_t>(offsetof(PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA, ___ps_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_ps_4() const { return ___ps_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_ps_4() { return &___ps_4; }
	inline void set_ps_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___ps_4 = value;
		Il2CppCodeGenWriteBarrier((&___ps_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PSBUBBLECOLLISION_T0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA_H
#ifndef TIMER_TD11671555E440B2E0490D401C230CA3B19EDD798_H
#define TIMER_TD11671555E440B2E0490D401C230CA3B19EDD798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer
struct  Timer_tD11671555E440B2E0490D401C230CA3B19EDD798  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image Timer::timerBar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___timerBar_4;
	// System.Single Timer::timeLeft
	float ___timeLeft_5;
	// System.Single Timer::introTime
	float ___introTime_6;
	// System.Single Timer::roundTime
	float ___roundTime_7;
	// UnityEngine.GameObject Timer::StartGameLeadText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___StartGameLeadText_8;
	// UnityEngine.GameObject Timer::StartGameObjectiveText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___StartGameObjectiveText_9;
	// UnityEngine.GameObject Timer::EndTimeText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EndTimeText_10;
	// System.Boolean Timer::doingIntro
	bool ___doingIntro_12;
	// MainCamera Timer::mainCamera
	MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * ___mainCamera_13;
	// UnityEngine.AudioSource Timer::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_14;

public:
	inline static int32_t get_offset_of_timerBar_4() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___timerBar_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_timerBar_4() const { return ___timerBar_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_timerBar_4() { return &___timerBar_4; }
	inline void set_timerBar_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___timerBar_4 = value;
		Il2CppCodeGenWriteBarrier((&___timerBar_4), value);
	}

	inline static int32_t get_offset_of_timeLeft_5() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___timeLeft_5)); }
	inline float get_timeLeft_5() const { return ___timeLeft_5; }
	inline float* get_address_of_timeLeft_5() { return &___timeLeft_5; }
	inline void set_timeLeft_5(float value)
	{
		___timeLeft_5 = value;
	}

	inline static int32_t get_offset_of_introTime_6() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___introTime_6)); }
	inline float get_introTime_6() const { return ___introTime_6; }
	inline float* get_address_of_introTime_6() { return &___introTime_6; }
	inline void set_introTime_6(float value)
	{
		___introTime_6 = value;
	}

	inline static int32_t get_offset_of_roundTime_7() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___roundTime_7)); }
	inline float get_roundTime_7() const { return ___roundTime_7; }
	inline float* get_address_of_roundTime_7() { return &___roundTime_7; }
	inline void set_roundTime_7(float value)
	{
		___roundTime_7 = value;
	}

	inline static int32_t get_offset_of_StartGameLeadText_8() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___StartGameLeadText_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_StartGameLeadText_8() const { return ___StartGameLeadText_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_StartGameLeadText_8() { return &___StartGameLeadText_8; }
	inline void set_StartGameLeadText_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___StartGameLeadText_8 = value;
		Il2CppCodeGenWriteBarrier((&___StartGameLeadText_8), value);
	}

	inline static int32_t get_offset_of_StartGameObjectiveText_9() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___StartGameObjectiveText_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_StartGameObjectiveText_9() const { return ___StartGameObjectiveText_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_StartGameObjectiveText_9() { return &___StartGameObjectiveText_9; }
	inline void set_StartGameObjectiveText_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___StartGameObjectiveText_9 = value;
		Il2CppCodeGenWriteBarrier((&___StartGameObjectiveText_9), value);
	}

	inline static int32_t get_offset_of_EndTimeText_10() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___EndTimeText_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EndTimeText_10() const { return ___EndTimeText_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EndTimeText_10() { return &___EndTimeText_10; }
	inline void set_EndTimeText_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EndTimeText_10 = value;
		Il2CppCodeGenWriteBarrier((&___EndTimeText_10), value);
	}

	inline static int32_t get_offset_of_doingIntro_12() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___doingIntro_12)); }
	inline bool get_doingIntro_12() const { return ___doingIntro_12; }
	inline bool* get_address_of_doingIntro_12() { return &___doingIntro_12; }
	inline void set_doingIntro_12(bool value)
	{
		___doingIntro_12 = value;
	}

	inline static int32_t get_offset_of_mainCamera_13() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___mainCamera_13)); }
	inline MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * get_mainCamera_13() const { return ___mainCamera_13; }
	inline MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 ** get_address_of_mainCamera_13() { return &___mainCamera_13; }
	inline void set_mainCamera_13(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * value)
	{
		___mainCamera_13 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_13), value);
	}

	inline static int32_t get_offset_of_audioSource_14() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798, ___audioSource_14)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_14() const { return ___audioSource_14; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_14() { return &___audioSource_14; }
	inline void set_audioSource_14(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_14 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_14), value);
	}
};

struct Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields
{
public:
	// System.Boolean Timer::runTimer
	bool ___runTimer_11;

public:
	inline static int32_t get_offset_of_runTimer_11() { return static_cast<int32_t>(offsetof(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields, ___runTimer_11)); }
	inline bool get_runTimer_11() const { return ___runTimer_11; }
	inline bool* get_address_of_runTimer_11() { return &___runTimer_11; }
	inline void set_runTimer_11(bool value)
	{
		___runTimer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_TD11671555E440B2E0490D401C230CA3B19EDD798_H
#ifndef AUDIOSOURCE_T5196F862B4E60F404613361C90D87FBDD041E93C_H
#define AUDIOSOURCE_T5196F862B4E60F404613361C90D87FBDD041E93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C  : public AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 * ___spatializerExtension_4;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 * ___ambisonicExtension_5;

public:
	inline static int32_t get_offset_of_spatializerExtension_4() { return static_cast<int32_t>(offsetof(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C, ___spatializerExtension_4)); }
	inline AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 * get_spatializerExtension_4() const { return ___spatializerExtension_4; }
	inline AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 ** get_address_of_spatializerExtension_4() { return &___spatializerExtension_4; }
	inline void set_spatializerExtension_4(AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 * value)
	{
		___spatializerExtension_4 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_4), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_5() { return static_cast<int32_t>(offsetof(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C, ___ambisonicExtension_5)); }
	inline AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 * get_ambisonicExtension_5() const { return ___ambisonicExtension_5; }
	inline AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 ** get_address_of_ambisonicExtension_5() { return &___ambisonicExtension_5; }
	inline void set_ambisonicExtension_5(AudioSourceExtension_t9643FEF245632F35A3FED88FBBDDEA3404BDEAE1 * value)
	{
		___ambisonicExtension_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T5196F862B4E60F404613361C90D87FBDD041E93C_H
#ifndef BOXCOLLIDER2D_TA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_H
#define BOXCOLLIDER2D_TA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider2D
struct  BoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87  : public Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER2D_TA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_H
#ifndef CIRCLECOLLIDER2D_T862AED067B612149EF365A560B26406F6088641F_H
#define CIRCLECOLLIDER2D_T862AED067B612149EF365A560B26406F6088641F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CircleCollider2D
struct  CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F  : public Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLECOLLIDER2D_T862AED067B612149EF365A560B26406F6088641F_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef RUNNINGANIMATOR_T3E9FF414BEDA1271746FF9D344FE5D64A059DA16_H
#define RUNNINGANIMATOR_T3E9FF414BEDA1271746FF9D344FE5D64A059DA16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// runningAnimator
struct  runningAnimator_t3E9FF414BEDA1271746FF9D344FE5D64A059DA16  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator runningAnimator::anime
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anime_4;

public:
	inline static int32_t get_offset_of_anime_4() { return static_cast<int32_t>(offsetof(runningAnimator_t3E9FF414BEDA1271746FF9D344FE5D64A059DA16, ___anime_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anime_4() const { return ___anime_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anime_4() { return &___anime_4; }
	inline void set_anime_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anime_4 = value;
		Il2CppCodeGenWriteBarrier((&___anime_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNINGANIMATOR_T3E9FF414BEDA1271746FF9D344FE5D64A059DA16_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#define SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_5)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_7)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_8)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_11)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_17)); }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_List_4)); }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef SLIDER_T0654A41304B5CE7074CA86F4E66CB681D0D52C09_H
#define SLIDER_T0654A41304B5CE7074CA86F4E66CB681D0D52C09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider
struct  Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_FillRect_18;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleRect_19;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_20;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_21;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_22;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_23;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_24;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * ___m_OnValueChanged_25;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_FillImage_26;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_FillTransform_27;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_FillContainerRect_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_HandleTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleContainerRect_30;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Offset_31;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_32;

public:
	inline static int32_t get_offset_of_m_FillRect_18() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillRect_18)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_FillRect_18() const { return ___m_FillRect_18; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_FillRect_18() { return &___m_FillRect_18; }
	inline void set_m_FillRect_18(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_FillRect_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_18), value);
	}

	inline static int32_t get_offset_of_m_HandleRect_19() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleRect_19)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleRect_19() const { return ___m_HandleRect_19; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleRect_19() { return &___m_HandleRect_19; }
	inline void set_m_HandleRect_19(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleRect_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_19), value);
	}

	inline static int32_t get_offset_of_m_Direction_20() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Direction_20)); }
	inline int32_t get_m_Direction_20() const { return ___m_Direction_20; }
	inline int32_t* get_address_of_m_Direction_20() { return &___m_Direction_20; }
	inline void set_m_Direction_20(int32_t value)
	{
		___m_Direction_20 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_21() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_MinValue_21)); }
	inline float get_m_MinValue_21() const { return ___m_MinValue_21; }
	inline float* get_address_of_m_MinValue_21() { return &___m_MinValue_21; }
	inline void set_m_MinValue_21(float value)
	{
		___m_MinValue_21 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_22() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_MaxValue_22)); }
	inline float get_m_MaxValue_22() const { return ___m_MaxValue_22; }
	inline float* get_address_of_m_MaxValue_22() { return &___m_MaxValue_22; }
	inline void set_m_MaxValue_22(float value)
	{
		___m_MaxValue_22 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_23() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_WholeNumbers_23)); }
	inline bool get_m_WholeNumbers_23() const { return ___m_WholeNumbers_23; }
	inline bool* get_address_of_m_WholeNumbers_23() { return &___m_WholeNumbers_23; }
	inline void set_m_WholeNumbers_23(bool value)
	{
		___m_WholeNumbers_23 = value;
	}

	inline static int32_t get_offset_of_m_Value_24() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Value_24)); }
	inline float get_m_Value_24() const { return ___m_Value_24; }
	inline float* get_address_of_m_Value_24() { return &___m_Value_24; }
	inline void set_m_Value_24(float value)
	{
		___m_Value_24 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_OnValueChanged_25)); }
	inline SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_25), value);
	}

	inline static int32_t get_offset_of_m_FillImage_26() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillImage_26)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_FillImage_26() const { return ___m_FillImage_26; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_FillImage_26() { return &___m_FillImage_26; }
	inline void set_m_FillImage_26(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_FillImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillImage_26), value);
	}

	inline static int32_t get_offset_of_m_FillTransform_27() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillTransform_27)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_FillTransform_27() const { return ___m_FillTransform_27; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_FillTransform_27() { return &___m_FillTransform_27; }
	inline void set_m_FillTransform_27(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_FillTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillTransform_27), value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_28() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillContainerRect_28)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_FillContainerRect_28() const { return ___m_FillContainerRect_28; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_FillContainerRect_28() { return &___m_FillContainerRect_28; }
	inline void set_m_FillContainerRect_28(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_FillContainerRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillContainerRect_28), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_29() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleTransform_29)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_HandleTransform_29() const { return ___m_HandleTransform_29; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_HandleTransform_29() { return &___m_HandleTransform_29; }
	inline void set_m_HandleTransform_29(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_HandleTransform_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_29), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleContainerRect_30)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleContainerRect_30() const { return ___m_HandleContainerRect_30; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleContainerRect_30() { return &___m_HandleContainerRect_30; }
	inline void set_m_HandleContainerRect_30(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_30), value);
	}

	inline static int32_t get_offset_of_m_Offset_31() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Offset_31)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Offset_31() const { return ___m_Offset_31; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Offset_31() { return &___m_Offset_31; }
	inline void set_m_Offset_31(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Offset_31 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_32() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Tracker_32)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_32() const { return ___m_Tracker_32; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_32() { return &___m_Tracker_32; }
	inline void set_m_Tracker_32(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDER_T0654A41304B5CE7074CA86F4E66CB681D0D52C09_H
#ifndef IMAGE_T18FED07D8646917E1C563745518CF3DD57FF0B3E_H
#define IMAGE_T18FED07D8646917E1C563745518CF3DD57FF0B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_41;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_42;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Sprite_31)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_OverrideSprite_32)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_41() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Tracked_41)); }
	inline bool get_m_Tracked_41() const { return ___m_Tracked_41; }
	inline bool* get_address_of_m_Tracked_41() { return &___m_Tracked_41; }
	inline void set_m_Tracked_41(bool value)
	{
		___m_Tracked_41 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_42() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_UseSpriteMesh_42)); }
	inline bool get_m_UseSpriteMesh_42() const { return ___m_UseSpriteMesh_42; }
	inline bool* get_address_of_m_UseSpriteMesh_42() { return &___m_UseSpriteMesh_42; }
	inline void set_m_UseSpriteMesh_42(bool value)
	{
		___m_UseSpriteMesh_42 = value;
	}
};

struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_VertScratch_43;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_UVScratch_44;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Xy_45;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Uv_46;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * ___m_TrackedTexturelessImages_47;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_48;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 * ___U3CU3Ef__mgU24cache0_49;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_43() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_VertScratch_43)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_VertScratch_43() const { return ___s_VertScratch_43; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_VertScratch_43() { return &___s_VertScratch_43; }
	inline void set_s_VertScratch_43(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_VertScratch_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_43), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_44() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_UVScratch_44)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_UVScratch_44() const { return ___s_UVScratch_44; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_UVScratch_44() { return &___s_UVScratch_44; }
	inline void set_s_UVScratch_44(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_UVScratch_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_44), value);
	}

	inline static int32_t get_offset_of_s_Xy_45() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Xy_45)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Xy_45() const { return ___s_Xy_45; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Xy_45() { return &___s_Xy_45; }
	inline void set_s_Xy_45(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Xy_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_45), value);
	}

	inline static int32_t get_offset_of_s_Uv_46() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Uv_46)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Uv_46() const { return ___s_Uv_46; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Uv_46() { return &___s_Uv_46; }
	inline void set_s_Uv_46(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Uv_46 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_46), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_47() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___m_TrackedTexturelessImages_47)); }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * get_m_TrackedTexturelessImages_47() const { return ___m_TrackedTexturelessImages_47; }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED ** get_address_of_m_TrackedTexturelessImages_47() { return &___m_TrackedTexturelessImages_47; }
	inline void set_m_TrackedTexturelessImages_47(List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * value)
	{
		___m_TrackedTexturelessImages_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_47), value);
	}

	inline static int32_t get_offset_of_s_Initialized_48() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Initialized_48)); }
	inline bool get_s_Initialized_48() const { return ___s_Initialized_48; }
	inline bool* get_address_of_s_Initialized_48() { return &___s_Initialized_48; }
	inline void set_s_Initialized_48(bool value)
	{
		___s_Initialized_48 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_49() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___U3CU3Ef__mgU24cache0_49)); }
	inline Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 * get_U3CU3Ef__mgU24cache0_49() const { return ___U3CU3Ef__mgU24cache0_49; }
	inline Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 ** get_address_of_U3CU3Ef__mgU24cache0_49() { return &___U3CU3Ef__mgU24cache0_49; }
	inline void set_U3CU3Ef__mgU24cache0_49(Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 * value)
	{
		___U3CU3Ef__mgU24cache0_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T18FED07D8646917E1C563745518CF3DD57FF0B3E_H
#ifndef TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#define TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_30)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_32)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * m_Items[1];

public:
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * m_Items[1];

public:
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * m_Items[1];

public:
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* GameObject_GetComponents_TisRuntimeObject_m5EA1B9B8C5B01E9DE33C6FB0D9AD52F85E0D3911_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m312D167000593C478994C1F4C93930C5DAED66D3_gshared (String_t* p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m352D452C728667C9C76C942525CDE26444568ECD_gshared (RuntimeObject * p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);

// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C" IL2CPP_METHOD_ATTR void AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.String Note::get_theNote()
extern "C" IL2CPP_METHOD_ATTR String_t* Note_get_theNote_mD93A18AA36F41A8FAD9E225130C45FC8C1CCEDE2 (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384 (float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Single UnityEngine.Random::get_value()
extern "C" IL2CPP_METHOD_ATTR float Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C (const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, bool p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, int32_t p1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, bool p0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponents<UnityEngine.AudioSource>()
inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* GameObject_GetComponents_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m0527EC164E9C63C03797F7F71607A88771DDE1B4 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponents_TisRuntimeObject_m5EA1B9B8C5B01E9DE33C6FB0D9AD52F85E0D3911_gshared)(__this, method);
}
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.SpriteRenderer>()
inline SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32* Component_GetComponentsInChildren_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mA1B86DC3DA1A3CE3E451FA4F1952456C0AE938F3 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared)(__this, method);
}
// System.String System.String::ToLower()
extern "C" IL2CPP_METHOD_ATTR String_t* String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8 (String_t* __this, const RuntimeMethod* method);
// System.Int32 Note::get_Octave()
extern "C" IL2CPP_METHOD_ATTR int32_t Note_get_Octave_m0886E707D45BB24FAAA7A11950C13C847063077D (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC (RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<UnityEngine.Sprite>(System.String)
inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * Resources_Load_TisSprite_tCA09498D612D08DE668653AF1E9C12BF53434198_m098412B7E141B9C12C6A755BF488DB1902FF9A97 (String_t* p0, const RuntimeMethod* method)
{
	return ((  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m312D167000593C478994C1F4C93930C5DAED66D3_gshared)(p0, method);
}
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_m9F5C8B2007AA03FAB66F0CB61260349DF1E28611 (SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * __this, Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void Ball::DoParticles(UnityEngine.GameObject,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Ball_DoParticles_mDB8D1063295BDF10CC104A1095D8F4D00944E3DC (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObj0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___location1, const RuntimeMethod* method);
// System.Void GameControl::CorrectAnswer()
extern "C" IL2CPP_METHOD_ATTR void GameControl_CorrectAnswer_m187B452C34F34298535BA72180165166DC739F90 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C" IL2CPP_METHOD_ATTR void AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, float p0, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<UnityEngine.AudioClip>(System.String)
inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F (String_t* p0, const RuntimeMethod* method)
{
	return ((  AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m312D167000593C478994C1F4C93930C5DAED66D3_gshared)(p0, method);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C" IL2CPP_METHOD_ATTR void AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, float p1, const RuntimeMethod* method);
// System.Void GameControl::Save()
extern "C" IL2CPP_METHOD_ATTR void GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator Ball::StartNextRoundAfterDelay(System.Single)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Ball_StartNextRoundAfterDelay_m5C458064B8D41DC06C0659E8DAA76152F57CB864 (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, float ___delay0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void GameControl::IncorrectAnswer()
extern "C" IL2CPP_METHOD_ATTR void GameControl_IncorrectAnswer_mAEFC76206981E577BDD9BAA6D2CDA74D54CE40CD (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Ball>::GetEnumerator()
inline Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489 (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  (*) (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Ball>::get_Current()
inline Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E (Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF * __this, const RuntimeMethod* method)
{
	return ((  Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * (*) (Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Ball>::MoveNext()
inline bool Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076 (Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Ball>::Dispose()
inline void Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC (Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Collections.IEnumerator Ball::LoadLevelAfterDelay(System.Single)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Ball_LoadLevelAfterDelay_mE7A8F2D13D12E276E9FFD81C447E87809A8F1A5A (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, float ___delay0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p2, const RuntimeMethod* method)
{
	return ((  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m352D452C728667C9C76C942525CDE26444568ECD_gshared)(p0, p1, p2, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (int32_t p0, int32_t p1, const RuntimeMethod* method);
// UnityEngine.ContactPoint2D UnityEngine.Collision2D::GetContact(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0  Collision2D_GetContact_mFAB5C832ACF83023A161CC0F2F9E495791820E05 (Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_point()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ContactPoint2D_get_point_mFD6ED12578CD5846B0C9CCE93972B9B6EF222534 (ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 * __this, const RuntimeMethod* method);
// System.Void Ball/<StartNextRoundAfterDelay>d__17::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CStartNextRoundAfterDelayU3Ed__17__ctor_mF3882F097463558F35831836F48E7B771F873B33 (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void Ball/<LoadLevelAfterDelay>d__18::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__18__ctor_m6C63FADE77274C97CD97BAD6F6B4AAB6F4C556DF (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559 (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * __this, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9 (String_t* p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD (String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<MainCamera>()
inline MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * GameObject_GetComponent_TisMainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_m2B6BDE27F8E0A59E4BD0D9F9F65427812542DE92 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void MainCamera::StartRound()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_StartRound_m78EB3716D63672EE7D7BB8F9A0C20AEFA6689A1D (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m258051AAA1489D2D8B252815A45C1E9A2C097201 (int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
extern "C" IL2CPP_METHOD_ATTR void Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_fontSize_mA9F9F916A9BC3B81CFEE7460966FFD1E6B67F45F (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" IL2CPP_METHOD_ATTR void GUI_Label_m283D6B1DD970038379FBB974BC5A45F87CA727B6 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE  p0, String_t* p1, GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * p2, const RuntimeMethod* method);
// System.Int32 GameControl::GetLevelBase()
extern "C" IL2CPP_METHOD_ATTR int32_t GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Note>::.ctor()
inline void List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116 (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void Note::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Note__ctor_m13B09FD5AD10AE98E732FC3B88D900C819D100C6 (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, int32_t ___noteIndex0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Note>::Add(!0)
inline void List_1_Add_mE0D78113FD836E165C8F1B82481737DC8C2E51A3 (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * __this, Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *, Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void GameControl::LevelUp()
extern "C" IL2CPP_METHOD_ATTR void GameControl_LevelUp_m6DA0150E04508093785FE715C07492327DCBDB25 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BinaryFormatter__ctor_mEA8ADD359BFAC7D9E9B6183FDC1C5C80E0F29806 (BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C" IL2CPP_METHOD_ATTR String_t* Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.IO.FileStream System.IO.File::Create(System.String)
extern "C" IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * File_Create_mE6AF90C7A82E96EC1315821EB061327CF3EB55DD (String_t* p0, const RuntimeMethod* method);
// System.Void PlayerData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayerData__ctor_m01DB4046DFC9FA3BEF1BF9C3E9A877E4F4D33D63 (PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object)
extern "C" IL2CPP_METHOD_ATTR void BinaryFormatter_Serialize_mBA2FB6DB94D34F42E14DF7D788056FCF0CF41D52 (BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.IO.File::Exists(System.String)
extern "C" IL2CPP_METHOD_ATTR bool File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB (String_t* p0, const RuntimeMethod* method);
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode)
extern "C" IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * File_Open_mDA5EB4A312EAEBF8543B13C572271FB5F673A501 (String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * BinaryFormatter_Deserialize_m20A831B13DF5C3F031F2141730291630A16A32AD (BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GUIStyle__ctor_m8AA3D5AA506A252687923D0DA80741862AA83805 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A (RuntimeArray * p0, RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  p1, const RuntimeMethod* method);
// System.Void GameControl::Load()
extern "C" IL2CPP_METHOD_ATTR void GameControl_Load_mBEDB97AE94477B6D97BAD1FC7A8820AB0B5D4968 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.BoxCollider2D>()
inline BoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87 * GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  BoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared)(__this, method);
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CircleCollider2D>()
inline CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F * Component_GetComponent_TisCircleCollider2D_t862AED067B612149EF365A560B26406F6088641F_m3DAB966F6D30A22BFF7873DAA1254C2FFD25226A (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Single UnityEngine.CircleCollider2D::get_radius()
extern "C" IL2CPP_METHOD_ATTR float CircleCollider2D_get_radius_m016333FD7A5A5FD84FEFD7D02B57D4CA728EFA27 (CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Rigidbody2D>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * Object_Instantiate_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_m7A27F732EFDF61FC8EB6C64ECDBCB4E5029B798A (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p2, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * (*) (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m352D452C728667C9C76C942525CDE26444568ECD_gshared)(p0, p1, p2, method);
}
// !!0 UnityEngine.Component::GetComponent<Ball>()
inline Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * Component_GetComponent_TisBall_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3_m1EFA6F667F1EE1492D5E1FFCE95E405249525BD0 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Ball>::Add(!0)
inline void List_1_Add_m6F48A1BBD6E77DDC2CF47802AD9D4E24DF825270 (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * __this, Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 *, Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Collections.Generic.List`1<Note> GameControl::GetNoteList()
extern "C" IL2CPP_METHOD_ATTR List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * GameControl_GetNoteList_m08557D7260276BDFD31767A38D7BE9CBC627F9F3 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Ball>::Clear()
inline void List_1_Clear_m7C39C9921E33484DF592F287D97CCBDCD64E14C0 (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<Note>::get_Count()
inline int32_t List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<Note>::get_Item(System.Int32)
inline Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0 (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * (*) (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 GameControl::GetNumBubbles()
extern "C" IL2CPP_METHOD_ATTR int32_t GameControl_GetNumBubbles_mEC449EA24FCB7D7371C17D9F1AE1D2A978317BD8 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// System.Void Note::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Note__ctor_mC1987B976BE8297565673F2B85BC34F3CB6F2D65 (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, const RuntimeMethod* method);
// System.Void MainCamera::GenBall(System.Single,System.Single,System.Single,System.Single,Note)
extern "C" IL2CPP_METHOD_ATTR void MainCamera_GenBall_m55EF5932A3ED533C76096EDB8A14162AB5B1155E (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, float ___fbpToNegX0, float ___fbpToPosX1, float ___fbpToNegY2, float ___fbpToPosY3, Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * ___note4, const RuntimeMethod* method);
// System.Void MainCamera::HideBalls()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_HideBalls_mC0B833AE68EF16E75E7F7D4572C1D075C9AA5B62 (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method);
// System.Void Timer::StartTimer()
extern "C" IL2CPP_METHOD_ATTR void Timer_StartTimer_m297CF68F48B8693DAF2424C0FC96F40B7BB9C5B5 (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Ball>::.ctor()
inline void List_1__ctor_m586CA3DF487C8F421B52B94EBB8AFB966C4E3D22 (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Random::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Random__ctor_mCD4B6E9DFD27A19F52FA441CD8CAEB687A9DD2F2 (Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * __this, const RuntimeMethod* method);
// System.Void MenuLayout/<CheckForChange>d__2::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CCheckForChangeU3Ed__2__ctor_mD1F0F9D3E1D97C8F339BD0A2F40F1EBD0A109A3D (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// UnityEngine.DeviceOrientation UnityEngine.Input::get_deviceOrientation()
extern "C" IL2CPP_METHOD_ATTR int32_t Input_get_deviceOrientation_mE405D05D2FEF3E94CEB5886E51C8E4E752AF883B (const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018 (String_t* __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * GameObject_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_mCDC3359066029BF682DB5CC73FECB9C648B1BE8E (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Single GameControl::GetRoundTime()
extern "C" IL2CPP_METHOD_ATTR float GameControl_GetRoundTime_mF013DFD64E5B774736534CD07229DAA223A9E352 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.String System.String::ToUpper()
extern "C" IL2CPP_METHOD_ATTR String_t* String_ToUpper_m23D019B7C5EF2C5C01F524EB8137A424B33EEFE2 (String_t* __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Image_set_fillAmount_mA775A069067A26F0F506F6590D05337C2F08A030 (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, float p0, const RuntimeMethod* method);
// System.Void MainCamera::ShowBalls()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_ShowBalls_mF2D16795F6AA46BD96C109D468AD2C3C4063875D (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator Timer::LoadLevelAfterDelay(System.Single)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Timer_LoadLevelAfterDelay_mA28E0E56333FA52D98A2FBA9D42AC3240354E26E (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * __this, float ___delay0, const RuntimeMethod* method);
// System.Void Timer/<LoadLevelAfterDelay>d__14::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__14__ctor_m0C028405AF7A6E73BD69C37E11796B07AD3D4CDF (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AudioScript::Start()
extern "C" IL2CPP_METHOD_ATTR void AudioScript_Start_mBC3EAA978B424134C50E93B1F89AF22DE99DEFB3 (AudioScript_t5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB * __this, const RuntimeMethod* method)
{
	{
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_0 = __this->get_MusicSource_5();
		AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * L_1 = __this->get_MusicClip_4();
		NullCheck(L_0);
		AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioScript::Update()
extern "C" IL2CPP_METHOD_ATTR void AudioScript_Update_m2857D4470E2CDD80848A2DE04D1B337ED6DEF24E (AudioScript_t5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void AudioScript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AudioScript__ctor_mA595E7543ED5A6B316E080C1B38873923EA68BA9 (AudioScript_t5A84B2D9A6BBBD54FEE9088A6023770616BC5DDB * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Ball::Start()
extern "C" IL2CPP_METHOD_ATTR void Ball_Start_mDDC34813CC3D81185647763525B29D09F3DB8C73 (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_Start_mDDC34813CC3D81185647763525B29D09F3DB8C73_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* V_3 = NULL;
	{
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_0 = __this->get_note_14();
		NullCheck(L_0);
		String_t* L_1 = Note_get_theNote_mD93A18AA36F41A8FAD9E225130C45FC8C1CCEDE2(L_0, /*hidden argument*/NULL);
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		float L_4 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		V_0 = L_4;
		float L_5 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		float L_6 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		if ((!(((float)L_5) < ((float)L_6))))
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		float L_7 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		V_0 = L_7;
	}

IL_004e:
	{
		float L_8 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((1.5f), (2.0f), /*hidden argument*/NULL);
		V_1 = L_8;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		float L_10 = V_0;
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_14), ((float)((float)L_10/(float)L_11)), ((float)((float)L_12/(float)L_13)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_14, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_9, L_15, /*hidden argument*/NULL);
		float L_16 = Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C(/*hidden argument*/NULL);
		float L_17 = Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C(/*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_2), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_16, (float)(0.5f))), (float)(3.0f))), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_17, (float)(0.5f))), (float)(3.0f))), (0.0f), /*hidden argument*/NULL);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_18 = __this->get_ball_4();
		NullCheck(L_18);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_19 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_20 = GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72(L_19, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72_RuntimeMethod_var);
		NullCheck(L_20);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_20, (bool)1, /*hidden argument*/NULL);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_21 = __this->get_ball_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_23 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C(L_21, L_23, 1, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_24 = Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8_RuntimeMethod_var);
		NullCheck(L_24);
		AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128(L_24, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_25 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* L_26 = GameObject_GetComponents_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m0527EC164E9C63C03797F7F71607A88771DDE1B4(L_25, /*hidden argument*/GameObject_GetComponents_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m0527EC164E9C63C03797F7F71607A88771DDE1B4_RuntimeMethod_var);
		V_3 = L_26;
		AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* L_27 = V_3;
		NullCheck(L_27);
		int32_t L_28 = 0;
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		__this->set_collisionAudioSource_11(L_29);
		AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* L_30 = V_3;
		NullCheck(L_30);
		int32_t L_31 = 1;
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		__this->set_noteAudioSource_12(L_32);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_33 = __this->get_ball_4();
		NullCheck(L_33);
		SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32* L_34 = Component_GetComponentsInChildren_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mA1B86DC3DA1A3CE3E451FA4F1952456C0AE938F3(L_33, /*hidden argument*/Component_GetComponentsInChildren_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mA1B86DC3DA1A3CE3E451FA4F1952456C0AE938F3_RuntimeMethod_var);
		NullCheck(L_34);
		int32_t L_35 = 1;
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_37 = __this->get_note_14();
		NullCheck(L_37);
		String_t* L_38 = Note_get_theNote_mD93A18AA36F41A8FAD9E225130C45FC8C1CCEDE2(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_38, /*hidden argument*/NULL);
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_40 = __this->get_note_14();
		NullCheck(L_40);
		int32_t L_41 = Note_get_Octave_m0886E707D45BB24FAAA7A11950C13C847063077D(L_40, /*hidden argument*/NULL);
		int32_t L_42 = L_41;
		RuntimeObject * L_43 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_42);
		String_t* L_44 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteralFE2E4EE3AD4A13C6904C5AB69929F7AD1CE1203F, L_39, L_43, /*hidden argument*/NULL);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_45 = Resources_Load_TisSprite_tCA09498D612D08DE668653AF1E9C12BF53434198_m098412B7E141B9C12C6A755BF488DB1902FF9A97(L_44, /*hidden argument*/Resources_Load_TisSprite_tCA09498D612D08DE668653AF1E9C12BF53434198_m098412B7E141B9C12C6A755BF488DB1902FF9A97_RuntimeMethod_var);
		NullCheck(L_36);
		SpriteRenderer_set_sprite_m9F5C8B2007AA03FAB66F0CB61260349DF1E28611(L_36, L_45, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ball::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Ball_FixedUpdate_m003D9BABEE449CA00828FD18069E900E286D96CF (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Ball::OnMouseDown()
extern "C" IL2CPP_METHOD_ATTR void Ball_OnMouseDown_mBDE8F785E5F59C095C108A67E0409A4241520D46 (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_OnMouseDown_mBDE8F785E5F59C095C108A67E0409A4241520D46_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		bool L_0 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_gameOn_16();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_1 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA_RuntimeMethod_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_1, (bool)1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void Ball::popBubble()
extern "C" IL2CPP_METHOD_ATTR void Ball_popBubble_m8EF60E2CAC7A2822CFA3FBDECCF0FBF847471973 (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_popBubble_m8EF60E2CAC7A2822CFA3FBDECCF0FBF847471973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_1 = GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996_RuntimeMethod_var);
		NullCheck(L_1);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_1, (0.0f), /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_popEffectObj_8();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_x_2();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_9), L_5, L_8, (0.0f), /*hidden argument*/NULL);
		Ball_DoParticles_mDB8D1063295BDF10CC104A1095D8F4D00944E3DC(__this, L_2, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		bool L_10 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_gameOn_16();
		if (!L_10)
		{
			goto IL_0216;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		String_t* L_11 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_theAnswer_6();
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_12 = __this->get_note_14();
		NullCheck(L_12);
		String_t* L_13 = Note_get_theNote_mD93A18AA36F41A8FAD9E225130C45FC8C1CCEDE2(L_12, /*hidden argument*/NULL);
		bool L_14 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_11, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0131;
		}
	}
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_15 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_15);
		GameControl_CorrectAnswer_m187B452C34F34298535BA72180165166DC739F90(L_15, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_16 = __this->get_noteAudioSource_12();
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_17 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_17);
		float L_18 = L_17->get_sfxVolume_8();
		NullCheck(L_16);
		AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06(L_16, L_18, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_19 = __this->get_noteAudioSource_12();
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_20 = __this->get_note_14();
		NullCheck(L_20);
		String_t* L_21 = Note_get_theNote_mD93A18AA36F41A8FAD9E225130C45FC8C1CCEDE2(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_21, /*hidden argument*/NULL);
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_23 = __this->get_note_14();
		NullCheck(L_23);
		int32_t L_24 = Note_get_Octave_m0886E707D45BB24FAAA7A11950C13C847063077D(L_23, /*hidden argument*/NULL);
		int32_t L_25 = L_24;
		RuntimeObject * L_26 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_25);
		String_t* L_27 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteral20889B47F440A2CF9FB9939B043699BEF0A898A9, L_22, L_26, /*hidden argument*/NULL);
		AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * L_28 = Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F(L_27, /*hidden argument*/Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F_RuntimeMethod_var);
		NullCheck(L_19);
		AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD(L_19, L_28, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D(L_29, (3.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		int32_t L_30 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_numCorrectAnswers_18();
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_numCorrectAnswers_18(((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1)));
		int32_t L_31 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_numCorrectAnswers_18();
		int32_t L_32 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_numPossibleAnswers_17();
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_0216;
		}
	}
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_33 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_33);
		GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD(L_33, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_34 = __this->get_noteAudioSource_12();
		AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * L_35 = Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F(_stringLiteralF93EE3D08E02119EFE5A9C363603C595399BCBB2, /*hidden argument*/Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F_RuntimeMethod_var);
		NullCheck(L_34);
		AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD(L_34, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_gameOn_16((bool)0);
		((Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields*)il2cpp_codegen_static_fields_for(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_il2cpp_TypeInfo_var))->set_runTimer_11((bool)0);
		RuntimeObject* L_36 = Ball_StartNextRoundAfterDelay_m5C458064B8D41DC06C0659E8DAA76152F57CB864(__this, (2.5f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_36, /*hidden argument*/NULL);
		return;
	}

IL_0131:
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_37 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_37);
		GameControl_IncorrectAnswer_mAEFC76206981E577BDD9BAA6D2CDA74D54CE40CD(L_37, /*hidden argument*/NULL);
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_38 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_38);
		GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD(L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_39 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_ballList_15();
		NullCheck(L_39);
		Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  L_40 = List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489(L_39, /*hidden argument*/List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489_RuntimeMethod_var);
		V_0 = L_40;
	}

IL_0150:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01b5;
		}

IL_0152:
		{
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_41 = Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E_RuntimeMethod_var);
			V_1 = L_41;
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_42 = V_1;
			NullCheck(L_42);
			bool L_43 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_42, NULL);
			if (L_43)
			{
				goto IL_01b5;
			}
		}

IL_0163:
		{
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_44 = V_1;
			NullCheck(L_44);
			Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_45 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA(L_44, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA_RuntimeMethod_var);
			NullCheck(L_45);
			Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_45, (bool)1, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_46 = __this->get_popEffectObj_8();
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_47 = V_1;
			NullCheck(L_47);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_48 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_47, /*hidden argument*/NULL);
			NullCheck(L_48);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_48, /*hidden argument*/NULL);
			float L_50 = L_49.get_x_2();
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_51 = V_1;
			NullCheck(L_51);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_52 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_51, /*hidden argument*/NULL);
			NullCheck(L_52);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_53 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_52, /*hidden argument*/NULL);
			float L_54 = L_53.get_y_3();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55;
			memset(&L_55, 0, sizeof(L_55));
			Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_55), L_50, L_54, (0.0f), /*hidden argument*/NULL);
			Ball_DoParticles_mDB8D1063295BDF10CC104A1095D8F4D00944E3DC(__this, L_46, L_55, /*hidden argument*/NULL);
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_56 = V_1;
			NullCheck(L_56);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_57 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_56, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D(L_57, (4.0f), /*hidden argument*/NULL);
		}

IL_01b5:
		{
			bool L_58 = Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076_RuntimeMethod_var);
			if (L_58)
			{
				goto IL_0152;
			}
		}

IL_01be:
		{
			IL2CPP_LEAVE(0x1CE, FINALLY_01c0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01c0;
	}

FINALLY_01c0:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC_RuntimeMethod_var);
		IL2CPP_END_FINALLY(448)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(448)
	{
		IL2CPP_JUMP_TBL(0x1CE, IL_01ce)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01ce:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_gameOn_16((bool)0);
		((Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields*)il2cpp_codegen_static_fields_for(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_il2cpp_TypeInfo_var))->set_runTimer_11((bool)0);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_59 = __this->get_noteAudioSource_12();
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_60 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_60);
		float L_61 = L_60->get_sfxVolume_8();
		NullCheck(L_59);
		AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06(L_59, L_61, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_62 = __this->get_noteAudioSource_12();
		AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * L_63 = Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F(_stringLiteral6105C32FEC73E438F25D3CC1CD9533F5A37249B7, /*hidden argument*/Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F_RuntimeMethod_var);
		NullCheck(L_62);
		AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD(L_62, L_63, /*hidden argument*/NULL);
		RuntimeObject* L_64 = Ball_LoadLevelAfterDelay_mE7A8F2D13D12E276E9FFD81C447E87809A8F1A5A(__this, (2.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_64, /*hidden argument*/NULL);
	}

IL_0216:
	{
		return;
	}
}
// System.Void Ball::DoParticles(UnityEngine.GameObject,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Ball_DoParticles_mDB8D1063295BDF10CC104A1095D8F4D00944E3DC (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObj0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___location1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_DoParticles_mDB8D1063295BDF10CC104A1095D8F4D00944E3DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		bool L_0 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_gameOn_16();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ___gameObj0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___location1;
		float L_3 = L_2.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___location1;
		float L_5 = L_4.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_6), L_3, L_5, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_7 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F(L_1, L_6, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
// System.Void Ball::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Ball_OnCollisionEnter2D_m2235B169D9A924C84E5C68015086634B2B826B95 (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_OnCollisionEnter2D_m2235B169D9A924C84E5C68015086634B2B826B95_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		bool L_0 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_gameOn_16();
		if (!L_0)
		{
			goto IL_0080;
		}
	}
	{
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_1 = __this->get_collisionAudioSource_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0080;
		}
	}
	{
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_3 = __this->get_collisionAudioSource_11();
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_4 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_4);
		float L_5 = L_4->get_sfxVolume_8();
		NullCheck(L_3);
		AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06(L_3, L_5, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_6 = __this->get_collisionAudioSource_11();
		AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* L_7 = __this->get_cseArr_13();
		AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* L_8 = __this->get_cseArr_13();
		NullCheck(L_8);
		int32_t L_9 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, (((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_10 = L_9;
		AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD(L_6, L_11, /*hidden argument*/NULL);
		Collision2D_t45DC963DE1229CFFC7D0B666800F0AE93688764D * L_12 = ___collision0;
		NullCheck(L_12);
		ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0  L_13 = Collision2D_GetContact_mFAB5C832ACF83023A161CC0F2F9E495791820E05(L_12, 0, /*hidden argument*/NULL);
		V_0 = L_13;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_collideEffectObj_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_15 = ContactPoint2D_get_point_mFD6ED12578CD5846B0C9CCE93972B9B6EF222534((ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 *)(&V_0), /*hidden argument*/NULL);
		float L_16 = L_15.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = ContactPoint2D_get_point_mFD6ED12578CD5846B0C9CCE93972B9B6EF222534((ContactPoint2D_t7DE4097DD62E4240F4629EBB41F4BF089141E2C0 *)(&V_0), /*hidden argument*/NULL);
		float L_18 = L_17.get_y_1();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_19), L_16, L_18, (0.0f), /*hidden argument*/NULL);
		Ball_DoParticles_mDB8D1063295BDF10CC104A1095D8F4D00944E3DC(__this, L_14, L_19, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Collections.IEnumerator Ball::StartNextRoundAfterDelay(System.Single)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Ball_StartNextRoundAfterDelay_m5C458064B8D41DC06C0659E8DAA76152F57CB864 (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, float ___delay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_StartNextRoundAfterDelay_m5C458064B8D41DC06C0659E8DAA76152F57CB864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * L_0 = (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 *)il2cpp_codegen_object_new(U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362_il2cpp_TypeInfo_var);
		U3CStartNextRoundAfterDelayU3Ed__17__ctor_mF3882F097463558F35831836F48E7B771F873B33(L_0, 0, /*hidden argument*/NULL);
		U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * L_1 = L_0;
		float L_2 = ___delay0;
		NullCheck(L_1);
		L_1->set_delay_2(L_2);
		return L_1;
	}
}
// System.Collections.IEnumerator Ball::LoadLevelAfterDelay(System.Single)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Ball_LoadLevelAfterDelay_mE7A8F2D13D12E276E9FFD81C447E87809A8F1A5A (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, float ___delay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_LoadLevelAfterDelay_mE7A8F2D13D12E276E9FFD81C447E87809A8F1A5A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * L_0 = (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 *)il2cpp_codegen_object_new(U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086_il2cpp_TypeInfo_var);
		U3CLoadLevelAfterDelayU3Ed__18__ctor_m6C63FADE77274C97CD97BAD6F6B4AAB6F4C556DF(L_0, 0, /*hidden argument*/NULL);
		U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * L_1 = L_0;
		float L_2 = ___delay0;
		NullCheck(L_1);
		L_1->set_delay_2(L_2);
		return L_1;
	}
}
// System.Void Ball::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Ball__ctor_mCF954B56503D505E1FC04F582DD66B086098B67A (Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Ball/<LoadLevelAfterDelay>d__18::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__18__ctor_m6C63FADE77274C97CD97BAD6F6B4AAB6F4C556DF (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Ball/<LoadLevelAfterDelay>d__18::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__18_System_IDisposable_Dispose_m66FD089D85B04584BB06F9E3D7B03A84A82D3338 (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Ball/<LoadLevelAfterDelay>d__18::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CLoadLevelAfterDelayU3Ed__18_MoveNext_m9415CFC69489F6D0E8ED1321D03DA436B76376E4 (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadLevelAfterDelayU3Ed__18_MoveNext_m9415CFC69489F6D0E8ED1321D03DA436B76376E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_delay_2();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_4 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(_stringLiteralEABCFFD8D5120B660823E2C294A8DC252DA5EA29, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object Ball/<LoadLevelAfterDelay>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadLevelAfterDelayU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB474A78A93A633778AFEC77854A9F6144AFE1498 (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Ball/<LoadLevelAfterDelay>d__18::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__18_System_Collections_IEnumerator_Reset_mFCB6C49D1A3FA13D71AAA216E40504EB977D08CE (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadLevelAfterDelayU3Ed__18_System_Collections_IEnumerator_Reset_mFCB6C49D1A3FA13D71AAA216E40504EB977D08CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CLoadLevelAfterDelayU3Ed__18_System_Collections_IEnumerator_Reset_mFCB6C49D1A3FA13D71AAA216E40504EB977D08CE_RuntimeMethod_var);
	}
}
// System.Object Ball/<LoadLevelAfterDelay>d__18::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadLevelAfterDelayU3Ed__18_System_Collections_IEnumerator_get_Current_m828743F0853BDBB6AF1514022D501DDB9D8C3EEE (U3CLoadLevelAfterDelayU3Ed__18_t913ACD66B31877FB20D9DF596D8D69C10FB74086 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Ball/<StartNextRoundAfterDelay>d__17::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CStartNextRoundAfterDelayU3Ed__17__ctor_mF3882F097463558F35831836F48E7B771F873B33 (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Ball/<StartNextRoundAfterDelay>d__17::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CStartNextRoundAfterDelayU3Ed__17_System_IDisposable_Dispose_m2E4DE0C1BAF7ED81ED33027F76CBDD297E03512B (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Ball/<StartNextRoundAfterDelay>d__17::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CStartNextRoundAfterDelayU3Ed__17_MoveNext_mF4E1A991B96F77DD5B12A46607092CD47F3283B5 (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartNextRoundAfterDelayU3Ed__17_MoveNext_mF4E1A991B96F77DD5B12A46607092CD47F3283B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_delay_2();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_4 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(_stringLiteral1994557C9850CA12CEDF370B8F7E1A2E35748A9C, /*hidden argument*/NULL);
		NullCheck(L_5);
		MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * L_6 = GameObject_GetComponent_TisMainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_m2B6BDE27F8E0A59E4BD0D9F9F65427812542DE92(L_5, /*hidden argument*/GameObject_GetComponent_TisMainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_m2B6BDE27F8E0A59E4BD0D9F9F65427812542DE92_RuntimeMethod_var);
		NullCheck(L_6);
		MainCamera_StartRound_m78EB3716D63672EE7D7BB8F9A0C20AEFA6689A1D(L_6, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object Ball/<StartNextRoundAfterDelay>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CStartNextRoundAfterDelayU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD8457B4ABF7285B0E5BDD040B57C7EB6FAF2BC3 (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Ball/<StartNextRoundAfterDelay>d__17::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CStartNextRoundAfterDelayU3Ed__17_System_Collections_IEnumerator_Reset_m2AB6F1864DDED9EC481CE5B0AE98BC1F1FE1507D (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartNextRoundAfterDelayU3Ed__17_System_Collections_IEnumerator_Reset_m2AB6F1864DDED9EC481CE5B0AE98BC1F1FE1507D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CStartNextRoundAfterDelayU3Ed__17_System_Collections_IEnumerator_Reset_m2AB6F1864DDED9EC481CE5B0AE98BC1F1FE1507D_RuntimeMethod_var);
	}
}
// System.Object Ball/<StartNextRoundAfterDelay>d__17::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CStartNextRoundAfterDelayU3Ed__17_System_Collections_IEnumerator_get_Current_mF66501AC6D2097F821692A7A684123AAD49B271B (U3CStartNextRoundAfterDelayU3Ed__17_t2336B7CD61FC8A8B347C6A29DE116B49D07A9362 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BubbleBlue::Start()
extern "C" IL2CPP_METHOD_ATTR void BubbleBlue_Start_mDE66FF1780D428FDE94CA18B36F75EDF42CDD191 (BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void BubbleBlue::Update()
extern "C" IL2CPP_METHOD_ATTR void BubbleBlue_Update_m9F22419819716B100470171BCFD1C9E41B515FBF (BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void BubbleBlue::StartOfBubbleAnimation()
extern "C" IL2CPP_METHOD_ATTR void BubbleBlue_StartOfBubbleAnimation_m9C5479E6D013E773B7FE49CE7B2D7E77B039414D (BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * __this, const RuntimeMethod* method)
{
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get_animator_4();
		int32_t L_1 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(1, 3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void BubbleBlue::StopAnimating()
extern "C" IL2CPP_METHOD_ATTR void BubbleBlue_StopAnimating_m3688AE7F3AD24B7859261BA2F1E45C6D161C13AE (BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * __this, const RuntimeMethod* method)
{
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get_animator_4();
		NullCheck(L_0);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_0, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void BubbleBlue::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BubbleBlue__ctor_m559348B1D426F757916C20ABEB2C3C20F97DCF67 (BubbleBlue_t45A8F3C166C89E6648F24B58288914702EE867EF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonFunctions::LoadByIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ButtonFunctions_LoadByIndex_mE690CB2009749257C8E90C0F7A065C66A8AE1850 (ButtonFunctions_tBA2ACAB62354CE0CB45267950432DF1D6A8E542A * __this, int32_t ___sceneIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonFunctions_LoadByIndex_mE690CB2009749257C8E90C0F7A065C66A8AE1850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_0 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_0);
		GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD(L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___sceneIndex0;
		SceneManager_LoadScene_m258051AAA1489D2D8B252815A45C1E9A2C097201(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonFunctions::ExitGame()
extern "C" IL2CPP_METHOD_ATTR void ButtonFunctions_ExitGame_m82B14835541FC4CB35A892B8909BDDFDC26445DB (ButtonFunctions_tBA2ACAB62354CE0CB45267950432DF1D6A8E542A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonFunctions_ExitGame_m82B14835541FC4CB35A892B8909BDDFDC26445DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_0 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_0);
		GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD(L_0, /*hidden argument*/NULL);
		Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95(/*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonFunctions::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ButtonFunctions__ctor_mB9ABB2B69AD7CA9195C1CD22A05C52FE2D52D2D1 (ButtonFunctions_tBA2ACAB62354CE0CB45267950432DF1D6A8E542A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CollisionPS::Start()
extern "C" IL2CPP_METHOD_ATTR void CollisionPS_Start_m738CA4EEC04BF533BAF22E38E71CAD98BF0BE31A (CollisionPS_t63D9F9E34A1D39F94602B7F3DA4D196029A20801 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CollisionPS::Update()
extern "C" IL2CPP_METHOD_ATTR void CollisionPS_Update_m310849435BD29C93B8880C952AF131DD99D9B676 (CollisionPS_t63D9F9E34A1D39F94602B7F3DA4D196029A20801 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CollisionPS::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CollisionPS__ctor_m59B5B19A559F72CC4645EAD9D83E9637A4D8B60B (CollisionPS_t63D9F9E34A1D39F94602B7F3DA4D196029A20801 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CollisionScript::Start()
extern "C" IL2CPP_METHOD_ATTR void CollisionScript_Start_m3A2FE0EFA2BB2342900BBC5972EE532B5CB12DE1 (CollisionScript_tAAD9616D66499CF3EE2069913094BC14012990C4 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CollisionScript::Update()
extern "C" IL2CPP_METHOD_ATTR void CollisionScript_Update_m2BB6231E762D8B4A48A60AB4F2F047A3BD8FD04E (CollisionScript_tAAD9616D66499CF3EE2069913094BC14012990C4 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CollisionScript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CollisionScript__ctor_mAC9163FAFBB1D31B38FA0D9F19E826A2F0FF1907 (CollisionScript_tAAD9616D66499CF3EE2069913094BC14012990C4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameControl::Awake()
extern "C" IL2CPP_METHOD_ATTR void GameControl_Awake_m7DCD4282485D4BFA123E5E7DE9933F241E7C44DD (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameControl_Awake_m7DCD4282485D4BFA123E5E7DE9933F241E7C44DD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_0 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207(L_2, /*hidden argument*/NULL);
		((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->set_control_4(__this);
		return;
	}

IL_001f:
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_3 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_5, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void GameControl::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void GameControl_OnGUI_m9420AEB8AF63DBB3AD8F729B7D1D9EB3EB7C57BA (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameControl_OnGUI_m9420AEB8AF63DBB3AD8F729B7D1D9EB3EB7C57BA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_0 = __this->get_guiStyle_9();
		NullCheck(L_0);
		GUIStyle_set_fontSize_mA9F9F916A9BC3B81CFEE7460966FFD1E6B67F45F(L_0, ((int32_t)36), /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_1), (10.0f), (10.0f), (100.0f), (30.0f), /*hidden argument*/NULL);
		int32_t L_2 = __this->get_level_5();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_3);
		String_t* L_5 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral6B9ACFFF7C03940622A9EE21C81E8EED7407A09F, L_4, /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_6 = __this->get_guiStyle_9();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3E5CBC6B113E392EBBE1453DEF2B7CD020F345AA_il2cpp_TypeInfo_var);
		GUI_Label_m283D6B1DD970038379FBB974BC5A45F87CA727B6(L_1, L_5, L_6, /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_7), (10.0f), (40.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		float L_8 = __this->get_experience_6();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_9);
		String_t* L_11 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral57B3C08D169273340910055DDEF7535B8DA26BD2, L_10, /*hidden argument*/NULL);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_12 = __this->get_guiStyle_9();
		GUI_Label_m283D6B1DD970038379FBB974BC5A45F87CA727B6(L_7, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GameControl::GetNumBubbles()
extern "C" IL2CPP_METHOD_ATTR int32_t GameControl_GetNumBubbles_mEC449EA24FCB7D7371C17D9F1AE1D2A978317BD8 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_0 = __this->get_numBubbles_BL_11();
		int32_t L_1 = GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Single GameControl::GetRoundTime()
extern "C" IL2CPP_METHOD_ATTR float GameControl_GetRoundTime_mF013DFD64E5B774736534CD07229DAA223A9E352 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = __this->get_roundTime_BL_13();
		int32_t L_1 = GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Int32 GameControl::GetLevelBase()
extern "C" IL2CPP_METHOD_ATTR int32_t GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_level_5();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		double L_1 = floor((((double)((double)((float)((float)(((float)((float)L_0)))/(float)(10.0f)))))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Collections.Generic.List`1<Note> GameControl::GetNoteList()
extern "C" IL2CPP_METHOD_ATTR List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * GameControl_GetNoteList_m08557D7260276BDFD31767A38D7BE9CBC627F9F3 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameControl_GetNoteList_m08557D7260276BDFD31767A38D7BE9CBC627F9F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_0 = (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *)il2cpp_codegen_object_new(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784_il2cpp_TypeInfo_var);
		List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116(L_0, /*hidden argument*/List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0021;
	}

IL_000a:
	{
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_1 = V_0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_2 = __this->get_notesToInclude_14();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_6 = (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B *)il2cpp_codegen_object_new(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B_il2cpp_TypeInfo_var);
		Note__ctor_m13B09FD5AD10AE98E732FC3B88D900C819D100C6(L_6, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_mE0D78113FD836E165C8F1B82481737DC8C2E51A3(L_1, L_6, /*hidden argument*/List_1_Add_mE0D78113FD836E165C8F1B82481737DC8C2E51A3_RuntimeMethod_var);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_8 = V_1;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_9 = __this->get_numPossNotes_BL_12();
		int32_t L_10 = GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		if ((((int32_t)L_8) < ((int32_t)L_12)))
		{
			goto IL_000a;
		}
	}
	{
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_13 = V_0;
		return L_13;
	}
}
// System.Void GameControl::CorrectAnswer()
extern "C" IL2CPP_METHOD_ATTR void GameControl_CorrectAnswer_m187B452C34F34298535BA72180165166DC739F90 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_experience_6();
		__this->set_experience_6(((float)il2cpp_codegen_add((float)L_0, (float)(10.0f))));
		float L_1 = __this->get_experience_6();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_2 = __this->get_expToLvl_BL_10();
		int32_t L_3 = GameControl_GetLevelBase_m61177CD2364567BD639286E4A815C812D282FAEA(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		if ((!(((float)L_1) >= ((float)(((float)((float)L_5)))))))
		{
			goto IL_0039;
		}
	}
	{
		GameControl_LevelUp_m6DA0150E04508093785FE715C07492327DCBDB25(__this, /*hidden argument*/NULL);
		__this->set_experience_6((0.0f));
	}

IL_0039:
	{
		return;
	}
}
// System.Void GameControl::IncorrectAnswer()
extern "C" IL2CPP_METHOD_ATTR void GameControl_IncorrectAnswer_mAEFC76206981E577BDD9BAA6D2CDA74D54CE40CD (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_experience_6();
		if ((!(((float)L_0) > ((float)(20.0f)))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = __this->get_experience_6();
		__this->set_experience_6(((float)il2cpp_codegen_subtract((float)L_1, (float)(40.0f))));
		return;
	}

IL_0020:
	{
		__this->set_experience_6((0.0f));
		return;
	}
}
// System.Void GameControl::LevelUp()
extern "C" IL2CPP_METHOD_ATTR void GameControl_LevelUp_m6DA0150E04508093785FE715C07492327DCBDB25 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_level_5();
		__this->set_level_5(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		return;
	}
}
// System.Void GameControl::Save()
extern "C" IL2CPP_METHOD_ATTR void GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameControl_Save_m79C4677DFBF0D73CAC4CF65A2227A198495A4ACD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * V_0 = NULL;
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * V_1 = NULL;
	{
		BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0 * L_0 = (BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0 *)il2cpp_codegen_object_new(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_mEA8ADD359BFAC7D9E9B6183FDC1C5C80E0F29806(L_0, /*hidden argument*/NULL);
		String_t* L_1 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_2 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_1, _stringLiteralE202493A5370BDEF61FBD54C32E91081454D6606, /*hidden argument*/NULL);
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_3 = File_Create_mE6AF90C7A82E96EC1315821EB061327CF3EB55DD(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_4 = (PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 *)il2cpp_codegen_object_new(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043_il2cpp_TypeInfo_var);
		PlayerData__ctor_m01DB4046DFC9FA3BEF1BF9C3E9A877E4F4D33D63(L_4, /*hidden argument*/NULL);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_5 = L_4;
		int32_t L_6 = __this->get_level_5();
		NullCheck(L_5);
		L_5->set_level_0(L_6);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_7 = L_5;
		float L_8 = __this->get_experience_6();
		NullCheck(L_7);
		L_7->set_experience_1(L_8);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_9 = L_7;
		float L_10 = __this->get_musicVolume_7();
		NullCheck(L_9);
		L_9->set_musicVolume_2(L_10);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_11 = L_9;
		float L_12 = __this->get_sfxVolume_8();
		NullCheck(L_11);
		L_11->set_sfxVolume_3(L_12);
		V_1 = L_11;
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_13 = V_1;
		NullCheck(L_13);
		L_13->set_level_0(((int32_t)55));
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_14 = V_0;
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_15 = V_1;
		NullCheck(L_0);
		BinaryFormatter_Serialize_mBA2FB6DB94D34F42E14DF7D788056FCF0CF41D52(L_0, L_14, L_15, /*hidden argument*/NULL);
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_16 = V_0;
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_16);
		return;
	}
}
// System.Void GameControl::Load()
extern "C" IL2CPP_METHOD_ATTR void GameControl_Load_mBEDB97AE94477B6D97BAD1FC7A8820AB0B5D4968 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameControl_Load_mBEDB97AE94477B6D97BAD1FC7A8820AB0B5D4968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * V_0 = NULL;
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * V_1 = NULL;
	{
		String_t* L_0 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_0, _stringLiteralE202493A5370BDEF61FBD54C32E91081454D6606, /*hidden argument*/NULL);
		bool L_2 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0073;
		}
	}
	{
		BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0 * L_3 = (BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0 *)il2cpp_codegen_object_new(BinaryFormatter_t116398AB9D7E425E4CFF83C37824A46443A2E6D0_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_mEA8ADD359BFAC7D9E9B6183FDC1C5C80E0F29806(L_3, /*hidden argument*/NULL);
		String_t* L_4 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_5 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_4, _stringLiteralE202493A5370BDEF61FBD54C32E91081454D6606, /*hidden argument*/NULL);
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_6 = File_Open_mDA5EB4A312EAEBF8543B13C572271FB5F673A501(L_5, 3, /*hidden argument*/NULL);
		V_0 = L_6;
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_7 = V_0;
		NullCheck(L_3);
		RuntimeObject * L_8 = BinaryFormatter_Deserialize_m20A831B13DF5C3F031F2141730291630A16A32AD(L_3, L_7, /*hidden argument*/NULL);
		V_1 = ((PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 *)CastclassClass((RuntimeObject*)L_8, PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043_il2cpp_TypeInfo_var));
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_9);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_level_0();
		__this->set_level_5(L_11);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_12 = V_1;
		NullCheck(L_12);
		float L_13 = L_12->get_experience_1();
		__this->set_experience_6(L_13);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_14 = V_1;
		NullCheck(L_14);
		float L_15 = L_14->get_musicVolume_2();
		__this->set_musicVolume_7(L_15);
		PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * L_16 = V_1;
		NullCheck(L_16);
		float L_17 = L_16->get_sfxVolume_3();
		__this->set_sfxVolume_8(L_17);
	}

IL_0073:
	{
		return;
	}
}
// System.Void GameControl::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GameControl__ctor_m6DCEE981B44B5B587E589B227A098AE27ED86C78 (GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameControl__ctor_m6DCEE981B44B5B587E589B227A098AE27ED86C78_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_0 = (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 *)il2cpp_codegen_object_new(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m8AA3D5AA506A252687923D0DA80741862AA83805(L_0, /*hidden argument*/NULL);
		__this->set_guiStyle_9(L_0);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_1 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)((int32_t)27));
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_2 = L_1;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_3 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____CF829E312D8BFDA60F0842C6A044BE77B6C5883D_3_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_2, L_3, /*hidden argument*/NULL);
		__this->set_expToLvl_BL_10(L_2);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_4 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)((int32_t)27));
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_5 = L_4;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_6 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____A265A6CED05B2457B5DB2B8AE4B7775CF3F75004_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_5, L_6, /*hidden argument*/NULL);
		__this->set_numBubbles_BL_11(L_5);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_7 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)((int32_t)27));
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_8 = L_7;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_9 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____6FC197002B3B804BF6C339C44A94A60A8622BC93_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_8, L_9, /*hidden argument*/NULL);
		__this->set_numPossNotes_BL_12(L_8);
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_10 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)((int32_t)27));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_11 = L_10;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_12 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____CAD08EB04AFAE771A9BE8CDA687EC435D84090A9_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_11, L_12, /*hidden argument*/NULL);
		__this->set_roundTime_BL_13(L_11);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_13 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)((int32_t)29));
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_14 = L_13;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_15 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____EFE5D31872C3AD74482056589725763E97F501B9_4_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_14, L_15, /*hidden argument*/NULL);
		__this->set_notesToInclude_14(L_14);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GuitarSounds::Start()
extern "C" IL2CPP_METHOD_ATTR void GuitarSounds_Start_mC74960F35A2FC80E86DE51A014874533C4C7F621 (GuitarSounds_t69398102599F6F9CE64D066A848098D68AECE165 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GuitarSounds::Update()
extern "C" IL2CPP_METHOD_ATTR void GuitarSounds_Update_mB07E4D1045144F3DA1E65EB2A26E0A31CD222F96 (GuitarSounds_t69398102599F6F9CE64D066A848098D68AECE165 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GuitarSounds::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GuitarSounds__ctor_mAD1E466845A9A7FEA23609C0FCE9FE8F7266BC45 (GuitarSounds_t69398102599F6F9CE64D066A848098D68AECE165 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainCamera::Start()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_Start_mBE9D257EA9AE8C3D6EAA5AECF1AFB5AABC62B73F (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera_Start_mBE9D257EA9AE8C3D6EAA5AECF1AFB5AABC62B73F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_0 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_0);
		GameControl_Load_mBEDB97AE94477B6D97BAD1FC7A8820AB0B5D4968(L_0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D(L_1, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_1, /*hidden argument*/NULL);
		__this->set_topCollider_8(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_3, /*hidden argument*/NULL);
		__this->set_bottomCollider_9(L_4);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D(L_5, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_5, /*hidden argument*/NULL);
		__this->set_rightCollider_11(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D(L_7, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		__this->set_leftCollider_10(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = __this->get_topCollider_8();
		NullCheck(L_9);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_9, _stringLiteralAAB03F7C7C5F55684470A259A8D0708E0C2A30F6, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = __this->get_bottomCollider_9();
		NullCheck(L_10);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_10, _stringLiteral5356314F1E58DF795010B28A52D552EBDE2168F4, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = __this->get_rightCollider_11();
		NullCheck(L_11);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_11, _stringLiteral6EAAAD406CBC78CB91855411019F284B7225DE23, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = __this->get_leftCollider_10();
		NullCheck(L_12);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_12, _stringLiteral7FE80403354BDA7FB596B0F9608845E4DA306DDC, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = __this->get_topCollider_8();
		NullCheck(L_13);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F(L_14, /*hidden argument*/GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F_RuntimeMethod_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = __this->get_bottomCollider_9();
		NullCheck(L_15);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F(L_16, /*hidden argument*/GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F_RuntimeMethod_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = __this->get_rightCollider_11();
		NullCheck(L_17);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F(L_18, /*hidden argument*/GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F_RuntimeMethod_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = __this->get_leftCollider_10();
		NullCheck(L_19);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F(L_20, /*hidden argument*/GameObject_AddComponent_TisBoxCollider2D_tA3DD87FE6F65C39F0A81CDB4BEC0EDB370486E87_mDC920F9846510D293F02362C225E944019237C4F_RuntimeMethod_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = __this->get_topCollider_8();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_21, L_22, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_23 = __this->get_bottomCollider_9();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_23, L_24, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = __this->get_rightCollider_11();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_26 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_25, L_26, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = __this->get_leftCollider_10();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_28 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_27, L_28, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_29 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_30 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_30, /*hidden argument*/NULL);
		__this->set_cameraPos_12(L_31);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_32 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_33), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5(L_32, L_34, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_35, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_37 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		int32_t L_38 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_39), (((float)((float)L_38))), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5(L_37, L_40, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_42 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_41, /*hidden argument*/NULL);
		float L_43 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_36, L_42, /*hidden argument*/NULL);
		(((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->set_x_0(((float)il2cpp_codegen_multiply((float)L_43, (float)(0.5f))));
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_44 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_45), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5(L_44, L_46, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_48 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_47, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_49 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		int32_t L_50 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_51), (0.0f), (((float)((float)L_50))), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_52 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_51, /*hidden argument*/NULL);
		NullCheck(L_49);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_53 = Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5(L_49, L_52, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_54 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_53, /*hidden argument*/NULL);
		float L_55 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_48, L_54, /*hidden argument*/NULL);
		(((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->set_y_1(((float)il2cpp_codegen_multiply((float)L_55, (float)(0.5f))));
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_56 = __this->get_rightCollider_11();
		float L_57 = __this->get_colDepth_4();
		float L_58 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		float L_59 = __this->get_colDepth_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_60), L_57, ((float)il2cpp_codegen_multiply((float)L_58, (float)(2.0f))), L_59, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_56, L_60, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_61 = __this->get_rightCollider_11();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_62 = __this->get_address_of_cameraPos_12();
		float L_63 = L_62->get_x_2();
		float L_64 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_65 = __this->get_rightCollider_11();
		NullCheck(L_65);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_66 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_65, /*hidden argument*/NULL);
		float L_67 = L_66.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_68 = __this->get_address_of_cameraPos_12();
		float L_69 = L_68->get_y_3();
		float L_70 = __this->get_zPosition_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_71;
		memset(&L_71, 0, sizeof(L_71));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_71), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_63, (float)L_64)), (float)((float)il2cpp_codegen_multiply((float)L_67, (float)(0.5f))))), L_69, L_70, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_61, L_71, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_72 = __this->get_leftCollider_10();
		float L_73 = __this->get_colDepth_4();
		float L_74 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		float L_75 = __this->get_colDepth_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_76;
		memset(&L_76, 0, sizeof(L_76));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_76), L_73, ((float)il2cpp_codegen_multiply((float)L_74, (float)(2.0f))), L_75, /*hidden argument*/NULL);
		NullCheck(L_72);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_72, L_76, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_77 = __this->get_leftCollider_10();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_78 = __this->get_address_of_cameraPos_12();
		float L_79 = L_78->get_x_2();
		float L_80 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_81 = __this->get_leftCollider_10();
		NullCheck(L_81);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_82 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_81, /*hidden argument*/NULL);
		float L_83 = L_82.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_84 = __this->get_address_of_cameraPos_12();
		float L_85 = L_84->get_y_3();
		float L_86 = __this->get_zPosition_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_87;
		memset(&L_87, 0, sizeof(L_87));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_87), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_79, (float)L_80)), (float)((float)il2cpp_codegen_multiply((float)L_83, (float)(0.5f))))), L_85, L_86, /*hidden argument*/NULL);
		NullCheck(L_77);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_77, L_87, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_88 = __this->get_topCollider_8();
		float L_89 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		float L_90 = __this->get_colDepth_4();
		float L_91 = __this->get_colDepth_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_92;
		memset(&L_92, 0, sizeof(L_92));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_92), ((float)il2cpp_codegen_multiply((float)L_89, (float)(2.0f))), L_90, L_91, /*hidden argument*/NULL);
		NullCheck(L_88);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_88, L_92, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_93 = __this->get_topCollider_8();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_94 = __this->get_address_of_cameraPos_12();
		float L_95 = L_94->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_96 = __this->get_address_of_cameraPos_12();
		float L_97 = L_96->get_y_3();
		float L_98 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_99 = __this->get_topCollider_8();
		NullCheck(L_99);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_100 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_99, /*hidden argument*/NULL);
		float L_101 = L_100.get_y_3();
		float L_102 = __this->get_zPosition_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_103;
		memset(&L_103, 0, sizeof(L_103));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_103), L_95, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_97, (float)L_98)), (float)((float)il2cpp_codegen_multiply((float)L_101, (float)(0.5f))))), L_102, /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_93, L_103, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_104 = __this->get_bottomCollider_9();
		float L_105 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		float L_106 = __this->get_colDepth_4();
		float L_107 = __this->get_colDepth_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_108), ((float)il2cpp_codegen_multiply((float)L_105, (float)(2.0f))), L_106, L_107, /*hidden argument*/NULL);
		NullCheck(L_104);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_104, L_108, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_109 = __this->get_bottomCollider_9();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_110 = __this->get_address_of_cameraPos_12();
		float L_111 = L_110->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_112 = __this->get_address_of_cameraPos_12();
		float L_113 = L_112->get_y_3();
		float L_114 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_115 = __this->get_bottomCollider_9();
		NullCheck(L_115);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_116 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_115, /*hidden argument*/NULL);
		float L_117 = L_116.get_y_3();
		float L_118 = __this->get_zPosition_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_119;
		memset(&L_119, 0, sizeof(L_119));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_119), L_111, ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_113, (float)L_114)), (float)((float)il2cpp_codegen_multiply((float)L_117, (float)(0.5f))))), L_118, /*hidden argument*/NULL);
		NullCheck(L_109);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_109, L_119, /*hidden argument*/NULL);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_120 = __this->get_ball_13();
		NullCheck(L_120);
		CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F * L_121 = Component_GetComponent_TisCircleCollider2D_t862AED067B612149EF365A560B26406F6088641F_m3DAB966F6D30A22BFF7873DAA1254C2FFD25226A(L_120, /*hidden argument*/Component_GetComponent_TisCircleCollider2D_t862AED067B612149EF365A560B26406F6088641F_m3DAB966F6D30A22BFF7873DAA1254C2FFD25226A_RuntimeMethod_var);
		NullCheck(L_121);
		float L_122 = CircleCollider2D_get_radius_m016333FD7A5A5FD84FEFD7D02B57D4CA728EFA27(L_121, /*hidden argument*/NULL);
		__this->set_ballDiameter_22(((float)il2cpp_codegen_multiply((float)L_122, (float)(2.0f))));
		float L_123 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		float L_124 = __this->get_ballDiameter_22();
		__this->set_fbpToNegX_23(((float)il2cpp_codegen_add((float)((-L_123)), (float)L_124)));
		float L_125 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_x_0();
		float L_126 = __this->get_ballDiameter_22();
		__this->set_fbpToPosX_24(((float)il2cpp_codegen_subtract((float)L_125, (float)L_126)));
		float L_127 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		float L_128 = __this->get_ballDiameter_22();
		__this->set_fbpToNegY_25(((float)il2cpp_codegen_add((float)((-L_127)), (float)L_128)));
		float L_129 = (((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_address_of_screenSize_7())->get_y_1();
		float L_130 = __this->get_ballDiameter_22();
		__this->set_fbpToPosY_26(((float)il2cpp_codegen_subtract((float)L_129, (float)L_130)));
		MainCamera_StartRound_m78EB3716D63672EE7D7BB8F9A0C20AEFA6689A1D(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainCamera::GenBall(System.Single,System.Single,System.Single,System.Single,Note)
extern "C" IL2CPP_METHOD_ATTR void MainCamera_GenBall_m55EF5932A3ED533C76096EDB8A14162AB5B1155E (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, float ___fbpToNegX0, float ___fbpToPosX1, float ___fbpToNegY2, float ___fbpToPosY3, Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * ___note4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera_GenBall_m55EF5932A3ED533C76096EDB8A14162AB5B1155E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * V_0 = NULL;
	{
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_0 = __this->get_ball_13();
		float L_1 = ___fbpToNegX0;
		float L_2 = ___fbpToPosX1;
		float L_3 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384(L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___fbpToNegY2;
		float L_5 = ___fbpToPosY3;
		float L_6 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384(L_4, L_5, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), L_3, L_6, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_8 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_9 = Object_Instantiate_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_m7A27F732EFDF61FC8EB6C64ECDBCB4E5029B798A(L_0, L_7, L_8, /*hidden argument*/Object_Instantiate_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_m7A27F732EFDF61FC8EB6C64ECDBCB4E5029B798A_RuntimeMethod_var);
		V_0 = L_9;
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_10 = V_0;
		NullCheck(L_10);
		Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_11 = Component_GetComponent_TisBall_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3_m1EFA6F667F1EE1492D5E1FFCE95E405249525BD0(L_10, /*hidden argument*/Component_GetComponent_TisBall_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3_m1EFA6F667F1EE1492D5E1FFCE95E405249525BD0_RuntimeMethod_var);
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_12 = ___note4;
		NullCheck(L_11);
		L_11->set_note_14(L_12);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_13 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_ballList_15();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_14 = V_0;
		NullCheck(L_14);
		Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_15 = Component_GetComponent_TisBall_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3_m1EFA6F667F1EE1492D5E1FFCE95E405249525BD0(L_14, /*hidden argument*/Component_GetComponent_TisBall_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3_m1EFA6F667F1EE1492D5E1FFCE95E405249525BD0_RuntimeMethod_var);
		NullCheck(L_13);
		List_1_Add_m6F48A1BBD6E77DDC2CF47802AD9D4E24DF825270(L_13, L_15, /*hidden argument*/List_1_Add_m6F48A1BBD6E77DDC2CF47802AD9D4E24DF825270_RuntimeMethod_var);
		return;
	}
}
// System.Void MainCamera::StartRound()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_StartRound_m78EB3716D63672EE7D7BB8F9A0C20AEFA6689A1D (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera_StartRound_m78EB3716D63672EE7D7BB8F9A0C20AEFA6689A1D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * V_2 = NULL;
	Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_0 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_0);
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_1 = GameControl_GetNoteList_m08557D7260276BDFD31767A38D7BE9CBC627F9F3(L_0, /*hidden argument*/NULL);
		__this->set_answerList_20(L_1);
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_2 = (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *)il2cpp_codegen_object_new(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784_il2cpp_TypeInfo_var);
		List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116(L_2, /*hidden argument*/List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116_RuntimeMethod_var);
		__this->set_answerListCopy_21(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_3 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_ballList_15();
		NullCheck(L_3);
		Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  L_4 = List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489(L_3, /*hidden argument*/List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489_RuntimeMethod_var);
		V_3 = L_4;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0047;
		}

IL_0028:
		{
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_5 = Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_3), /*hidden argument*/Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E_RuntimeMethod_var);
			V_4 = L_5;
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_6 = V_4;
			NullCheck(L_6);
			bool L_7 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_6, NULL);
			if (L_7)
			{
				goto IL_0047;
			}
		}

IL_003b:
		{
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_8 = V_4;
			NullCheck(L_8);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_9, /*hidden argument*/NULL);
		}

IL_0047:
		{
			bool L_10 = Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_3), /*hidden argument*/Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076_RuntimeMethod_var);
			if (L_10)
			{
				goto IL_0028;
			}
		}

IL_0050:
		{
			IL2CPP_LEAVE(0x60, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_3), /*hidden argument*/Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC_RuntimeMethod_var);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_11 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_ballList_15();
		NullCheck(L_11);
		List_1_Clear_m7C39C9921E33484DF592F287D97CCBDCD64E14C0(L_11, /*hidden argument*/List_1_Clear_m7C39C9921E33484DF592F287D97CCBDCD64E14C0_RuntimeMethod_var);
		Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * L_12 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_rand_19();
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_13 = __this->get_answerList_20();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA(L_13, /*hidden argument*/List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA_RuntimeMethod_var);
		NullCheck(L_12);
		int32_t L_15 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(6 /* System.Int32 System.Random::Next(System.Int32) */, L_12, L_14);
		V_0 = L_15;
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_16 = __this->get_answerList_20();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_18 = List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0(L_16, L_17, /*hidden argument*/List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0_RuntimeMethod_var);
		NullCheck(L_18);
		String_t* L_19 = Note_get_theNote_mD93A18AA36F41A8FAD9E225130C45FC8C1CCEDE2(L_18, /*hidden argument*/NULL);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_theAnswer_6(L_19);
		V_5 = 0;
		goto IL_00be;
	}

IL_009b:
	{
		int32_t L_20 = V_5;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) == ((int32_t)L_21)))
		{
			goto IL_00b8;
		}
	}
	{
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_22 = __this->get_answerListCopy_21();
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_23 = __this->get_answerList_20();
		int32_t L_24 = V_5;
		NullCheck(L_23);
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_25 = List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0(L_23, L_24, /*hidden argument*/List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0_RuntimeMethod_var);
		NullCheck(L_22);
		List_1_Add_mE0D78113FD836E165C8F1B82481737DC8C2E51A3(L_22, L_25, /*hidden argument*/List_1_Add_mE0D78113FD836E165C8F1B82481737DC8C2E51A3_RuntimeMethod_var);
	}

IL_00b8:
	{
		int32_t L_26 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
	}

IL_00be:
	{
		int32_t L_27 = V_5;
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_28 = __this->get_answerList_20();
		NullCheck(L_28);
		int32_t L_29 = List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA(L_28, /*hidden argument*/List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA_RuntimeMethod_var);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_009b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_numPossibleAnswers_17(2);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_numCorrectAnswers_18(0);
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_30 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_30);
		int32_t L_31 = GameControl_GetNumBubbles_mEC449EA24FCB7D7371C17D9F1AE1D2A978317BD8(L_30, /*hidden argument*/NULL);
		V_1 = L_31;
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_32 = (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B *)il2cpp_codegen_object_new(Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B_il2cpp_TypeInfo_var);
		Note__ctor_mC1987B976BE8297565673F2B85BC34F3CB6F2D65(L_32, /*hidden argument*/NULL);
		V_2 = L_32;
		V_6 = 1;
		goto IL_0158;
	}

IL_00ef:
	{
		int32_t L_33 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		int32_t L_34 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_numPossibleAnswers_17();
		if ((((int32_t)L_33) > ((int32_t)L_34)))
		{
			goto IL_0107;
		}
	}
	{
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_35 = __this->get_answerList_20();
		int32_t L_36 = V_0;
		NullCheck(L_35);
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_37 = List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0(L_35, L_36, /*hidden argument*/List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0_RuntimeMethod_var);
		V_2 = L_37;
		goto IL_0133;
	}

IL_0107:
	{
		int32_t L_38 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		int32_t L_39 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_numPossibleAnswers_17();
		if ((((int32_t)L_38) == ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)2)))))
		{
			goto IL_0133;
		}
	}
	{
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_40 = __this->get_answerListCopy_21();
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * L_41 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_rand_19();
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_42 = __this->get_answerListCopy_21();
		NullCheck(L_42);
		int32_t L_43 = List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA(L_42, /*hidden argument*/List_1_get_Count_mA1108D1754036861C658F30D6B735BD883EFA6BA_RuntimeMethod_var);
		NullCheck(L_41);
		int32_t L_44 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(6 /* System.Int32 System.Random::Next(System.Int32) */, L_41, L_43);
		NullCheck(L_40);
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_45 = List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0(L_40, L_44, /*hidden argument*/List_1_get_Item_mDF06FBF0273CBD807CC97605F23DF33432BA7BA0_RuntimeMethod_var);
		V_2 = L_45;
	}

IL_0133:
	{
		float L_46 = __this->get_fbpToNegX_23();
		float L_47 = __this->get_fbpToPosX_24();
		float L_48 = __this->get_fbpToNegY_25();
		float L_49 = __this->get_fbpToPosY_26();
		Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * L_50 = V_2;
		MainCamera_GenBall_m55EF5932A3ED533C76096EDB8A14162AB5B1155E(__this, L_46, L_47, L_48, L_49, L_50, /*hidden argument*/NULL);
		int32_t L_51 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_0158:
	{
		int32_t L_52 = V_6;
		int32_t L_53 = V_1;
		if ((((int32_t)L_52) <= ((int32_t)L_53)))
		{
			goto IL_00ef;
		}
	}
	{
		MainCamera_HideBalls_mC0B833AE68EF16E75E7F7D4572C1D075C9AA5B62(__this, /*hidden argument*/NULL);
		Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * L_54 = __this->get_timer_14();
		NullCheck(L_54);
		Timer_StartTimer_m297CF68F48B8693DAF2424C0FC96F40B7BB9C5B5(L_54, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainCamera::HideBalls()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_HideBalls_mC0B833AE68EF16E75E7F7D4572C1D075C9AA5B62 (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera_HideBalls_mC0B833AE68EF16E75E7F7D4572C1D075C9AA5B62_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_0 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_ballList_15();
		NullCheck(L_0);
		Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  L_1 = List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489(L_0, /*hidden argument*/List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001f;
		}

IL_000d:
		{
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_2 = Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E_RuntimeMethod_var);
			NullCheck(L_2);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_2, /*hidden argument*/NULL);
			NullCheck(L_3);
			GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)0, /*hidden argument*/NULL);
		}

IL_001f:
		{
			bool L_4 = Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076_RuntimeMethod_var);
			if (L_4)
			{
				goto IL_000d;
			}
		}

IL_0028:
		{
			IL2CPP_LEAVE(0x38, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC_RuntimeMethod_var);
		IL2CPP_END_FINALLY(42)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0038:
	{
		return;
	}
}
// System.Void MainCamera::ShowBalls()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_ShowBalls_mF2D16795F6AA46BD96C109D468AD2C3C4063875D (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera_ShowBalls_mF2D16795F6AA46BD96C109D468AD2C3C4063875D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_0 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_ballList_15();
		NullCheck(L_0);
		Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF  L_1 = List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489(L_0, /*hidden argument*/List_1_GetEnumerator_mF5EC8A73F12544B8E1621004F036F3EED557D489_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001f;
		}

IL_000d:
		{
			Ball_t2A505E3143FBAC85D561E48B8E9B9462719F3EB3 * L_2 = Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_get_Current_m11A995C9D522B4CD180C004A3B70F238C31FFF4E_RuntimeMethod_var);
			NullCheck(L_2);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_2, /*hidden argument*/NULL);
			NullCheck(L_3);
			GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)1, /*hidden argument*/NULL);
		}

IL_001f:
		{
			bool L_4 = Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mB96464BBF987E8539656DFAC7E268ED950D59076_RuntimeMethod_var);
			if (L_4)
			{
				goto IL_000d;
			}
		}

IL_0028:
		{
			IL2CPP_LEAVE(0x38, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC((Enumerator_tEADA40C26FB2A9B11A2C043B82DD65D94B92A0BF *)(&V_0), /*hidden argument*/Enumerator_Dispose_m50CF4ECB301871A2AE1F909FC5992FC92DD130AC_RuntimeMethod_var);
		IL2CPP_END_FINALLY(42)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0038:
	{
		return;
	}
}
// System.Void MainCamera::ClearRound()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_ClearRound_m6BC054933A01450947243932EFEF8270081E826F (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera_ClearRound_m6BC054933A01450947243932EFEF8270081E826F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_0 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_ballList_15();
		NullCheck(L_0);
		List_1_Clear_m7C39C9921E33484DF592F287D97CCBDCD64E14C0(L_0, /*hidden argument*/List_1_Clear_m7C39C9921E33484DF592F287D97CCBDCD64E14C0_RuntimeMethod_var);
		return;
	}
}
// System.Void MainCamera::Update()
extern "C" IL2CPP_METHOD_ATTR void MainCamera_Update_mB9BDFE0F535CDFAE9555B8910B3297994D1A6DF0 (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MainCamera::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MainCamera__ctor_m0DBB1A4B33E2FA2CAE1BEAB5E7F1AA96BE67BF17 (MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera__ctor_m0DBB1A4B33E2FA2CAE1BEAB5E7F1AA96BE67BF17_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_colDepth_4((40.0f));
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_0 = (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *)il2cpp_codegen_object_new(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784_il2cpp_TypeInfo_var);
		List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116(L_0, /*hidden argument*/List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116_RuntimeMethod_var);
		__this->set_answerList_20(L_0);
		List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 * L_1 = (List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784 *)il2cpp_codegen_object_new(List_1_tCABEADDE7156DE34035F33555BC0B75E12FB3784_il2cpp_TypeInfo_var);
		List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116(L_1, /*hidden argument*/List_1__ctor_mF20C2AE2D69F7E09E0174CD4A424592A152AC116_RuntimeMethod_var);
		__this->set_answerListCopy_21(L_1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainCamera::.cctor()
extern "C" IL2CPP_METHOD_ATTR void MainCamera__cctor_m79E60A08E55B86886C621B56E1DBC0DFEF90A292 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera__cctor_m79E60A08E55B86886C621B56E1DBC0DFEF90A292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 * L_0 = (List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6 *)il2cpp_codegen_object_new(List_1_t2D41893E28DE8FBC9208386ACDC461ED7C3E63A6_il2cpp_TypeInfo_var);
		List_1__ctor_m586CA3DF487C8F421B52B94EBB8AFB966C4E3D22(L_0, /*hidden argument*/List_1__ctor_m586CA3DF487C8F421B52B94EBB8AFB966C4E3D22_RuntimeMethod_var);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_ballList_15(L_0);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_gameOn_16((bool)0);
		Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * L_1 = (Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F *)il2cpp_codegen_object_new(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F_il2cpp_TypeInfo_var);
		Random__ctor_mCD4B6E9DFD27A19F52FA441CD8CAEB687A9DD2F2(L_1, /*hidden argument*/NULL);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_rand_19(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu::Start()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MainMenu::Update()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MainMenu::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuLayout::Start()
extern "C" IL2CPP_METHOD_ATTR void MenuLayout_Start_mB06FBE1B9F5B359CFF9E831141CE05F7594C1EF4 (MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MenuLayout::Update()
extern "C" IL2CPP_METHOD_ATTR void MenuLayout_Update_mBD83C7BB695F849A13D7B8562A9AA20C572C5BC1 (MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator MenuLayout::CheckForChange()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MenuLayout_CheckForChange_mFE349E388AC54C070288FC2F2A0A0AC2A48FD57B (MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuLayout_CheckForChange_mFE349E388AC54C070288FC2F2A0A0AC2A48FD57B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * L_0 = (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 *)il2cpp_codegen_object_new(U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01_il2cpp_TypeInfo_var);
		U3CCheckForChangeU3Ed__2__ctor_mD1F0F9D3E1D97C8F339BD0A2F40F1EBD0A109A3D(L_0, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void MenuLayout::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MenuLayout__ctor_mDD3407E05C87A0E2F146C1B087C08C8B029C7BDB (MenuLayout_tF5F4902CBDEF9388989C7F621F64B424BCA978B7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuLayout/<CheckForChange>d__2::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CCheckForChangeU3Ed__2__ctor_mD1F0F9D3E1D97C8F339BD0A2F40F1EBD0A109A3D (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MenuLayout/<CheckForChange>d__2::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CCheckForChangeU3Ed__2_System_IDisposable_Dispose_mFEA993CFCBB2E8278C2D43EEE3C59D13E029E072 (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MenuLayout/<CheckForChange>d__2::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CCheckForChangeU3Ed__2_MoveNext_mE710A3BE43874CC0F08EC90F70AF58D47B8ADE69 (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0036;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0017:
	{
		int32_t L_3 = Input_get_deviceOrientation_mE405D05D2FEF3E94CEB5886E51C8E4E752AF883B(/*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)5))) <= ((uint32_t)1))))
		{
			goto IL_0017;
		}
	}

IL_0026:
	{
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0036:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_0017;
	}
}
// System.Object MenuLayout/<CheckForChange>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCheckForChangeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m303C034A3BDB16DE6CB6EEC1CE02FFBF292825BA (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MenuLayout/<CheckForChange>d__2::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CCheckForChangeU3Ed__2_System_Collections_IEnumerator_Reset_m3A2B6944DF0E6BC0C61F8AA1DE6E9B9DED15D8E6 (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckForChangeU3Ed__2_System_Collections_IEnumerator_Reset_m3A2B6944DF0E6BC0C61F8AA1DE6E9B9DED15D8E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CCheckForChangeU3Ed__2_System_Collections_IEnumerator_Reset_m3A2B6944DF0E6BC0C61F8AA1DE6E9B9DED15D8E6_RuntimeMethod_var);
	}
}
// System.Object MenuLayout/<CheckForChange>d__2::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCheckForChangeU3Ed__2_System_Collections_IEnumerator_get_Current_m494B4FC0D4C07580BA2A5429BB73CE4079F02010 (U3CCheckForChangeU3Ed__2_t71710E97EFE51ED653E9F3FE4835F607C8324A01 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MusicManager::Start()
extern "C" IL2CPP_METHOD_ATTR void MusicManager_Start_mF77913A780CD343FC5CC6FE204CF7AFCD8043272 (MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MusicManager_Start_mF77913A780CD343FC5CC6FE204CF7AFCD8043272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_0 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_0);
		GameControl_Load_mBEDB97AE94477B6D97BAD1FC7A8820AB0B5D4968(L_0, /*hidden argument*/NULL);
		Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * L_1 = __this->get_musicSlider_4();
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_2 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_2);
		float L_3 = L_2->get_musicVolume_7();
		NullCheck(L_1);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_1, L_3);
		Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * L_4 = __this->get_sfxSlider_5();
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_5 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_5);
		float L_6 = L_5->get_sfxVolume_8();
		NullCheck(L_4);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_4, L_6);
		return;
	}
}
// System.Void MusicManager::Update()
extern "C" IL2CPP_METHOD_ATTR void MusicManager_Update_m59A3AA607F68AD3DD974C49E955F3520914A1442 (MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MusicManager_Update_m59A3AA607F68AD3DD974C49E955F3520914A1442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_0 = __this->get_myMusic_6();
		Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * L_1 = __this->get_musicSlider_4();
		NullCheck(L_1);
		float L_2 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		NullCheck(L_0);
		AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06(L_0, L_2, /*hidden argument*/NULL);
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_3 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * L_4 = __this->get_musicSlider_4();
		NullCheck(L_4);
		float L_5 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_4);
		NullCheck(L_3);
		L_3->set_musicVolume_7(L_5);
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_6 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * L_7 = __this->get_sfxSlider_5();
		NullCheck(L_7);
		float L_8 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_7);
		NullCheck(L_6);
		L_6->set_sfxVolume_8(L_8);
		return;
	}
}
// System.Void MusicManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MusicManager__ctor_m0910B32A2F10526CE5FCB5831384F261A434549B (MusicManager_tA48147377617955FE0763C9A03D97A97DAA15879 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Note::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Note__ctor_mC1987B976BE8297565673F2B85BC34F3CB6F2D65 (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Note__ctor_mC1987B976BE8297565673F2B85BC34F3CB6F2D65_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)7);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		__this->set_baseNotes_4(L_7);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)((int32_t)12));
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral6FBA1A95114471EB786272EF22C5244A40D9AD40);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral6FBA1A95114471EB786272EF22C5244A40D9AD40);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral49BEA2E16586CCC7DFEF61BBCE35FDFB4C1763EF);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral49BEA2E16586CCC7DFEF61BBCE35FDFB4C1763EF);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral2193DBF0F5379E90F733E226DB48796946392057);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral2193DBF0F5379E90F733E226DB48796946392057);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral37797DDCDD6B610A6C3DF82593C89B2BC0D01901);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral37797DDCDD6B610A6C3DF82593C89B2BC0D01901);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3C38EBEDF83A90567CB4B0BB88497E43FA652A67);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral3C38EBEDF83A90567CB4B0BB88497E43FA652A67);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		__this->set_notes_5(L_20);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Note::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Note__ctor_m13B09FD5AD10AE98E732FC3B88D900C819D100C6 (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, int32_t ___noteIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Note__ctor_m13B09FD5AD10AE98E732FC3B88D900C819D100C6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)7);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		__this->set_baseNotes_4(L_7);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)((int32_t)12));
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral32096C2E0EFF33D844EE6D675407ACE18289357D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral6FBA1A95114471EB786272EF22C5244A40D9AD40);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral6FBA1A95114471EB786272EF22C5244A40D9AD40);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral50C9E8D5FC98727B4BBC93CF5D64A68DB647F04F);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral49BEA2E16586CCC7DFEF61BBCE35FDFB4C1763EF);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral49BEA2E16586CCC7DFEF61BBCE35FDFB4C1763EF);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralE0184ADEDF913B076626646D3F52C3B49C39AD6D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteralE69F20E9F683920D3FB4329ABD951E878B1F9372);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral2193DBF0F5379E90F733E226DB48796946392057);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral2193DBF0F5379E90F733E226DB48796946392057);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteralA36A6718F54524D846894FB04B5B885B4E43E63B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral37797DDCDD6B610A6C3DF82593C89B2BC0D01901);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral37797DDCDD6B610A6C3DF82593C89B2BC0D01901);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral6DCD4CE23D88E2EE9568BA546C007C63D9131C1B);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3C38EBEDF83A90567CB4B0BB88497E43FA652A67);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral3C38EBEDF83A90567CB4B0BB88497E43FA652A67);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteralAE4F281DF5A5D0FF3CAD6371F76D5C29B6D953EC);
		__this->set_notes_5(L_20);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = __this->get_notes_5();
		int32_t L_22 = ___noteIndex0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_23 = __this->get_notes_5();
		NullCheck(L_23);
		NullCheck(L_21);
		int32_t L_24 = ((int32_t)((int32_t)L_22%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length))))));
		String_t* L_25 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		__this->set__note_7(L_25);
		int32_t L_26 = ___noteIndex0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_27 = __this->get_notes_5();
		NullCheck(L_27);
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		double L_28 = floor((((double)((double)((float)((float)(((float)((float)L_26)))/(float)(((float)((float)(((int32_t)((int32_t)(((RuntimeArray *)L_27)->max_length)))))))))))));
		__this->set__octave_6((((int32_t)((int32_t)L_28))));
		String_t* L_29 = __this->get__note_7();
		NullCheck(L_29);
		int32_t L_30 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_29, /*hidden argument*/NULL);
		if ((((int32_t)L_30) <= ((int32_t)1)))
		{
			goto IL_0100;
		}
	}
	{
		__this->set_hasAccidentals_8((bool)1);
		return;
	}

IL_0100:
	{
		__this->set_hasAccidentals_8((bool)0);
		return;
	}
}
// System.String Note::get_theNote()
extern "C" IL2CPP_METHOD_ATTR String_t* Note_get_theNote_mD93A18AA36F41A8FAD9E225130C45FC8C1CCEDE2 (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__note_7();
		return L_0;
	}
}
// System.Int32 Note::get_Octave()
extern "C" IL2CPP_METHOD_ATTR int32_t Note_get_Octave_m0886E707D45BB24FAAA7A11950C13C847063077D (Note_tB8381BB2C942DB3B8911836FC539545E83D4D43B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__octave_6();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PSbubbleCollision::Start()
extern "C" IL2CPP_METHOD_ATTR void PSbubbleCollision_Start_m3E1B0CF7B8E4F3D72F3E081AA90E942D2A95EA27 (PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PSbubbleCollision::OnParticleSystemStopped()
extern "C" IL2CPP_METHOD_ATTR void PSbubbleCollision_OnParticleSystemStopped_mD70A211C5AD327481604FFAAA6ED99A5A34FC5DB (PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PSbubbleCollision_OnParticleSystemStopped_mD70A211C5AD327481604FFAAA6ED99A5A34FC5DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_0 = __this->get_ps_4();
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PSbubbleCollision::Update()
extern "C" IL2CPP_METHOD_ATTR void PSbubbleCollision_Update_m66A3FDE39F764AF5A0893A7018832CE0B712F8A7 (PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PSbubbleCollision::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PSbubbleCollision__ctor_mB11FD40DDA47253876B36DBABA78D5D0E51FDBE0 (PSbubbleCollision_t0B60501CBB8D50CF4675F447EC6DFEFB937CDDFA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayerData__ctor_m01DB4046DFC9FA3BEF1BF9C3E9A877E4F4D33D63 (PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Timer::Start()
extern "C" IL2CPP_METHOD_ATTR void Timer_Start_mEFDCF17412717C132D11A41F14C8F9E96A4B78E4 (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Timer_Start_mEFDCF17412717C132D11A41F14C8F9E96A4B78E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_1 = GameObject_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_mCDC3359066029BF682DB5CC73FECB9C648B1BE8E(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_mCDC3359066029BF682DB5CC73FECB9C648B1BE8E_RuntimeMethod_var);
		__this->set_audioSource_14(L_1);
		return;
	}
}
// System.Void Timer::StartTimer()
extern "C" IL2CPP_METHOD_ATTR void Timer_StartTimer_m297CF68F48B8693DAF2424C0FC96F40B7BB9C5B5 (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Timer_StartTimer_m297CF68F48B8693DAF2424C0FC96F40B7BB9C5B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_introTime_6((2.0f));
		GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248 * L_0 = ((GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_StaticFields*)il2cpp_codegen_static_fields_for(GameControl_t60391CE7B0B029798FBC6F92207CDB38C6B1F248_il2cpp_TypeInfo_var))->get_control_4();
		NullCheck(L_0);
		float L_1 = GameControl_GetRoundTime_mF013DFD64E5B774736534CD07229DAA223A9E352(L_0, /*hidden argument*/NULL);
		__this->set_roundTime_7(L_1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_StartGameLeadText_8();
		NullCheck(L_2);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_StartGameObjectiveText_9();
		NullCheck(L_3);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_StartGameObjectiveText_9();
		NullCheck(L_4);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_5 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_4, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var);
		String_t* L_6 = ((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->get_theAnswer_6();
		NullCheck(L_6);
		String_t* L_7 = String_ToUpper_m23D019B7C5EF2C5C01F524EB8137A424B33EEFE2(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_7);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_EndTimeText_10();
		NullCheck(L_8);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_8, (bool)0, /*hidden argument*/NULL);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_9 = Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8(__this, /*hidden argument*/Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8_RuntimeMethod_var);
		NullCheck(L_9);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_9, (bool)0, /*hidden argument*/NULL);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_10 = Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8(__this, /*hidden argument*/Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8_RuntimeMethod_var);
		__this->set_timerBar_4(L_10);
		float L_11 = __this->get_introTime_6();
		__this->set_timeLeft_5(L_11);
		__this->set_doingIntro_12((bool)1);
		((Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields*)il2cpp_codegen_static_fields_for(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_il2cpp_TypeInfo_var))->set_runTimer_11((bool)1);
		((MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_StaticFields*)il2cpp_codegen_static_fields_for(MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573_il2cpp_TypeInfo_var))->set_gameOn_16((bool)1);
		return;
	}
}
// System.Void Timer::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Timer_FixedUpdate_mFFCBEB534DA7514BB9F15499D09EA7DFEE134E6D (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Timer_FixedUpdate_mFFCBEB534DA7514BB9F15499D09EA7DFEE134E6D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields*)il2cpp_codegen_static_fields_for(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_il2cpp_TypeInfo_var))->get_runTimer_11();
		if (!L_0)
		{
			goto IL_00d1;
		}
	}
	{
		float L_1 = __this->get_timeLeft_5();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_2 = __this->get_timeLeft_5();
		float L_3 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_timeLeft_5(((float)il2cpp_codegen_subtract((float)L_2, (float)L_3)));
		bool L_4 = __this->get_doingIntro_12();
		if (L_4)
		{
			goto IL_00d1;
		}
	}
	{
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_5 = Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8(__this, /*hidden argument*/Component_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_mE35687928423C6384AFA5449298B1140012832A8_RuntimeMethod_var);
		NullCheck(L_5);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_5, (bool)1, /*hidden argument*/NULL);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_6 = __this->get_timerBar_4();
		float L_7 = __this->get_timeLeft_5();
		float L_8 = __this->get_roundTime_7();
		NullCheck(L_6);
		Image_set_fillAmount_mA775A069067A26F0F506F6590D05337C2F08A030(L_6, ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		return;
	}

IL_0059:
	{
		bool L_9 = __this->get_doingIntro_12();
		if (!L_9)
		{
			goto IL_0098;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_StartGameLeadText_8();
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = __this->get_StartGameObjectiveText_9();
		NullCheck(L_11);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_11, (bool)0, /*hidden argument*/NULL);
		MainCamera_tFD85DD5C86E2E33A0D38306F77612F04983DC573 * L_12 = __this->get_mainCamera_13();
		NullCheck(L_12);
		MainCamera_ShowBalls_mF2D16795F6AA46BD96C109D468AD2C3C4063875D(L_12, /*hidden argument*/NULL);
		__this->set_doingIntro_12((bool)0);
		float L_13 = __this->get_roundTime_7();
		__this->set_timeLeft_5(L_13);
		return;
	}

IL_0098:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_EndTimeText_10();
		NullCheck(L_14);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_14, (bool)1, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_15 = __this->get_audioSource_14();
		AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * L_16 = Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F(_stringLiteral44D95C3B6D58CE780E8322AC7DD9F7883B0E0BE1, /*hidden argument*/Resources_Load_TisAudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051_mEFBBF028B72CF71A92F7EB801D6DA765A205706F_RuntimeMethod_var);
		NullCheck(L_15);
		AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD(L_15, L_16, /*hidden argument*/NULL);
		RuntimeObject* L_17 = Timer_LoadLevelAfterDelay_mA28E0E56333FA52D98A2FBA9D42AC3240354E26E(__this, (3.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_17, /*hidden argument*/NULL);
		((Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_StaticFields*)il2cpp_codegen_static_fields_for(Timer_tD11671555E440B2E0490D401C230CA3B19EDD798_il2cpp_TypeInfo_var))->set_runTimer_11((bool)0);
	}

IL_00d1:
	{
		return;
	}
}
// System.Collections.IEnumerator Timer::LoadLevelAfterDelay(System.Single)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Timer_LoadLevelAfterDelay_mA28E0E56333FA52D98A2FBA9D42AC3240354E26E (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * __this, float ___delay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Timer_LoadLevelAfterDelay_mA28E0E56333FA52D98A2FBA9D42AC3240354E26E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * L_0 = (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B *)il2cpp_codegen_object_new(U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B_il2cpp_TypeInfo_var);
		U3CLoadLevelAfterDelayU3Ed__14__ctor_m0C028405AF7A6E73BD69C37E11796B07AD3D4CDF(L_0, 0, /*hidden argument*/NULL);
		U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * L_1 = L_0;
		float L_2 = ___delay0;
		NullCheck(L_1);
		L_1->set_delay_2(L_2);
		return L_1;
	}
}
// System.Void Timer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Timer__ctor_m05F02A43855C43EEF0FE3171A63A895A95D9322C (Timer_tD11671555E440B2E0490D401C230CA3B19EDD798 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Timer/<LoadLevelAfterDelay>d__14::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__14__ctor_m0C028405AF7A6E73BD69C37E11796B07AD3D4CDF (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Timer/<LoadLevelAfterDelay>d__14::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__14_System_IDisposable_Dispose_mBCBDCB73AACD5E0B689477AFBD93562EBEDEA9E4 (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Timer/<LoadLevelAfterDelay>d__14::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CLoadLevelAfterDelayU3Ed__14_MoveNext_m2169FB23AF6A69A4D091FD7687A75EAD456DAF3E (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadLevelAfterDelayU3Ed__14_MoveNext_m2169FB23AF6A69A4D091FD7687A75EAD456DAF3E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_delay_2();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_4 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(_stringLiteralEABCFFD8D5120B660823E2C294A8DC252DA5EA29, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object Timer/<LoadLevelAfterDelay>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadLevelAfterDelayU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADBCEADCD23F2D11007203F88861AF4908AC06B3 (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Timer/<LoadLevelAfterDelay>d__14::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CLoadLevelAfterDelayU3Ed__14_System_Collections_IEnumerator_Reset_m46CD2F1CB19E31D90E3E090D3FE356A887381F59 (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadLevelAfterDelayU3Ed__14_System_Collections_IEnumerator_Reset_m46CD2F1CB19E31D90E3E090D3FE356A887381F59_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CLoadLevelAfterDelayU3Ed__14_System_Collections_IEnumerator_Reset_m46CD2F1CB19E31D90E3E090D3FE356A887381F59_RuntimeMethod_var);
	}
}
// System.Object Timer/<LoadLevelAfterDelay>d__14::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadLevelAfterDelayU3Ed__14_System_Collections_IEnumerator_get_Current_mC76C1B628B4DF9A83FCD46C675DD95AEB6E5D514 (U3CLoadLevelAfterDelayU3Ed__14_t8770C7FEF537DAA3A3A54000B456E5940BD58B5B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void runningAnimator::Start()
extern "C" IL2CPP_METHOD_ATTR void runningAnimator_Start_mD40A31302453B398A3EE4F7A420B278AD7C2CCB8 (runningAnimator_t3E9FF414BEDA1271746FF9D344FE5D64A059DA16 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void runningAnimator::Update()
extern "C" IL2CPP_METHOD_ATTR void runningAnimator_Update_m46563584A5719A2AA592061F89BAEE8D43BEBE34 (runningAnimator_t3E9FF414BEDA1271746FF9D344FE5D64A059DA16 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void runningAnimator::.ctor()
extern "C" IL2CPP_METHOD_ATTR void runningAnimator__ctor_mCCB0FCB0BAD032A2F5A4BFD6FFB7D4CC95356C15 (runningAnimator_t3E9FF414BEDA1271746FF9D344FE5D64A059DA16 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
